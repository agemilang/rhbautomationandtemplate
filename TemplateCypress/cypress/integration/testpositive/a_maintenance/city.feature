Feature: City

  I want to CRUD CITY
  
Scenario: Add And Approve City
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click City
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Form City
    Then I Click Logout