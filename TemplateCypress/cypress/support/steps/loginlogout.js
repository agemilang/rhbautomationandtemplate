Given('I open RHB login page', () => {
    cy.visit('https://13.251.82.102/login?redirect=/')
  });
  
  When('I type in RHB', (datatable) => { 
    datatable.hashes().forEach(element => {
      cy.get('div:nth-child(1) > .field .input').type(element.userid)
      cy.get('div:nth-child(2) > .field .input').type(element.password)
      cy.wait(5000)
    })
  })

  Then('I type in RHB', (datatable) => { 
    datatable.hashes().forEach(element => {
      cy.get('div:nth-child(1) > .field .input').type(element.userid)
      cy.get('div:nth-child(2) > .field .input').type(element.password)
      cy.wait(5000)
    })
  })
  
  When('I click on Sign in button', () => {
    cy.get('.button').contains('LOGIN').should('be.visible').click()
  });

  Then('I Login admin2demo', () => { 
    cy.get(':nth-child(2) > .column > .field > .control > .input').type('admin2demo')
    cy.get(':nth-child(3) > .column > .field > .control > .input').type('password')
  })

  Then('I click on Sign in button', () => {
    cy.get('.button').contains('LOGIN').should('be.visible').click()
  });


  Then('I Click Logout', (content) => {
    cy.wait(5000)
    cy.xpath("//div[@id='app']/div/nav/div/div[2]/div[2]/div/div/p/button").click({force: true})
    cy.wait(5000)
  });