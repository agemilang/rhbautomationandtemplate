from behave import given, when, then

@given("I Fill PIN")
def pin(context):
    context.app.pin.pin()

@then("I Fill PIN")
def pin(context):
    context.app.pin.pin()

@given("I Fill Wrong PIN")
def wrong_pin(context):
    context.app.pin.wrong_pin()

@then("I Fill Wrong PIN")
def wrong_pin(context):
    context.app.pin.wrong_pin()

@then("I Check Error Message PIN Wrong")
def alert_pin_wrong(context):
    context.app.pin.alert_pin_wrong()

@then("I Click Skip Activation")
def alert_pin_wrong(context):
    context.app.pin.alert_pin_wrong()

@when("I Click Maybe Later")
def click_maybe_later(context):
    context.app.pinIOS.click_maybe_later()