from behave import given, when, then

@when("I Choose Maybe Later Biometric")
def biometeric_maybe_later(context):
    context.app.EAppsMyKadTRIO.biometeric_maybe_later()

@then("I Click Mobile Banking")
def click_mobile_banking(context):
    context.app.EAppsMyKadTRIO.click_mobile_banking()

@then("I Click Center Menu Choose EApps")
def click_center_menu(context):
    context.app.EAppsMyKadTRIO.click_center_menu()

@then("I Click Product TRIO")
def choose_product(context):
    context.app.EAppsMyKadTRIO.choose_product()

@then("I Fill Form Account Detail TRIO")
def fill_form_account_detail_trio(context):
    context.app.EAppsMyKadTRIO.fill_form_account_detail_trio()

@then("I Fill Form Contact Detail HYSA & TRIO")
def fill_form_contact_detail_trio(context):
    context.app.EAppsMyKadTRIO.fill_form_contact_detail_trio()

@then("I Fill Employment Status HYSA & TRIO")
def fill_form_employment_status_employed_trio(context):
    context.app.EAppsMyKadTRIO.fill_form_employment_status_employed_trio()

@then("I Fill Declaration HYSA & TRIO")
def fill_declaration_trio(context):
    context.app.EAppsMyKadTRIO.fill_declaration_trio()

@then("I Confirm Application Detail")
def confirm_application(context):
    context.app.EAppsMyKadTRIO.confirm_application()

@then("I Agree Term & Conditions")
def agree_term_condition(context):
    context.app.EAppsMyKadTRIO.agree_term_condition()




#
# @then("I Click Hamburger Menu and E apps")
# def button_login(context):
#     context.app.EAppsMyKadTRIO.click_hamburger_menu()
#
# @then("I Choose Product")
# def button_login(context):
#     context.app.EAppsMyKadTRIO.choose_product()
#
# @then("I Fill Form Account Detail")
# def button_login(context):
#     context.app.EAppsMyKadTRIO.fill_form_account_detail()
#
# @then("I Fill Form Placement Detail")
# def button_login(context):
#     context.app.EAppsMyKadTRIO.fill_form_placement_detail()
#
#
#





