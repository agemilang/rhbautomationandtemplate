from behave import given, when, then


@then("I Click Product FDP")
def choose_product(context):
    context.app.EAppsMyKadFDP.choose_product()

@then("I Fill FDP Form")
def fill_fix_deposit_placement(context):
    context.app.EAppsMyKadFDP.fill_fix_deposit_placement()

@then("I Confirm FDP Form")
def confirm_fix_deposit_placement(context):
    context.app.EAppsMyKadFDP.confirm_fix_deposit_placement()

@then("I Click Account Summary")
def click_account_summary(context):
    context.app.EAppsMyKadFDP.click_account_summary()







