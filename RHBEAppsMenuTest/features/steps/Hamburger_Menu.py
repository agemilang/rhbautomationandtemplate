from behave import given, when, then

@when("I Click Hamburger Menu")
def click_hamburger_menu(context):
    context.app.a_MyAccount.click_hamburger_menu()

@then("I Click Hamburger Menu")
def click_hamburger_menu(context):
    context.app.a_MyAccount.click_hamburger_menu()


@when("I Click Hamburger Menu My Account 2x")
def click_hamburger_menu_my_account_2x(context):
    context.app.a_MyAccount.click_hamburger_menu_my_account_2x()


# Pay/Transfer
@then("I Click Pay / Transfer")
def click_pay_transfer(context):
    context.app.b_Pay_Transfer.click_pay_transfer()

@then("I Click Make A New Payment")
def click_makenewpayment(context):
    context.app.b_Pay_Transfer.click_makenewpayment()

@then("I Click Own Account")
def click_pay_transfer_own_account(context):
    context.app.b_Pay_Transfer.click_pay_transfer_own_account()

@then("I Check Alert Empty and Fill Own Account")
def check_alert_own_account_and_fill(context):
    context.app.b_Pay_Transfer.check_alert_own_account_and_fill()



@then("I Click Next Confirm your Transaction")
def click_next_confirm_your_transaction_own_account(context):
    context.app.b_Pay_Transfer.click_next_confirm_your_transaction_own_account()

@then("I Click Next Make Another Payment And Check Transafer Succesful")
def check_success_click_next_make_another_payment(context):
    context.app.b_Pay_Transfer.check_success_click_next_make_another_payment()

@then("I Click Next Make Another Payment And Check Transafer Submitted")
def check_submit_click_next_make_another_payment(context):
    context.app.b_Pay_Transfer.check_submit_click_next_make_another_payment()



@then("I Click Other Account")
def click_pay_transfer_other_account(context):
    context.app.b_Pay_Transfer.click_pay_transfer_other_account()

@then("I Check Alert Empty and Fill Other Account")
def check_alert_other_account_and_fill(context):
    context.app.b_Pay_Transfer.check_alert_other_account_and_fill()


@then("I Fill PIN Code Payment False")
def app_pin_false_code(context):
    context.app.b_Pay_Transfer.app_pin_false_code()

@then("I Fill PIN Code Payment True")
def app_pin_right_code(context):
    context.app.b_Pay_Transfer.app_pin_right_code()


# BillPayment
@then("I Click Next Make Bill Payment SingTel")
def click_pay_transfer_billpayment_singtel(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_singtel()

@then("I Click Next Make Bill Payment M1")
def click_pay_transfer_billpayment_m1(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_m1()

@then("I Click Next Make Bill Payment Starhub")
def click_pay_transfer_billpayment_starhub(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_starhub()

@then("I Click Next Make Bill Payment Sunpage")
def click_pay_transfer_billpayment_sunpage(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_sunpage()

@then("I Click Next Make Bill Payment Zone")
def click_pay_transfer_billpayment_zone(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_zone()

@then("I Click Next Make Bill Payment IRAS Payment")
def click_pay_transfer_billpayment_iras_payment(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_iras_payment()

@then("I Click Next Make Bill Payment TPF")
def click_pay_transfer_billpayment_tpf(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_tpf()

@then("I Click Next Make Bill Payment Stemcord")
def click_pay_transfer_billpayment_Stemcord(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_Stemcord()

# @then("I Click Next Make Bill Payment AvivaIndiPolicy")
# def click_pay_transfer_billpayment_AvivaInsuranceIndividualPolicy(context):
#     context.app.b_Pay_Transfer.click_pay_transfer_billpayment_AvivaInsuranceIndividualPolicy()
#
# @then("I Click Next Make Bill Payment AvivaMind")
# def click_pay_transfer_billpayment_AvivaInsuranceMindef(context):
#     context.app.b_Pay_Transfer.click_pay_transfer_billpayment_AvivaInsuranceMindef()

@then("I Click Next Make Bill Payment Hitachi Equipment")
def click_pay_transfer_billpayment_HitaciCapitalEquipment(context):
    context.app.b_Pay_Transfer.click_pay_transfer_billpayment_HitaciCapitalEquipment()


















# Apply for new account
@then("I Click Apply for new account")
def click_apply_for_new_account(context):
    context.app.c_Apply_For_New_Account.click_apply_for_new_account()

# Fix Deposit Placement
@then("I Click Fixed Deposit Placement")
def click_fixed_deposit_placement(context):
    context.app.d_Fixed_Deposit_Placement.click_fixed_deposit_placement()


# Authentication
@then("I Click Authentication")
def click_authentication(context):
    context.app.e_Authentication.click_authentication()

@then("I Click Change PIN and Fill Old PIN")
def click_change_pin_and_fill_old_pin(context):
    context.app.e_Authentication.click_change_pin_and_fill_old_pin()

@then("I Click Change PIN and Fill New PIN")
def click_change_pin_and_fill_new_pin(context):
    context.app.e_Authentication.click_change_pin_and_fill_new_pin()


# Notification
@then("I Click Notification")
def click_notification(context):
    context.app.f_Notification.click_notification()

@then("I Click Uncheck 3 Notification")
def click_3_notification_uncheck(context):
    context.app.f_Notification.click_3_notification_uncheck()

@then("I Click Check 3 Notification")
def click_3_notification_check(context):
    context.app.f_Notification.click_3_notification_check()



# Daily Limit
@then("I Click Daily Limit")
def click_daily_limit(context):
    context.app.g_Daily_Limit_Setting.click_daily_limit()

@then("I Clear Field and Fill Field Daily Limit")
def set_null_field_and_fill_field(context):
    context.app.g_Daily_Limit_Setting.set_null_field_and_fill_field()

@then("I Clear Field and Fill Field Under Daily Limit")
def set_under_daily_limit_fill_field(context):
    context.app.g_Daily_Limit_Setting.set_under_daily_limit_fill_field()

@then("I Clear Field and Fill Field Upper Daily Limit")
def set_upper_daily_limit_fill_field(context):
    context.app.g_Daily_Limit_Setting.set_upper_daily_limit_fill_field()





# Frequently Asked Question
@then("I Click Frequently Asked Question")
def click_frequently_asked_question(context):
    context.app.h_Frequently_Asked_Question.click_frequently_asked_question()

# RHB Mobile SG App
@then("I Click RHB Mobile SG App")
def click_rhb_mobile_sg_app(context):
    context.app.i_RHB_Mobile_SG_App.click_rhb_mobile_sg_app()

# Personal Information
@then("I Click Personal Information")
def click_personal_information(context):
    context.app.j_Personal_Information.click_personal_information()

# Sign Out
@then("I Click Sign Out")
def click_sign_out(context):
    context.app.k_Sign_Out.click_sign_out()














