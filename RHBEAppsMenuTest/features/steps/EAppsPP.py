from behave import given, when, then


@then("I Click Product PP")
def choose_product(context):
    context.app.EAppsMyKadPP.choose_product()

@then("I Fill Form Account Detail PP")
def fill_form_account_detail_pp(context):
    context.app.EAppsMyKadPP.fill_form_account_detail_pp()

@then("I Click Next Contact Detail PP")
def fill_form_contact_detail_trio(context):
    context.app.EAppsMyKadPP.fill_form_contact_detail_trio()

@then("I Fill Employement Detail PP")
def fill_form_employment_status_employed_pp(context):
    context.app.EAppsMyKadPP.fill_form_employment_status_employed_pp()

@then("I Fill Declaration PP")
def fill_declaration_pp(context):
    context.app.EAppsMyKadPP.fill_declaration_pp()



