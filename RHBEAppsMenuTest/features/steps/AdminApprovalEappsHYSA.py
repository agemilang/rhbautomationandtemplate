from behave import given, when, then
import requests
import json
from credentials import  email, email1, email2, email3, email4, email5, email6, email7, url


@given(u'I POST admin login Customer Info HYSA')
def login_admin_name_screening_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s90",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintokenmanagement = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer EApps Get Identity No And ID Name Customer Management HYSA')
    def get_list_customer_onboarding_name_screening_maker(context):
        headers = {"Authorization": "Bearer" + admintokenmanagement}
        response = requests.get(url + '/api/v1/customer/search/management?username=pp2@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])

        identityno = response.json()['content'][0]['identityNo']

        last_char = identityno[-9:]
        print(last_char)


        id = response.json()['content'][0]['id']
        print("\n")
        assert response.status_code == 200

        @then(u'I POST admin login Name Screening Maker HYSA')
        def login_admin_name_screening_maker(context):
            payload = {
                "grant_type": "password",
                "username": "d0150c101",
                "password": "mm",
                "scope": "read",
                "client_id": "backoffice"
            }

            headers = {"Authorization": "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
            response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
            print(response)
            print("\n")
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            # print(response.json()['access_token'])
            admintoken = response.json()['access_token']

            assert response.status_code == 200

            @then(u'I Get List Task Approval EApps Get Task ID Name Screening Maker HYSA')
            def get_list_task_approval_onboarding_name_screening_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/e_application?objectId=' + last_char,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                # Body json
                # print(response.json()['content'][0]['id'])
                # id = response.json()['content'][0]['id']
                json_data = json.loads(response.text)
                json_formatted_str = json.dumps(json_data, indent=2)
                print(json_formatted_str)
                print("\n")
                assert response.status_code == 200


                @then(u'I Get List Task Detail Approval EApps Get Onboard ID Name Screening Maker HYSA')
                def get_list_task_detail_approval_onboarding_name_screening_maker(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v2/apprv/onboarding/e_application' + id,
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    print(response.json()['objectId'])
                    object_id = response.json()['objectId']
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    assert response.status_code == 200
                    print("\n")
#
#
                @then(u'I Get List Task Approval EApps Get Reason ID Name Screening Maker HYSA')
                def get_list_task_approval_onboarding_name_screening_maker(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v1/reason/droplist/l1',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    print(response.json()[9]['id'])
                    reason_id = response.json()[9]['id']
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")
                    assert response.status_code == 200
#
#
                    @then(u'I Post Action Task Approval EApps Name Screening Maker HYSA')
                    def post_action_task_approval_onboarding_name_screening_maker(context):

                        payload = {

                            "action": "recommend",
                            "comment": "hahahahha",
                            "id": id,
                            "reasonId": reason_id
                        }

                        headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                        response = requests.post(url + '/api/v2/apprv/onboarding/e_application', data=json.dumps(payload),
                                                 headers=headers, verify=False)
                        print(response)
                        print("\n")

                        assert response.status_code == 200
#
#
#
#
@given(u'I POST admin login Name Screening Checker HYSA')
def login_admin_name_screening_checker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s91",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer EApps Get Identity No And ID Name Screening Checker HYSA')
    def get_list_customer_onboarding_name_screening_checker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/management?username=pp2@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])

        identityno = response.json()['content'][0]['identityNo']
        #
        id = identityno[-9:]
        print(id)
        # json_data = json.loads(response.text)
        # json_formatted_str = json.dumps(json_data, indent=2)
        # print(json_formatted_str)
        print("\n")

        assert response.status_code == 200


        @then(u'I Get List Task Approval EApps Get Task ID Name Screening Checker HYSA')
        def get_list_task_approval_onboarding_name_screening_checker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/e_application?objectId=' + id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            print(response.json()['content'][0]['id'])
            id_screening_checker = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")
            assert response.status_code == 200


            @then(u'I Get List Task Detail Approval EApps Get Onboard ID Name Screening Checker HYSA')
            def get_list_task_detail_approval_onboarding_name_screening_checker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/e_application/' + id_screening_checker,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['id'])
                id_onboard = response.json()['id']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")
                assert response.status_code == 200



                @then(u'I Post Action Task Approval EApps Name Screening Checker HYSA')
                def post_action_task_approval_onboarding_name_screening_checker(context):

                    payload = {

                        "action": "recommend",
                        "comment": "hahahahha",
                        "id": id_screening_checker,
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/e_application', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200
#
#
#
#
# @given(u'I POST admin login EDD')
# def login_admin_edd(context):
#     payload = {
#         "grant_type": "password",
#         "username": "d0150s105",
#         "password": "mm",
#         "scope": "read",
#         "client_id": "backoffice"
#     }
#
#     headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
#     response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
#     print(response)
#     print("\n")
#     # json_data = json.loads(response.text)
#     # json_formatted_str = json.dumps(json_data, indent=2)
#     # print(json_formatted_str)
#     # print(response.json()['access_token'])
#     admintoken = response.json()['access_token']
#
#     assert response.status_code == 200
#
#     @then(u'I Get List Customer Onboarding Get Identity No And ID EDD')
#     def get_list_customer_onboarding_edd(context):
#         headers = {"Authorization": "Bearer" + admintoken}
#         response = requests.get(url + '/api/v1/customer/search/onboarding?username=pp2@yopmail.com',
#                                 headers=headers, verify=False)
#         # Code response
#         print(response)
#         # Body json
#         # print(response.text)
#         print(response.json()['content'][0]['identityNo'])
#         identityno = response.json()['content'][0]['identityNo']
#         print("\n")
#         assert response.status_code == 200
#
#
#         @then(u'I Get List Task Approval Onboarding Get Task ID EDD')
#         def get_list_task_approval_onboarding_edd(context):
#             headers = {"Authorization": "Bearer" + admintoken}
#             response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
#                                     headers=headers, verify=False)
#             # Code response
#             print(response)
#             # Body json
#             # print(response.json()['content'][0]['id'])
#             task_id = response.json()['content'][0]['id']
#             # json_data = json.loads(response.text)
#             # json_formatted_str = json.dumps(json_data, indent=2)
#             # print(json_formatted_str)
#             print("\n")
#             assert response.status_code == 200
#
#
#             @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID EDD')
#             def get_list_task_detail_approval_onboarding_edd(context):
#                 headers = {"Authorization": "Bearer" + admintoken}
#                 response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
#                                         headers=headers, verify=False)
#                 # Code response
#                 print(response)
#                 print(response.json()['object']['onboardId'])
#                 id_onboard = response.json()['object']['onboardId']
#                 # Body json
#                 # json_data = json.loads(response.text)
#                 # json_formatted_str = json.dumps(json_data, indent=2)
#                 # print(json_formatted_str)
#
#                 print("\n")
#                 assert response.status_code == 200
#
#                 @then(u'I Post Upload Document Approval Onboarding EDD')
#                 def post_upload_document_approval_onboarding_edd(context):
#                     files = {'file': open('DeskripsiMaskot.pdf', 'rb')}
#
#                     payload = {
#
#                         "onboardId": id_onboard,
#                         "taskName": "Enhanced Due Dilligence",
#                         "documentType": "APPROVAL_DOCUMENT"
#                     }
#
#                     headers = {"Authorization": "Bearer " + admintoken}
#                     response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
#                                              headers=headers, verify=False)
#                     print(response)
#                     print("\n")
#
#                     assert response.status_code == 200
#
#                 @then(u'I Post Action Task Approval Onboarding EDD')
#                 def post_action_task_approval_onboarding_edd(context):
#
#                     payload = {
#                         "action": "approve",
#                         "comment": "",
#                         "id": task_id
#                     }
#
#                     headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
#                     response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
#                                              headers=headers, verify=False)
#                     print(response)
#                     print("\n")
#
#                     assert response.status_code == 200