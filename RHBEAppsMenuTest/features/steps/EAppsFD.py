from behave import given, when, then


@then("I Click Product FD")
def choose_product(context):
    context.app.EAppsMyKadFD.choose_product()

@then("I Fill Form Account Detail FD")
def fill_form_account_detail_fix_deposit(context):
    context.app.EAppsMyKadFD.fill_form_account_detail_fix_deposit()

@then("I Fill Form Placement Detail FD")
def fill_form_placement_detail_fix_deposit(context):
    context.app.EAppsMyKadFD.fill_form_placement_detail_fix_deposit()

@then("I Fill Form Employment Detail FD")
def fill_form_employment_detail_Employed_fix_deposit(context):
    context.app.EAppsMyKadFD.fill_form_employment_detail_Employed_fix_deposit()

@then("I Fill Form Declaration FD")
def fill_declaration_fix_deposit(context):
    context.app.EAppsMyKadFD.fill_declaration_fix_deposit()

@then("I Fill PIN Code Payment")
def app_pin_code(context):
    context.app.EAppsMyKadFD.app_pin_code()