from behave import given, when, then

@then("I Click Skip Button IOS")
def click_skip_button(context):
    context.app.SliderFD.click_skip_button()

@then("I Choose Slider FD IOS")
def select_fixed_deposit(context):
    context.app.SliderFD.select_fixed_deposit()