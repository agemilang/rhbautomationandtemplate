from behave import given, when, then


@then("I Click Product HYSA")
def choose_product(context):
    context.app.EAppsMyKadHYSA.choose_product()

@then("I Fill Form Account Detail HYSA")
def fill_form_account_detail_trio(context):
    context.app.EAppsMyKadHYSA.fill_form_account_detail_hysa()