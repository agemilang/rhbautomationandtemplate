from behave import given, when, then

@when("I Click Hamburger Menu IOS")
def click_hamburger_menu(context):
    context.app.a_MyAccount_IOS.click_hamburger_menu()

@then("I Click Hamburger Menu IOS")
def click_hamburger_menu(context):
    context.app.a_MyAccount_IOS.click_hamburger_menu()


@when("I Click Hamburger Menu My Account 2x IOS")
def click_hamburger_menu_my_account_2x(context):
    context.app.a_MyAccount_IOS.click_hamburger_menu_my_account_2x()


# Pay/Transfer
@then("I Click Pay / Transfer IOS")
def click_pay_transfer(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer()

@then("I Click Own Account IOS")
def click_pay_transfer_own_account(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_own_account()

@then("I Check Alert Empty and Fill Own Account IOS")
def check_alert_own_account_and_fill(context):
    context.app.b_Pay_Transfer_IOS.check_alert_own_account_and_fill()



@then("I Click Next Confirm your Transaction IOS")
def click_next_confirm_your_transaction_own_account(context):
    context.app.b_Pay_Transfer_IOS.click_next_confirm_your_transaction_own_account()

@then("I Check Message Succesful Click Next Make Another Payment IOS")
def check_message_succesful_click_next_make_another_payment_ios(context):
    context.app.b_Pay_Transfer_IOS.check_message_succesful_click_next_make_another_payment_ios()

@then("I Check Message Submitted Click Next Make Another Payment IOS")
def check_message_submitted_click_next_make_another_payment(context):
    context.app.b_Pay_Transfer_IOS.check_message_submitted_click_next_make_another_payment()





@then("I Click Other Account IOS")
def click_pay_transfer_other_account_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_other_account_ios()

@then("I Check Alert Empty and Fill Other Account IOS")
def check_alert_other_account_and_fill(context):
    context.app.b_Pay_Transfer_IOS.check_alert_other_account_and_fill()


@then("I Fill PIN Code Payment False IOS")
def app_pin_false_code(context):
    context.app.b_Pay_Transfer_IOS.app_pin_false_code()

@then("I Fill PIN Code Payment True IOS")
def app_pin_right_code(context):
    context.app.b_Pay_Transfer_IOS.app_pin_right_code()

@then("I Click Next Make Bill Payment SINGTEL IOS")
def click_pay_transfer_billpayment_singtel_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_billpayment_singtel_ios()

@then("I Click Next Make Bill Payment M1 IOS")
def click_pay_transfer_billpayment_m1_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_billpayment_m1_ios()

@then("I Click Next Make Bill Payment Starhub IOS")
def click_pay_transfer_billpayment_starhub_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_billpayment_starhub_ios()

@then("I Click Next Make Bill Payment Sunpage IOS")
def click_pay_transfer_billpayment_sunpage_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_billpayment_sunpage_ios()

@then("I Click Next Make Bill Payment I Click Apply for new account IOS")
def click_pay_transfer_billpayment_zone_ios(context):
    context.app.b_Pay_Transfer_IOS.click_pay_transfer_billpayment_zone_ios()



# Apply for new account
@then("I Click Apply for new account IOS")
def click_apply_for_new_account(context):
    context.app.c_Apply_For_New_Account_IOS.click_apply_for_new_account()

# Fix Deposit Placement
@then("I Click Fixed Deposit Placement IOS")
def click_fixed_deposit_placement(context):
    context.app.d_Fixed_Deposit_Placement_IOS.click_fixed_deposit_placement()


# Authentication
@then("I Click Authentication IOS")
def click_authentication(context):
    context.app.e_Authentication_IOS.click_authentication()

@then("I Click Change PIN and Fill Old PIN IOS")
def click_change_pin_and_fill_old_pin(context):
    context.app.e_Authentication_IOS.click_change_pin_and_fill_old_pin()

@then("I Click Change PIN and Fill New PIN IOS")
def click_change_pin_and_fill_new_pin(context):
    context.app.e_Authentication_IOS.click_change_pin_and_fill_new_pin()


# Notification
@then("I Click Notification IOS")
def click_notification(context):
    context.app.f_Notification_IOS.click_notification()

@then("I Click Uncheck 3 Notification IOS")
def click_3_notification_uncheck(context):
    context.app.f_Notification_IOS.click_3_notification_uncheck()

@then("I Click Check 3 Notification IOS")
def click_3_notification_check(context):
    context.app.f_Notification_IOS.click_3_notification_check()



# Daily Limit
@then("I Click Daily Limit IOS")
def click_daily_limit(context):
    context.app.g_Daily_Limit_Setting_IOS.click_daily_limit()

@then("I Clear Field and Fill Field Daily Limit IOS")
def set_null_field_and_fill_field(context):
    context.app.g_Daily_Limit_Setting_IOS.set_null_field_and_fill_field()

@then("I Clear Field and Fill Field Under Daily Limit IOS")
def set_under_daily_limit_fill_field(context):
    context.app.g_Daily_Limit_Setting_IOS.set_under_daily_limit_fill_field()

@then("I Clear Field and Fill Field Upper Daily Limit IOS")
def set_upper_daily_limit_fill_field(context):
    context.app.g_Daily_Limit_Setting_IOS.set_upper_daily_limit_fill_field()





# Frequently Asked Question
@then("I Click Frequently Asked Question IOS")
def click_frequently_asked_question(context):
    context.app.h_Frequently_Asked_Question_IOS.click_frequently_asked_question()

# RHB Mobile SG App
@then("I Click RHB Mobile SG App IOS")
def click_rhb_mobile_sg_app(context):
    context.app.i_RHB_Mobile_SG_App_IOS.click_rhb_mobile_sg_app()

# Personal Information
@then("I Click Personal Information IOS")
def click_personal_information(context):
    context.app.j_Personal_Information_IOS.click_personal_information()

# Sign Out
@then("I Click Sign Out IOS")
def click_sign_out(context):
    context.app.k_Sign_Out_IOS.click_sign_out()














