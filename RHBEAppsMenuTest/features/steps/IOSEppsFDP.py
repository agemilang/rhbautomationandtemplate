from behave import given, when, then

@then("I Click Product FDP IOS")
def choose_product(context):
    context.app.EAppsMyKadFDPIOS.choose_product()

@then("I Fill FDP Form IOS")
def fill_fix_deposit_placement(context):
    context.app.EAppsMyKadFDPIOS.fill_fix_deposit_placement()

@then("I Check Maturity Instruction FD 1 IOS")
def check_maturity_instructionfd1(context):
    context.app.EAppsMyKadFDPIOS.check_maturity_instructionfd1()

@then("I Check Maturity Instruction FD 2 IOS")
def check_maturity_instructionfd2(context):
    context.app.EAppsMyKadFDPIOS.check_maturity_instructionfd2()







