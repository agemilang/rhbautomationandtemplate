from behave import given, when, then



@then("I Click Mobile Banking IOS")
def click_mobile_banking_ios(context):
    context.app.Home.click_mobile_banking_ios()

@then("I Click Center Menu Choose EApps IOS")
def click_center_menu(context):
    context.app.Home.click_center_menu()