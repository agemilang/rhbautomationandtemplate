from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

class SliderFD(Page):
    SKIP = (By.XPATH, '//XCUIElementTypeButton[@name="SKIP"]')

    COMBO_BOX_SAVING = (By.XPATH, '//XCUIElementTypeButton[@name="SAVINGS"]')
    FD = (By.XPATH, '//XCUIElementTypeButton[@name="Fixed Deposit"]')


    FD1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]')
    FD2 = (By.XPATH, '//XCUIElementTypeButton[@name="Fixed Deposit"]')



    def click_skip_button(self):
        time.sleep(5)
        self.click_on_element(self.SKIP)
        time.sleep(5)

    def select_fixed_deposit(self):

        self.click_on_element(self.COMBO_BOX_SAVING)
        self.click_on_element(self.FD)
        time.sleep(5)
        touch = TouchAction(self.driver)
        touch.press(x=348, y=226)   .move_to(x=46, y=223) .release()   .perform()
        time.sleep(15)






