from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class EAppsMyKadFDP(Page):

    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")
    BUTTON_NEXT2 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext2")

    DONE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/action_next")


    # Biometric
    MAYBE_LATER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btCancel')

    # Mobile Banking Button
    MB_BUTTON = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')


    # Center Menu
    HAMBURGER_MENU = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView')
    E_APPS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]')

    # Choose Product

    PRODUCT_FIX_DEPOSIT_PLACEMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llFdp')

    # Allow Location
    ALLOW_ALL_TIME = (By.ID, 'com.android.permissioncontroller:id/permission_allow_always_button')
    ALLOW_ONLY_USE_APP = (By.ID, 'com.android.permissioncontroller:id/permission_allow_foreground_only_button')
    DENY = (By.ID, 'com.android.permissioncontroller:id/permission_deny_button')


    # Fixed Deposit Placement
    PLACEMENT_TO = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvPlacementToName')
    PLACEMENT_TO_1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout')

    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    CURRENCY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout')



    TENURE_1 = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[1]")
    TENURE_2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[2]')
    TENURE_3 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[3]')
    TENURE_4 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[4]')

    PLACEMENT_AMOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')
    SOURCE_OF_FUNDS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout')
    SOURCE_OF_WEALTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout')

    FROM = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout')
    HYSA3 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]')



    MATURITY_INSTRUCTION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout')

    RENEW_PRINCIPAL_AND_INTEREST = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]')
    RENEW_PRINCIPAL = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]')
    DO_NOT_RENEW = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]')

    TO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[8]')

    TO1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout')


    ACCOUUNT_SUMMARY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSummary')


    def choose_product(self):
        self.click_on_element(self.PRODUCT_FIX_DEPOSIT_PLACEMENT)
        # self.click_on_element(self.ALLOW_ALL_TIME)
        time.sleep(5)


    def fill_fix_deposit_placement(self):
        self.click_on_element(self.PLACEMENT_TO)
        self.click_on_element(self.PLACEMENT_TO_1)

        self.click_on_element(self.TENURE_3)
        self.input("1000", self.PLACEMENT_AMOUNT)
        self.click_on_element(self.SOURCE_OF_FUNDS)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)

        self.click_on_element(self.SOURCE_OF_WEALTH)
        time.sleep(5)

        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.FROM)
        self.click_on_element(self.HYSA3)
        self.click_on_element(self.MATURITY_INSTRUCTION)
        self.click_on_element(self.DO_NOT_RENEW)
        self.click_on_element(self.TO)
        self.click_on_element(self.TO1)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.BUTTON_NEXT)

    def confirm_fix_deposit_placement(self):
        self.click_on_element(self.BUTTON_NEXT2)

    def click_account_summary(self):
        self.click_on_element(self.ACCOUUNT_SUMMARY)

