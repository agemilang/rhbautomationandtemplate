from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class PinIOS(Page):


    # PIN
    NO_1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther')
    NO_2 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther')
    NO_3 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther')
    NO_4 = (By.ID, 'four_icon')
    NO_5 = (By.ID, 'five_icon')
    NO_6 = (By.ID, 'six_icon')
    NO_7 = (By.ID, 'seven_icon')
    NO_8 = (By.ID, 'eight_icon')
    NO_9 = (By.ID, 'nine_icon')
    NO_0 = (By.ID, 'zero_icon')
    BACKSPACE = (By.ID, 'backspace_ic')

    ERROR_PIN_FALSE = (By.ID, "'Oops! Your PIN doesn't seem right. Try again.'")

    Forgot_PIN = (By.ID, "Forgot PIN")


    MAYBE_LATER = (By.ID, "MAYBE LATER")

    def pin(self):
        time.sleep(5)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)

        time.sleep(5)


    def wrong_pin(self):
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)

        time.sleep(5)

    def alert_pin_wrong(self):
        error_message_pin_wrong = self.find_elements(self.ERROR_PIN_FALSE)
        compare_data_with_expected(expected="Oops! Your PIN doesn't seem right. Try again.", real=error_message_pin_wrong[0].text)
        time.sleep(5)

    def click_maybe_later(self):
        self.click_on_element(self.MAYBE_LATER)
        time.sleep(15)
