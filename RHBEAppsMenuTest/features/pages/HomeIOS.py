from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class Home(Page):
    # Mobile Banking Button
    MB_BUTTON = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[3]')

    # Center Menu
    HAMBURGER_MENU = (By.ID, 'iconMenuIcMenuBubble')

    E_APPS = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[3]')
    LIMIT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[3]')
    PAY =(By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[3]')


    def click_mobile_banking_ios(self):
        time.sleep(5)
        self.click_on_element(self.MB_BUTTON)

        time.sleep(5)
    def click_center_menu(self):
        self.click_on_element(self.HAMBURGER_MENU)
        time.sleep(5)

    def click_eapps(self):
        self.click_on_element(self.HAMBURGER_MENU)
        time.sleep(5)

    def click_limit(self):
        self.click_on_element(self.HAMBURGER_MENU)
        time.sleep(5)

    def click_pay(self):
        self.click_on_element(self.HAMBURGER_MENU)
        time.sleep(5)






