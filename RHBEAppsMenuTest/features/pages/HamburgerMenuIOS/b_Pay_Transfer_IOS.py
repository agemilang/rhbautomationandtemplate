from credentials import Singtel_valid, M1_valid, Starhub_valid, Sunpage_valid, Zone1511_valid, IRAS_valid, TPF_valid, number4_generator, char3_generator, char1_generator, Stemcord_valid, StemcordInvoice_valid, female, No689_valid, HitaciDept1_valid
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class b_Pay_Transfer_IOS(Page):

    BUTTON_NEXT = (By.ID, "NEXT")
    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')

    BUTTON_CLOSE = (By.ID, "iconIcCloseDark")
    BACK = (By.ID, "back")


    BUTTON_BACK = (By.XPATH, '//android.widget.ImageButton[@content-desc="Navigate up"]')
    SEARCH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    CHOOSE_BANK = (By.ID, "RHB Bank Berhad")
    CHOOSE_STUDY = (By.ID, "Study")

    HAMBURGER_MENU = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btHamburgerMenu')
    HAMBURGER_MENU_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibHamburgerMenu')




    PAY_TRANSFER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')
    MAKE_NEW_PAYMENT = (By.ID, 'MAKE A NEW PAYMENT')
    MAKE_ANOTHER_PAYMENT = (By.ID, 'MAKE ANOTHER PAYMENT')

    # OWN ACCOUNT
    OWN_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeButton')

    # Alert Empty
    FROM_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvSourceOfFundError')
    TO_ACCOUNT_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvToAccountError')
    AMOUNT_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvErrorEditTextBox')


    # ERROR_FROM_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout')
    # ERROR_TO_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout')
    # # ERROR_AMOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    #
    #


    FROM_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]')
    ACCOUNT_1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton')
    ACCOUNT_2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]')
    ACCOUNT_3 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]')


    TO_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther')
    TO_ACCOUNT_1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]')
    AMOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    REMARK = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')

    # PIN
    PIN_CODE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    NO_SECURE_PIN_CODE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextField')

    SHOW_PIN_CODE = (By.XPATH, '//XCUIElementTypeButton[@name="eye"]')
    SUBMIT_PIN_CODE = (By.XPATH, '//XCUIElementTypeButton[@name="Submit"]')

    CLEAR_PIN = (By.XPATH, '//XCUIElementTypeButton[@name="cancel"]')

    ERROR_MESSAGE_PIN_FALSE = (By.ID, 'You entered a wrong App PIN Code')


    TRANSACTION_TITLE_SUCCESSFUL = (By.ID, 'Transaction Successful')
    TRANSACTION_TITLE_SUBMITTED= (By.ID, 'Transaction Submitted')
    # TRANSACTION_AMOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvAmount')

    # OTHER ACCOUNT
    OTHER_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    BENEFICIARY_BANK = (By.XPATH, '(//XCUIElementTypeButton[@name="bigButtonDropDownView"])[1]')
    BENEFICIARY_ACCOUNT_NUMBER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    BENEFICIARY_NAME = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    AMOUNT_OTHER_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    PURPOSE_OF_TRANSFER = (By.XPATH, '(//XCUIElementTypeButton[@name="bigButtonDropDownView"])[2]')
    REMARK_OTHER_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[8]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    SAVE_PAYEE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/swFavourites')

    # Alert Empty
    BENEFICIARY_BANK_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView')
    BENEFICIARY_ACCOUNT_NUMBER_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView')
    BENEFICIARY_NAME_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView')
    AMOUNT_OTHER_ACCOUNT_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.TextView')
    PURPOSE_OF_TRANSFER_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.TextView')






    # Bill Payment
    BILL_PAYMENT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]')

    FROM_BILL_PAYMENT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    HYSA_3 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton')

    BILLER_BILL_PAYMENT = (By.XPATH, '//XCUIElementTypeButton[@name="bigButtonDropDownView"]')

    CHOOSE_SINGTEL = (By.ID, "SingTel")
    CHOOSE_M1 = (By.ID, "M1")
    CHOOSE_Starhub = (By.ID, "Starhub")
    CHOOSE_Sunpage = (By.ID, "Sunpage (NextWave)")
    CHOOSE_Zone = (By.ID, "Zone 1511")



    FIELD1_BILLER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    FIELD2_BILLER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    FIELD3_BILLER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')








    def click_pay_transfer(self):
        self.click_on_element(self.PAY_TRANSFER)
        time.sleep(5)

        self.click_on_element(self.MAKE_NEW_PAYMENT)
        time.sleep(5)

    def check_alert_own_account_and_fill(self):
        self.click_on_element(self.OWN_ACCOUNT)
        # time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)

        # Check Alert Empty
        error_message_from_empty = self.find_elements(self.FROM_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_from_empty[0].text)

        error_message_to_account_empty = self.find_elements(self.TO_ACCOUNT_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_to_account_empty[0].text)

        error_message_amount_empty = self.find_elements(self.AMOUNT_EMPTY)
        compare_data_with_expected(
            expected="Amount must be greater than zero",
            real=error_message_amount_empty[0].text)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.ACCOUNT_1)

        self.click_on_element(self.TO_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.TO_ACCOUNT_1)
        self.input("10.15", self.AMOUNT)
        self.input("Transfer aja", self.REMARK)
        self.driver.hide_keyboard()
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)


    def click_pay_transfer_own_account(self):

        self.click_on_element(self.OWN_ACCOUNT)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.TO_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.TO_ACCOUNT_1)
        self.input("10.15", self.AMOUNT)
        self.input("Transfer aja", self.REMARK)
        self.driver.hide_keyboard()
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)


    def click_next_confirm_your_transaction_own_account(self):
        self.click_on_element(self.BUTTON_NEXT)

    def app_pin_false_code(self):
        self.input("995995", self.PIN_CODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.SHOW_PIN_CODE)
        self.click_on_element(self.SUBMIT_PIN_CODE)

        error_message_wrong_pin = self.find_elements(self.ERROR_MESSAGE_PIN_FALSE)
        compare_data_with_expected(expected="You entered a wrong App PIN Code",real=error_message_wrong_pin[0].text)
        self.click_on_element(self.CLEAR_PIN)
        time.sleep(15)

    def app_pin_right_code(self):
        self.input("321321", self.NO_SECURE_PIN_CODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.SHOW_PIN_CODE)
        self.click_on_element(self.SUBMIT_PIN_CODE)
        time.sleep(5)

    def check_message_succesful_click_next_make_another_payment(self):
        title_successful = self.find_elements(self.TRANSACTION_TITLE_SUCCESSFUL)
        compare_data_with_expected(
            expected="Transaction Successful",
            real=title_successful[0].text)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)
        self.click_on_element(self.MAKE_NEW_PAYMENT)
        self.click_on_element(self.BACK)

        time.sleep(5)

    def check_message_submitted_click_next_make_another_payment(self):
        title_successful = self.find_elements(self.TRANSACTION_TITLE_SUBMITTED)
        compare_data_with_expected(
            expected="Transaction Submitted",
            real=title_successful[0].text)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)
        self.click_on_element(self.MAKE_ANOTHER_PAYMENT)
        self.click_on_element(self.BACK)

        time.sleep(5)





    def check_alert_other_account_and_fill(self):
        self.click_on_element(self.OTHER_ACCOUNT)
        # time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(15)
        # Check Alert Empty
        error_message_from_empty = self.find_elements(self.FROM_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_from_empty[0].text)

        error_message_beneficiary_bank_empty = self.find_elements(self.BENEFICIARY_BANK_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_beneficiary_bank_empty[0].text)

        error_message_beneficiary_account_empty = self.find_elements(self.BENEFICIARY_ACCOUNT_NUMBER_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_beneficiary_account_empty[0].text)

        error_message_beneficiary_name_empty = self.find_elements(self.BENEFICIARY_NAME_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_beneficiary_name_empty[0].text)

        error_message_amount_empty = self.find_elements(self.AMOUNT_OTHER_ACCOUNT_EMPTY)
        compare_data_with_expected(
            expected="Amount must be greater than zero",
            real=error_message_amount_empty[0].text)

        error_message_purpose_transfer_empty = self.find_elements(self.PURPOSE_OF_TRANSFER_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_purpose_transfer_empty[0].text)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.HYSA_3)
        self.click_on_element(self.BENEFICIARY_BANK)
        self.input("RHB Bank Berhad", self.SEARCH)
        # self.find_element(self.SEARCH).clear()

        self.click_on_element(self.CHOOSE_BANK)
        self.input("000019810502005", self.BENEFICIARY_ACCOUNT_NUMBER)
        self.input("FDA 5", self.BENEFICIARY_NAME)
        self.input("10", self.AMOUNT_OTHER_ACCOUNT)
        self.click_on_element(self.PURPOSE_OF_TRANSFER)
        self.input("Study", self.SEARCH)
        self.click_on_element(self.CHOOSE_STUDY)
        self.input("Bayar Sekolah", self.REMARK_OTHER_ACCOUNT)
        # self.click_on_element(self.SAVE_PAYEE)
        self.click_on_element(self.BUTTON_NEXT)

        time.sleep(5)

    def click_pay_transfer_other_account_ios(self):

        self.click_on_element(self.OTHER_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.HYSA_3)
        self.click_on_element(self.BENEFICIARY_BANK)
        self.input("RHB Bank Berhad", self.SEARCH)
        self.click_on_element(self.CHOOSE_BANK)
        self.input("000019810502005", self.BENEFICIARY_ACCOUNT_NUMBER)
        self.input("FDA 5", self.BENEFICIARY_NAME)
        self.input("10", self.AMOUNT_OTHER_ACCOUNT)
        self.click_on_element(self.DONE)
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.PURPOSE_OF_TRANSFER)
        self.input("Study", self.SEARCH)
        self.click_on_element(self.CHOOSE_STUDY)
        self.input("Bayar Sekolah", self.REMARK_OTHER_ACCOUNT)
        # self.click_on_element(self.SAVE_PAYEE)
        self.click_on_element(self.DONE)
        self.click_on_element(self.BUTTON_NEXT)


        time.sleep(5)

    def click_pay_transfer_billpayment_singtel_ios(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        time.sleep(5)

        self.input("SingTel", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_SINGTEL)
        self.input(Singtel_valid, self.FIELD1_BILLER)
        self.input("10", self.FIELD2_BILLER)
        self.click_on_element(self.DONE)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_m1_ios(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("M1", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_M1)
        self.input(M1_valid, self.FIELD1_BILLER)
        self.input("10", self.FIELD2_BILLER)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_starhub_ios(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Starhub", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_Starhub)
        self.input(Starhub_valid, self.FIELD1_BILLER)
        self.input("10", self.FIELD2_BILLER)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_sunpage_ios(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Sunpage", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_Sunpage)
        self.input(Sunpage_valid, self.FIELD1_BILLER)
        self.input("10", self.FIELD2_BILLER)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_zone_ios(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Zone", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_Zone)
        self.input(Zone1511_valid, self.FIELD1_BILLER)
        self.input("10", self.FIELD2_BILLER)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_iras_payment(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("IRAS - Pay", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(IRAS_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_tpf(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Traffic", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(TPF_valid, self.FIELD1)
        self.input(char3_generator()+number4_generator()+char1_generator(), self.FIELD2)
        self.input("10", self.FIELD3)

        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)