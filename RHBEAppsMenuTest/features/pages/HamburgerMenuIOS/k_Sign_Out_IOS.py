from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class k_Sign_Out_IOS(Page):
    SIGN_OUT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[16]/XCUIElementTypeOther/XCUIElementTypeButton')
    CANCEL = (By.ID, 'android:id/button2')
    CONFIRM_SIGN_OUT = (By.ID, 'android:id/button1')

    B1 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[1]')
    B2 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[2]')
    B3 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[3]')
    B4 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[4]')
    B5 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[5]')

    RT = (By.ID, 'Response Time')
    N = (By.ID, 'Navigation')
    AF = (By.ID, 'App Features')
    EU = (By.ID, 'Ease of Use')
    LF = (By.ID, 'Look & Feel')
    NS = (By.ID, 'No. of Steps')

    TB = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView')

    SUBMIT = (By.XPATH, '//XCUIElementTypeButton[@name="Submit"]')


    def click_sign_out(self):
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=45).release().perform()
        self.click_on_element(self.SIGN_OUT)
        # self.click_on_element(self.CANCEL)
        # self.click_on_element(self.SIGN_OUT)
        # self.click_on_element(self.CONFIRM_SIGN_OUT)
        time.sleep(5)
        self.click_on_element(self.B5)
        time.sleep(5)
        self.click_on_element(self.SUBMIT)

    # def click_sign_out(self):
    #     self.driver.swipe(15, 1844, 15, 560)
    #     self.click_on_element(self.SIGN_OUT)
    #     self.click_on_element(self.CANCEL)
    #     self.click_on_element(self.SIGN_OUT)
    #     self.click_on_element(self.CONFIRM_SIGN_OUT)
    #     time.sleep(15)
