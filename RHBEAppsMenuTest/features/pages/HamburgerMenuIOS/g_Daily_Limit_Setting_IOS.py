from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class g_Daily_Limit_Setting(Page):
    DAILY_LIMIT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[9]/XCUIElementTypeOther/XCUIElementTypeButton')



    LOCAL_TRANSFER = (By.XPATH, '(//XCUIElementTypeTextField[@name="txtInput"])[1]')
    BILL_PAYMENT = (By.XPATH, '(//XCUIElementTypeTextField[@name="txtInput"])[2]')

    SUBMIT_CHANGED = (By.XPATH, '//XCUIElementTypeButton[@name="SAVE CHANGES"]')

    ALERT_LIMIT = (By.XPATH, '//XCUIElementTypeStaticText[@name="Oops! Transfer Limit must be between 4,999 and 50,000 on Payment with currency SGD. Please try again."]')



    def click_daily_limit(self):
        self.click_on_element(self.DAILY_LIMIT)
        # time.sleep(15)


    # def set_null_field_and_fill_field(self):
    #
    #     self.find_element(self.LOCAL_TRANSFER).clear()
    #     self.find_element(self.BILL_PAYMENT).clear()
    #
    #     self.click_on_element(self.SUBMIT_CHANGED)
    #
    #
    #     self.input("6000", self.LOCAL_TRANSFER)
    #     self.click_on_element(self.SUBMIT_CHANGED)
    #
    #     self.input("6000", self.BILL_PAYMENT)
    #     self.click_on_element(self.SUBMIT_CHANGED)



    def set_under_daily_limit_fill_field(self):

        self.input("1000", self.LOCAL_TRANSFER)
        self.click_on_element(self.SUBMIT_CHANGED)


        self.input("1000", self.BILL_PAYMENT)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_under_limit = self.find_elements(self.ALERT_LIMIT)
        compare_data_with_expected(expected="Oops!\nTransfer Limit must be between 5,000.00 and 50,000.00 on Payment with currency SGD. Please try again.", real=error_message_under_limit[0].text)

    def set_upper_daily_limit_fill_field(self):

        self.input("100000", self.LOCAL_TRANSFER)
        self.click_on_element(self.SUBMIT_CHANGED)


        self.input("100000", self.BILL_PAYMENT)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_under_limit = self.find_elements(self.ALERT_LIMIT)
        compare_data_with_expected(expected="Oops!\nTransfer Limit must be between 5,000.00 and 50,000.00 on Payment with currency SGD. Please try again.", real=error_message_under_limit[0].text)

        time.sleep(15)


