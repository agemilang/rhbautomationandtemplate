from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class j_Personal_Information(Page):
    PERSONAL_INFORMATION = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[15]/XCUIElementTypeOther/XCUIElementTypeButton')
    BUTTON_BACK = (By.XPATH, '//XCUIElementTypeButton[@name="icBackDark"]')

    def click_personal_information(self):
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.PERSONAL_INFORMATION)
        self.click_on_element(self.BUTTON_BACK)

        time.sleep(15)

