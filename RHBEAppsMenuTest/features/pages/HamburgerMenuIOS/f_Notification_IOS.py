from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class f_Notification(Page):
    NOTIFICATION = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[8]/XCUIElementTypeOther/XCUIElementTypeButton')

    SMS_NOTIFICATION = (By.XPATH, '//XCUIElementTypeSwitch[@name="SMS notifications, Receive SMS notification on transaction activities"]')
    EMAIL_NOTIFICATION = (By.XPATH, '//XCUIElementTypeSwitch[@name="Email notifications, Receive email notification on transaction activities"]')
    APP_NOTIFICATION = (By.XPATH, '//XCUIElementTypeSwitch[@name="App notifications, Turn ON Push Notification of your card and currency balance"]')

    BACK_NOTIFICATION = (By.XPATH, '//XCUIElementTypeButton[@name="back"]')



    def click_notification(self):
        self.click_on_element(self.NOTIFICATION)

    def click_3_notification_uncheck(self):
        self.click_on_element(self.SMS_NOTIFICATION)
        self.click_on_element(self.EMAIL_NOTIFICATION)
        self.click_on_element(self.APP_NOTIFICATION)
        self.click_on_element(self.BACK_NOTIFICATION)


    def click_3_notification_check(self):
        self.click_on_element(self.NOTIFICATION)
        self.click_on_element(self.SMS_NOTIFICATION)
        self.click_on_element(self.EMAIL_NOTIFICATION)
        self.click_on_element(self.APP_NOTIFICATION)
        self.click_on_element(self.BACK_NOTIFICATION)

        time.sleep(15)


# lanjut Eapss FIX DEPOSIT PLACEMENT

