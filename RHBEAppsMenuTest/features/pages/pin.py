from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class Pin(Page):


    # PIN
    NO_1 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btOne')
    NO_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btTwo')
    NO_3 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btThree')
    NO_4 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFour')
    NO_5 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFive')
    NO_6 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSix')
    NO_7 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSeven')
    NO_8 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btEight')
    NO_9 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNine')
    NO_0 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btZero')
    BACKSPACE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btBackscape')

    ERROR_PIN_FALSE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvEnterPin')

    # Skip Activate Soft Token
    SKIP_ACTIVATION = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btSkipSoftTokenActivation")

    def pin(self):
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)

        time.sleep(5)


    def wrong_pin(self):
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_3)

        time.sleep(5)

    def alert_pin_wrong(self):
        error_message_pin_wrong = self.find_elements(self.ERROR_PIN_FALSE)
        compare_data_with_expected(expected="Oops! Your PIN doesn't seem right. Try again.", real=error_message_pin_wrong[0].text)
        time.sleep(5)

    def skipactivation(self):
        self.click_on_element(self.SKIP_ACTIVATION)