from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class k_Sign_Out(Page):
    SIGN_OUT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout')
    CANCEL = (By.ID, 'android:id/button2')
    CONFIRM_SIGN_OUT = (By.ID, 'android:id/button1')


    def click_sign_out(self):
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.SIGN_OUT)
        self.click_on_element(self.CANCEL)
        self.click_on_element(self.SIGN_OUT)
        self.click_on_element(self.CONFIRM_SIGN_OUT)
        time.sleep(15)

