from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class a_MyAccount(Page):


    HAMBURGER_MENU = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btHamburgerMenu')
    HAMBURGER_MENU_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibHamburgerMenu')


    MY_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout')



    PAY_TRANSFER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout')
    MAKE_NEW_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNewPayment')

    OWN_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llOwnAccount')
    OTHER_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llSomeone')
    BILL_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llBill')


    def click_hamburger_menu(self):
        self.click_on_element(self.HAMBURGER_MENU)

        time.sleep(5)
        # self.click_on_element(self.MY_ACCOUNT)
        # self.click_on_element(self.HAMBURGER_MENU_2)
        # self.click_on_element(self.MY_ACCOUNT)
        # self.click_on_element(self.HAMBURGER_MENU_2)

    def click_hamburger_menu_my_account_2x(self):
        self.click_on_element(self.HAMBURGER_MENU)
        self.click_on_element(self.MY_ACCOUNT)
        self.click_on_element(self.HAMBURGER_MENU_2)
        self.click_on_element(self.MY_ACCOUNT)


