from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class f_Notification(Page):
    NOTIFICATION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout')

    SMS_NOTIFICATION = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/swSmsNotifications')
    EMAIL_NOTIFICATION = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/swEmailNotifications')
    APP_NOTIFICATION = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/swMobileNotifications')

    BACK_NOTIFICATION = (By.XPATH, '//android.widget.ImageButton[@content-desc="Navigate up"]')



    def click_notification(self):
        self.click_on_element(self.NOTIFICATION)

    def click_3_notification_uncheck(self):
        self.click_on_element(self.SMS_NOTIFICATION)
        self.click_on_element(self.EMAIL_NOTIFICATION)
        self.click_on_element(self.APP_NOTIFICATION)
        self.click_on_element(self.BACK_NOTIFICATION)


    def click_3_notification_check(self):
        self.click_on_element(self.NOTIFICATION)
        self.click_on_element(self.SMS_NOTIFICATION)
        self.click_on_element(self.EMAIL_NOTIFICATION)
        self.click_on_element(self.APP_NOTIFICATION)
        self.click_on_element(self.BACK_NOTIFICATION)

        time.sleep(15)


# lanjut Eapss FIX DEPOSIT PLACEMENT

