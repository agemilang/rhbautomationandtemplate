from credentials import Singtel_valid, M1_valid, Starhub_valid, Sunpage_valid, Zone1511_valid, IRAS_valid, TPF_valid, number4_generator, char3_generator, char1_generator, Stemcord_valid, StemcordInvoice_valid, female, No689_valid, HitaciDept1_valid
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class b_Pay_Transfer(Page):

    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")
    BUTTON_CLOSE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/ibClose")

    BUTTON_BACK = (By.XPATH, '//android.widget.ImageButton[@content-desc="Kembali ke atas"]')
    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")

    HAMBURGER_MENU = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btHamburgerMenu')
    HAMBURGER_MENU_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibHamburgerMenu')

    PAY_TRANSFER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout')
    MAKE_NEW_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNewPayment')


    # OWN ACCOUNT
    OWN_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llOwnAccount')

    # Alert Empty
    FROM_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvSourceOfFundError')
    TO_ACCOUNT_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvToAccountError')
    AMOUNT_EMPTY = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvErrorEditTextBox')


    # ERROR_FROM_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout')
    # ERROR_TO_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout')
    # # ERROR_AMOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    #
    #


    FROM_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvSourceFundName')
    HYSA_1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout')
    HYSA_2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]')
    HYSA_3 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]')


    TO_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvToAccountName')
    AMOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    REMARK = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # PIN
    PIN_CODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SHOW_PIN_CODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView[1]')
    SUBMIT_PIN_CODE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSubmit')

    CLEAR_PIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ivClose')

    ERROR_MESSAGE_PIN_FALSE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvEmptyPassword')


    TRANSACTION_TITLE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvTitle')
    TRANSACTION_AMOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvAmount')

    # OTHER ACCOUNT
    OTHER_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llSomeone')
    BENEFICIARY_BANK = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout')
    BENEFICIARY_ACCOUNT_NUMBER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    BENEFICIARY_NAME = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    AMOUNT_OTHER_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PURPOSE_OF_TRANSFER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout')
    REMARK_OTHER_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SAVE_PAYEE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/swFavourites')

    # Alert Empty
    BENEFICIARY_BANK_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView')
    BENEFICIARY_ACCOUNT_NUMBER_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView')
    BENEFICIARY_NAME_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView')
    AMOUNT_OTHER_ACCOUNT_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.TextView')
    PURPOSE_OF_TRANSFER_EMPTY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.TextView')






    # Bill Payment
    BILL_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llBill')
    FROM_BILL_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llSourceOfFund')
    BILLER_BILL_PAYMENT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llDropdown')


    # SingTel
    FIELD1 = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    FIELD2 = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    FIELD3 = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    FIELD4 = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    FIELD5 = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    def click_pay_transfer(self):
        self.click_on_element(self.PAY_TRANSFER)
        time.sleep(5)

    def click_makenewpayment(self):
        self.click_on_element(self.MAKE_NEW_PAYMENT)

        time.sleep(5)

    def check_alert_own_account_and_fill(self):
        self.click_on_element(self.OWN_ACCOUNT)
        # time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)

        # Check Alert Empty
        error_message_from_empty = self.find_elements(self.FROM_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_from_empty[0].text)

        error_message_to_account_empty = self.find_elements(self.TO_ACCOUNT_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_to_account_empty[0].text)

        error_message_amount_empty = self.find_elements(self.AMOUNT_EMPTY)
        compare_data_with_expected(
            expected="Amount must be greater than zero",
            real=error_message_amount_empty[0].text)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.TO_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.HYSA_1)
        self.input("10.15", self.AMOUNT)
        self.input("Transfer aja", self.REMARK)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)


    def click_pay_transfer_own_account(self):

        self.click_on_element(self.OWN_ACCOUNT)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.TO_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.HYSA_1)
        self.input("10.15", self.AMOUNT)
        self.input("Transfer aja", self.REMARK)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)


    def click_next_confirm_your_transaction_own_account(self):
        self.click_on_element(self.BUTTON_NEXT)

    def app_pin_false_code(self):
        self.input("995995", self.PIN_CODE)
        self.click_on_element(self.SHOW_PIN_CODE)
        self.click_on_element(self.SUBMIT_PIN_CODE)

        error_message_wrong_pin = self.find_elements(self.ERROR_MESSAGE_PIN_FALSE)
        compare_data_with_expected(expected="Please ensure your App PIN Code is keyed in correctly.\n\nFor assistance, please call Customer Support.",real=error_message_wrong_pin[0].text)
        self.click_on_element(self.CLEAR_PIN)

    def app_pin_right_code(self):
        self.input("321321", self.PIN_CODE)
        self.click_on_element(self.SHOW_PIN_CODE)
        self.click_on_element(self.SUBMIT_PIN_CODE)
        time.sleep(5)


    def check_success_click_next_make_another_payment(self):
        title_successful = self.find_elements(self.TRANSACTION_TITLE)
        compare_data_with_expected(
            expected="Transaction Successful",
            real=title_successful[0].text)
        self.click_on_element(self.BUTTON_NEXT)
        self.click_on_element(self.BUTTON_BACK)

        time.sleep(5)

    def check_submit_click_next_make_another_payment(self):
        title_successful = self.find_elements(self.TRANSACTION_TITLE)
        compare_data_with_expected(
            expected="Transaction Submitted",
            real=title_successful[0].text)
        self.click_on_element(self.BUTTON_NEXT)
        self.click_on_element(self.BUTTON_BACK)

        time.sleep(5)
        # self.driver.close_app()

    def check_alert_other_account_and_fill(self):
        self.click_on_element(self.OTHER_ACCOUNT)
        # time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(15)
        # Check Alert Empty
        error_message_from_empty = self.find_elements(self.FROM_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_from_empty[0].text)

        error_message_beneficiary_bank_empty = self.find_elements(self.BENEFICIARY_BANK_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_beneficiary_bank_empty[0].text)

        error_message_beneficiary_account_empty = self.find_elements(self.BENEFICIARY_ACCOUNT_NUMBER_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_beneficiary_account_empty[0].text)

        error_message_beneficiary_name_empty = self.find_elements(self.BENEFICIARY_NAME_EMPTY)
        compare_data_with_expected(
            expected="Please fill up this field",
            real=error_message_beneficiary_name_empty[0].text)

        error_message_amount_empty = self.find_elements(self.AMOUNT_OTHER_ACCOUNT_EMPTY)
        compare_data_with_expected(
            expected="Amount must be greater than zero",
            real=error_message_amount_empty[0].text)

        error_message_purpose_transfer_empty = self.find_elements(self.PURPOSE_OF_TRANSFER_EMPTY)
        compare_data_with_expected(
            expected="Please select an option",
            real=error_message_purpose_transfer_empty[0].text)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.HYSA_3)
        self.click_on_element(self.BENEFICIARY_BANK)
        self.input("RHB Bank Berhad", self.SEARCH)
        self.find_element(self.SEARCH).clear()

        self.click_on_element(self.CHOOSE_ROW1)
        self.input("000019810502005", self.BENEFICIARY_ACCOUNT_NUMBER)
        self.input("FDA 5", self.BENEFICIARY_NAME)
        self.input("10", self.AMOUNT_OTHER_ACCOUNT)
        self.click_on_element(self.PURPOSE_OF_TRANSFER)
        self.input("Study", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input("Bayar Sekolah", self.REMARK_OTHER_ACCOUNT)
        # self.click_on_element(self.SAVE_PAYEE)
        self.click_on_element(self.BUTTON_NEXT)

        time.sleep(5)

    def click_pay_transfer_other_account(self):

        self.click_on_element(self.OTHER_ACCOUNT)
        time.sleep(5)

        self.click_on_element(self.FROM_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.HYSA_3)
        self.click_on_element(self.BENEFICIARY_BANK)
        self.input("RHB Bank Berhad", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input("000019810502005", self.BENEFICIARY_ACCOUNT_NUMBER)
        self.input("FDA 5", self.BENEFICIARY_NAME)
        self.input("10", self.AMOUNT_OTHER_ACCOUNT)
        self.click_on_element(self.PURPOSE_OF_TRANSFER)
        self.input("Study", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input("Bayar Sekolah", self.REMARK_OTHER_ACCOUNT)
        self.click_on_element(self.SAVE_PAYEE)
        self.click_on_element(self.BUTTON_NEXT)


        time.sleep(5)

    def click_pay_transfer_billpayment_singtel(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Singtel", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(Singtel_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)


    def click_pay_transfer_billpayment_m1(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("M1", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(M1_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_starhub(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Starhub", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(Starhub_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_sunpage(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Sunpage", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(Sunpage_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_zone(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Zone", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(Zone1511_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_iras_payment(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("IRAS - Pay", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(IRAS_valid, self.FIELD1)
        self.input("10", self.FIELD2)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_tpf(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Traffic", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(TPF_valid, self.FIELD1)
        self.input(char3_generator()+number4_generator()+char1_generator(), self.FIELD2)
        self.input("10", self.FIELD3)

        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_Stemcord(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("StemCord", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(Stemcord_valid, self.FIELD1)
        # self.input(StemcordInvoice_valid + number4_generator(8), self.FIELD2)
        self.input("RC" + number4_generator(8), self.FIELD2)

        self.input(female, self.FIELD3)
        # self.input(No689_valid + number4_generator(7), self.FIELD4)
        self.input("9" + number4_generator(7), self.FIELD4)

        self.input("100", self.FIELD5)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    # def click_pay_transfer_billpayment_AvivaInsuranceIndividualPolicy(self):
    #     self.click_on_element(self.BILL_PAYMENT)
    #
    #     self.click_on_element(self.FROM_BILL_PAYMENT)
    #     self.click_on_element(self.HYSA_3)
    #
    #     self.click_on_element(self.BILLER_BILL_PAYMENT)
    #     self.input("Aviva Insurance - Individual Life (By", self.SEARCH)
    #     time.sleep(5)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.input(AvivaIndividual_valid, self.FIELD1)
    #     self.input("6"+number4_generator(7), self.FIELD2)
    #     self.input("10", self.FIELD3)
    #     self.input("10", self.FIELD4)
    #     self.click_on_element(self.BUTTON_NEXT)
    #     time.sleep(10)
    #
    #
    # def click_pay_transfer_billpayment_AvivaInsuranceIndividualHealth(self):
    #     self.click_on_element(self.BILL_PAYMENT)
    #
    #     self.click_on_element(self.FROM_BILL_PAYMENT)
    #     self.click_on_element(self.HYSA_3)
    #
    #     self.click_on_element(self.BILLER_BILL_PAYMENT)
    #     self.input("Aviva Insurance - Individual Life/Health", self.SEARCH)
    #     time.sleep(5)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.input(AvivaIndividual_valid, self.FIELD1)
    #     self.input(number4_generator(8), self.FIELD2)
    #     self.input("10", self.FIELD3)
    #     self.input("10", self.FIELD4)
    #     self.click_on_element(self.BUTTON_NEXT)
    #     time.sleep(10)
    #
    #
    #
    #
    #
    # def click_pay_transfer_billpayment_AvivaInsuranceMindef(self):
    #     self.click_on_element(self.BILL_PAYMENT)
    #
    #     self.click_on_element(self.FROM_BILL_PAYMENT)
    #     self.click_on_element(self.HYSA_3)
    #
    #     self.click_on_element(self.BILLER_BILL_PAYMENT)
    #     self.input("Aviva Insurance - MIND", self.SEARCH)
    #     time.sleep(5)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.input(AvivaMindef_valid, self.FIELD1)
    #     self.input(number4_generator(8), self.FIELD2)
    #     self.input("10", self.FIELD3)
    #     self.click_on_element(self.BUTTON_NEXT)
    #     time.sleep(10)
    #

    def click_pay_transfer_billpayment_HitaciCapitalEquipment(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Hitachi Capital - Equip", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(HitaciDept1_valid, self.FIELD1)
        self.input("6"+number4_generator(7), self.FIELD2)
        self.input("10", self.FIELD3)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def click_pay_transfer_billpayment_HitaciCapitalRoadTax(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Hitachi Capital - Road", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(HitaciDept1_valid, self.FIELD1)
        self.input("9"+number4_generator(7), self.FIELD2)
        self.input("10", self.FIELD3)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)


    def click_pay_transfer_billpayment_ShortTermRental(self):
        self.click_on_element(self.BILL_PAYMENT)

        self.click_on_element(self.FROM_BILL_PAYMENT)
        self.click_on_element(self.HYSA_3)

        self.click_on_element(self.BILLER_BILL_PAYMENT)
        self.input("Hitachi Capital - Short", self.SEARCH)
        time.sleep(5)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(HitaciDept1_valid, self.FIELD1)
        self.input("8"+number4_generator(7), self.FIELD2)
        self.input("10", self.FIELD3)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)