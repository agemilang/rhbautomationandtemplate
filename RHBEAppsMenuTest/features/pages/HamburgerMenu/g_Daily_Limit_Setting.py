from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class g_Daily_Limit_Setting(Page):
    DAILY_LIMIT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout')



    LOCAL_TRANSFER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    BILL_PAYMENT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')


    ERROR_LOCAL_TRANSFER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvErrorEditTextBox')


    SUBMIT_CHANGED = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSubmit')

    ALERT_LIMIT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvMessage')



    def click_daily_limit(self):
        self.click_on_element(self.DAILY_LIMIT)
        # time.sleep(15)


    def set_null_field_and_fill_field(self):

        self.find_element(self.LOCAL_TRANSFER).clear()
        self.find_element(self.BILL_PAYMENT).clear()

        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("6000", self.LOCAL_TRANSFER)
        self.click_on_element(self.SUBMIT_CHANGED)
        #
        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("6000", self.BILL_PAYMENT)
        self.click_on_element(self.SUBMIT_CHANGED)



    def set_under_daily_limit_fill_field(self):
        self.find_element(self.LOCAL_TRANSFER).clear()
        self.find_element(self.BILL_PAYMENT).clear()

        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("1000", self.LOCAL_TRANSFER)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("1000", self.BILL_PAYMENT)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_under_limit = self.find_elements(self.ALERT_LIMIT)
        compare_data_with_expected(expected="Oops!\nTransfer Limit must be between 5,000.00 and 50,000.00 on Payment with currency SGD. Please try again.", real=error_message_under_limit[0].text)

    def set_upper_daily_limit_fill_field(self):
        self.find_element(self.LOCAL_TRANSFER).clear()
        self.find_element(self.BILL_PAYMENT).clear()

        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("100000", self.LOCAL_TRANSFER)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_local_transfer = self.find_elements(self.ERROR_LOCAL_TRANSFER)
        compare_data_with_expected(expected="Please fill up this field", real=error_message_local_transfer[0].text)

        self.input("100000", self.BILL_PAYMENT)
        self.click_on_element(self.SUBMIT_CHANGED)

        error_message_under_limit = self.find_elements(self.ALERT_LIMIT)
        compare_data_with_expected(expected="Oops!\nTransfer Limit must be between 5,000.00 and 50,000.00 on Payment with currency SGD. Please try again.", real=error_message_under_limit[0].text)

        time.sleep(15)


