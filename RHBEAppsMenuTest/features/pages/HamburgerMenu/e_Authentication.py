from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class e_Authentication(Page):
    AUTHENTICATION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout')

    # Change PIN
    CHANGE_APP_PIN_CODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView')
    TYPE_OLD_PIN_CODE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tietPin')
    TYPE_NEW_PIN_CODE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tietPin')
    SEE_PIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ivPeek')
    CLEAR_PIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ivClose')


    # TYPE_OLD_PIN_CODE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputLayout')
    SUBMIT_PIN_CODE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSubmit')
    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")
    BACK_AUTHENTICATION = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btBack")


    # Change Password
    CHANGE_PASSWORD = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView')

    def click_authentication(self):
        self.click_on_element(self.AUTHENTICATION)


    def click_change_pin_and_fill_old_pin(self):
        self.click_on_element(self.CHANGE_APP_PIN_CODE)
        # self.click_on_element(self.TYPE_OLD_PIN_CODE)

        self.input("995995", self.TYPE_OLD_PIN_CODE)
        self.click_on_element(self.SEE_PIN)
        self.click_on_element(self.CLEAR_PIN)
        self.input("995995", self.TYPE_OLD_PIN_CODE)
        self.click_on_element(self.SEE_PIN)

        self.click_on_element(self.SUBMIT_PIN_CODE)


        time.sleep(5)

    def click_change_pin_and_fill_new_pin(self):
        self.input("559559", self.TYPE_NEW_PIN_CODE)
        self.click_on_element(self.SEE_PIN)
        self.click_on_element(self.CLEAR_PIN)
        self.input("559559", self.TYPE_NEW_PIN_CODE)
        self.click_on_element(self.SEE_PIN)

        time.sleep(15)
        self.click_on_element(self.BUTTON_NEXT)
        self.click_on_element(self.BACK_AUTHENTICATION)




        time.sleep(15)
# lanjut Eapss FIX DEPOSIT PLACEMENT

