from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class EAppsMyKadFDPIOS(Page):

    BUTTON_NEXT = (By.ID, 'NEXT')
    # BUTTON_NEXT2 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext2")

    DONE = (By.ID, '(//XCUIElementTypeButton[@name="Done"])[1]')


    PRODUCT_TRIO = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[1]')
    PRODUCT_PREMIER_PLUS = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]')
    PRODUCT_FIX_DEPOSIT_PLACEMENT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')


    #
    #
    #
    #
    # # Allow Location
    # ALLOW_ALL_TIME = (By.ID, 'com.android.permissioncontroller:id/permission_allow_always_button')
    # ALLOW_ONLY_USE_APP = (By.ID, 'com.android.permissioncontroller:id/permission_allow_foreground_only_button')
    # DENY = (By.ID, 'com.android.permissioncontroller:id/permission_deny_button')


    # Fixed Deposit Placement
    # PLACEMENT_TO = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]')
    # PLACEMENT_TO_1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]')

    PLACEMENT_TO = (By.ID, 'fpPlacementTo')
    PLACEMENT_TO_1 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton')
    PLACEMENT_TO_2 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeButton')

    SEARCH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    # CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    # CURRENCY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout')



    TENURE_1 = (By.XPATH, '//XCUIElementTypeButton[@name="1"]')
    TENURE_2 = (By.XPATH, '//XCUIElementTypeButton[@name="2"]')
    TENURE_3 = (By.XPATH, '//XCUIElementTypeButton[@name="3"]')
    TENURE_4 = (By.XPATH, '//XCUIElementTypeButton[@name="4"]')

    # PLACEMENT_AMOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[9]')
    PLACEMENT_AMOUNT = (By.ID, 'fpPlacementAmount')



    SOURCE_OF_FUNDS = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[13]')
    SOURCE_OF_FUNDS = (By.ID, 'ddSourceOfFunds')


    BUSINESS_INCOME = (By.XPATH, '//XCUIElementTypeStaticText[@name="Business Income"]')
    BUSINESS_INVESTMENT = (By.XPATH, '//XCUIElementTypeStaticText[@name="Business investment"]')


    SOURCE_OF_WEALTH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[15]')
    SALARY = (By.XPATH, '//XCUIElementTypeStaticText[@name="Salary"]')
    SAVING = (By.XPATH, '//XCUIElementTypeStaticText[@name="Savings"]')

    FROM = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[17]')
    HYSA3 = (By.XPATH, '(//XCUIElementTypeStaticText[@name="labelValue"])[2]')



    MATURITY_INSTRUCTION = (By.XPATH, '//XCUIElementTypeOther[@name="fpMaturityInstruction"]')

    RENEW_PRINCIPAL_AND_INTEREST = (By.XPATH, '//XCUIElementTypeCell[@name="Renew Principal and Interest"]')
    RENEW_PRINCIPAL = (By.XPATH, '//XCUIElementTypeCell[@name="Renew Principal"]')
    DO_NOT_RENEW = (By.XPATH, '//XCUIElementTypeCell[@name="Do not Renew"]')

    TO = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[21]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]')

    TO2 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeButton')


    ACCOUUNT_SUMMARY = (By.ID, 'Submit')


    def choose_product(self):
        self.click_on_element(self.PRODUCT_FIX_DEPOSIT_PLACEMENT)
        # self.click_on_element(self.ALLOW_ALL_TIME)
        time.sleep(15)


    def fill_fix_deposit_placement(self):
        self.click_on_element(self.PLACEMENT_TO)
        self.click_on_element(self.PLACEMENT_TO_1)

        self.click_on_element(self.TENURE_3)
        self.input("1000", self.PLACEMENT_AMOUNT)
        self.click_on_element(self.SOURCE_OF_FUNDS)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.SALARY)
        self.click_on_element(self.DONE)

        self.click_on_element(self.SOURCE_OF_WEALTH)
        time.sleep(5)

        self.input("Salary", self.SEARCH)
        self.click_on_element(self.SALARY)
        self.click_on_element(self.DONE)
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.FROM)
        self.click_on_element(self.HYSA3)
        self.click_on_element(self.MATURITY_INSTRUCTION)
        self.click_on_element(self.DO_NOT_RENEW)
        self.click_on_element(self.TO)
        self.click_on_element(self.TO2)
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.BUTTON_NEXT)


    def check_maturity_instructionfd1(self):
        self.click_on_element(self.PLACEMENT_TO)
        self.click_on_element(self.PLACEMENT_TO_1)
        time.sleep(5)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()

        self.click_on_element(self.MATURITY_INSTRUCTION)

        error_message_account_rejected = self.find_elements(self.DO_NOT_RENEW)
        compare_data_with_expected(
            expected="Do not Renew",
            real=error_message_account_rejected[0].text)

    def check_maturity_instructionfd2(self):
        self.click_on_element(self.PLACEMENT_TO)
        self.click_on_element(self.PLACEMENT_TO_2)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()

        self.click_on_element(self.MATURITY_INSTRUCTION)

        error_message_account_rejected = self.find_elements(self.RENEW_PRINCIPAL_AND_INTEREST)
        compare_data_with_expected(
            expected="Renew Principal and Interest",
            real=error_message_account_rejected[0].text)

    def confirm_fix_deposit_placement(self):
        self.click_on_element(self.BUTTON_NEXT)

    def click_account_summary(self):
        self.click_on_element(self.ACCOUUNT_SUMMARY)

