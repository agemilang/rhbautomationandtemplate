from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class EAppsMyKadTRIO(Page):

    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")

    # # PIN
    # NO_1 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btOne')
    # NO_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btTwo')
    # NO_3 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btThree')
    # NO_4 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFour')
    # NO_5 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFive')
    # NO_6 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSix')
    # NO_7 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSeven')
    # NO_8 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btEight')
    # NO_9 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNine')
    # NO_0 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btZero')
    # BACKSPACE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btBackscape')

    # Biometric
    MAYBE_LATER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btCancel')

    # Mobile Banking Button
    MB_BUTTON = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')


    # Center Menu
    HAMBURGER_MENU = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView')
    E_APPS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]')

    # Choose Product
    PRODUCT_TRIO = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llTrio')
    PRODUCT_PREMIER_PLUS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llPremierPlus')
    PRODUCT_FIX_DEPOSIT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llFda')

    # Allow Location
    ALLOW_ALL_TIME = (By.ID, 'com.android.permissioncontroller:id/permission_allow_always_button')
    ALLOW_ONLY_USE_APP = (By.ID, 'com.android.permissioncontroller:id/permission_allow_foreground_only_button')
    DENY = (By.ID, 'com.android.permissioncontroller:id/permission_deny_button')

    # Fixed Deposit
    PURPOSE_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    SOURCE_OF_FUND = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    DONE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/action_next")
    SOURCE_OF_WEALTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    CURRENCY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')

    # Mailing Address TRIO
    COUNTRY_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    STATE_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    CITY_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    ADDRESS_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    APPARTMENT_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PORTAL_CODE_TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Mailing Address Fix Deposit
    COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    APPARTMENT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PORTAL_CODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Employment Details
    EMPLOYMENT_STATUS_TRIO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/tvValue")

    # Employed
    OCCUPATION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    EMPLOYER_NAME = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')
    EMPLOYER_BUSINESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    INCOME_ANNUM = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')



    # Self Employed



    # Unemployed




    # Declaration
    COUNTRY_TAX = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvValue')
    # NO_HAVE_TIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbTinNo')
    TIN_NUMBER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')
    SIGNATURE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ivSignature')
    SIGNATURE_TAKE_PHOTO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    SIGNATURE_ACCEPT_PHOTO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")


    # Term & Condition
    # pilihan1
    I_UNDERSTOOD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbTermCondition')

    # pilihan2
    I_AGREE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotional')
    VOICE_CALL = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalVoiceCall')
    SMS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalSms')


    # Placement Details
    TENURE_1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[1]')
    TENURE_2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[2]')
    TENURE_3 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[3]')
    TENURE_4 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.Button[4]')

    PLACEMENT_AMOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')

    FROM = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvSourceFundName')
    TRIO = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]')
    PREMIER_PLUS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]')
    HIGH_YIELD = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]')


    MATURITY_INSTRUCTIONS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvValue')
    RENEW_PRINCIPAL_AND_INTEREST = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]')
    RENEW_PRINCIPAL = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]')


    DO_NOT_RENEW = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.TextView')
    TO = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvToAccountName')

    def biometeric_maybe_later(self):
        self.click_on_element(self.MAYBE_LATER)

    def click_mobile_banking(self):
        self.click_on_element(self.MB_BUTTON)

        time.sleep(15)
    def click_center_menu(self):
        self.click_on_element(self.HAMBURGER_MENU)
        self.click_on_element(self.E_APPS)
    #
    def choose_product(self):
        self.click_on_element(self.PRODUCT_TRIO)
        # self.click_on_element(self.ALLOW_ALL_TIME)
    #

    def fill_form_account_detail_trio(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Investment", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.SOURCE_OF_FUND)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.SOURCE_OF_WEALTH)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.COUNTRY_TRIO)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.STATE_TRIO)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        time.sleep(5)

        self.driver.swipe(15, 1844, 15, 560)
        time.sleep(5)
        self.input(city, self.CITY_TRIO)
        self.input(address, self.ADDRESS_TRIO)
        # self.input("SGD", self.APPARTMENT)
        self.input(postalcode, self.PORTAL_CODE_TRIO)

        self.click_on_element(self.BUTTON_NEXT)






    def fill_form_account_detail_fix_deposit(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Investment", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.SOURCE_OF_FUND)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.SOURCE_OF_WEALTH)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.CURRENCY)
        self.input("SGD", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        time.sleep(5)

        self.driver.swipe(15, 1844, 15, 560)
        time.sleep(5)
        self.click_on_element(self.COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)

        self.input(city, self.CITY)
        self.input(address, self.ADDRESS)
        # self.input("SGD", self.APPARTMENT)
        self.input(postalcode, self.PORTAL_CODE)

        self.click_on_element(self.BUTTON_NEXT)


    def fill_form_contact_detail_trio(self):
        time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_form_employment_status_employed_trio(self):
        self.click_on_element(self.EMPLOYMENT_STATUS_TRIO)
        self.input("Employed", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.OCCUPATION)
        self.input("Designer", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)

        self.input(male, self.EMPLOYER_NAME)
        self.click_on_element(self.EMPLOYER_BUSINESS)
        self.input("R&D", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.INCOME_ANNUM)
        self.input("$90", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_declaration_trio(self):
        time.sleep(2)
        self.click_on_element(self.COUNTRY_TAX)
        self.input("Australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(phone, self.TIN_NUMBER)
        time.sleep(2)

        self.driver.swipe(15, 1844, 15, 560)
        time.sleep(2)
        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_PHOTO)
        self.click_on_element(self.SIGNATURE_ACCEPT_PHOTO)
        self.click_on_element(self.BUTTON_NEXT)

    def confirm_application(self):
        for i in range(3):
            self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.BUTTON_NEXT)

    def agree_term_condition(self):
        self.click_on_element(self.I_UNDERSTOOD)
        self.click_on_element(self.I_AGREE)
        self.click_on_element(self.VOICE_CALL)
        self.click_on_element(self.SMS)

        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)


    # def fill_form_employment_status_self_employed_trio(self):
    #     self.click_on_element(self.EMPLOYMENT_STATUS_TRIO)
    #     self.input("Self Employed", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #
    # def fill_form_employment_status_unemployed_trio(self):
    #     self.click_on_element(self.EMPLOYMENT_STATUS_TRIO)
    #     self.input("Unemployed", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)











    # def fill_form_placement_detail_fix_deposit(self):
    #     self.click_on_element(self.TENURE_3)
    #     self.input("10000", self.PLACEMENT_AMOUNT)
    #
    #     self.click_on_element(self.FROM)
    #     self.click_on_element(self.TRIO)
    #
    #     self.click_on_element(self.MATURITY_INSTRUCTIONS)
    #     self.click_on_element(self.DO_NOT_RENEW)
    #
    #     self.click_on_element(self.TO)
    #     self.click_on_element(self.TRIO)
    #     self.click_on_element(self.BUTTON_NEXT)







        time.sleep(15)


