from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class EAppsMyKadPP(Page):

    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")

    # Biometric
    MAYBE_LATER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btCancel')

    # Mobile Banking Button
    MB_BUTTON = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')


    # Center Menu
    HAMBURGER_MENU = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView')
    E_APPS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]')

    # Choose Product
    PRODUCT_HYSA = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llHysaSelection')
    PRODUCT_TRIO = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llTrio')
    PRODUCT_PREMIER_PLUS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llPremierPlus')
    PRODUCT_FIX_DEPOSIT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]')

    # Allow Location
    ALLOW_ALL_TIME = (By.ID, 'com.android.permissioncontroller:id/permission_allow_always_button')
    ALLOW_ONLY_USE_APP = (By.ID, 'com.android.permissioncontroller:id/permission_allow_foreground_only_button')
    DENY = (By.ID, 'com.android.permissioncontroller:id/permission_deny_button')


    # Account Detail PP
    PURPOSE_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    SOURCE_OF_FUND = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    DONE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/action_next")
    SOURCE_OF_WEALTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')

    COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout')
    STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout')
    CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    APPARTMENT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PORTAL_CODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Employment Details
    EMPLOYMENT_STATUS_PP = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llDropdown")

    # Employed
    OCCUPATION = (By.XPATH,
                  '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    EMPLOYER_NAME = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')
    EMPLOYER_BUSINESS = (By.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    INCOME_ANNUM = (By.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')

    # Declaration
    COUNTRY_TAX = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvValue')
    # NO_HAVE_TIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbTinNo')
    TIN_NUMBER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')
    SIGNATURE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ivSignature')
    SIGNATURE_TAKE_PHOTO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    SIGNATURE_ACCEPT_PHOTO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")

    #
    def choose_product(self):
        self.click_on_element(self.PRODUCT_PREMIER_PLUS)

    def fill_form_account_detail_pp(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Investment", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.SOURCE_OF_FUND)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.SOURCE_OF_WEALTH)
        self.input("Salary", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.DONE)
        self.click_on_element(self.COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        time.sleep(5)

        self.driver.swipe(15, 1844, 15, 560)
        time.sleep(5)
        self.input(city, self.CITY)
        self.input(address, self.ADDRESS)
        # self.input("SGD", self.APPARTMENT)
        self.input(postalcode, self.PORTAL_CODE)
        # time.sleep(55)
        self.click_on_element(self.BUTTON_NEXT)

    # def fill_form_account_detail_fix_deposit(self):
    #     self.click_on_element(self.PURPOSE_ACCOUNT)
    #     self.input("Investment", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.click_on_element(self.SOURCE_OF_FUND)
    #     self.input("Salary", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.click_on_element(self.DONE)
    #     self.click_on_element(self.SOURCE_OF_WEALTH)
    #     self.input("Salary", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.click_on_element(self.DONE)
    #     self.click_on_element(self.CURRENCY)
    #     self.input("SGD", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     time.sleep(5)
    #
    #     self.driver.swipe(15, 1844, 15, 560)
    #     time.sleep(5)
    #     self.click_on_element(self.COUNTRY)
    #     self.input("Singapore", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #     self.click_on_element(self.STATE)
    #     self.input("Singapore", self.SEARCH)
    #     self.click_on_element(self.CHOOSE_ROW1)
    #
    #     self.input(city, self.CITY)
    #     self.input(address, self.ADDRESS)
    #     # self.input("SGD", self.APPARTMENT)
    #     self.input(postalcode, self.PORTAL_CODE)
    #
    #     self.click_on_element(self.BUTTON_NEXT)

    def fill_form_contact_detail_trio(self):
        time.sleep(5)
        self.click_on_element(self.BUTTON_NEXT)
        # time.sleep(1)


    def fill_form_employment_status_employed_pp(self):
        self.click_on_element(self.EMPLOYMENT_STATUS_PP)
        self.input("Employed", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.OCCUPATION)
        self.input("Designer", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)

        self.input(male, self.EMPLOYER_NAME)
        self.click_on_element(self.EMPLOYER_BUSINESS)
        self.input("R&D", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.INCOME_ANNUM)
        self.input("$90", self.SEARCH)
        time.sleep(2)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(15)




    def fill_declaration_pp(self):
        time.sleep(2)
        self.click_on_element(self.COUNTRY_TAX)
        self.input("Australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(phone, self.TIN_NUMBER)
        time.sleep(2)

        self.driver.swipe(15, 1844, 15, 560)
        time.sleep(2)
        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_PHOTO)
        self.click_on_element(self.SIGNATURE_ACCEPT_PHOTO)
        self.click_on_element(self.BUTTON_NEXT)









        time.sleep(15)


