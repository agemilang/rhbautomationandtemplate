from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class Bottom_Menu(Page):


    MOBILE_BANKING = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')
    APPLY_FOR_NEW_ACCOUNT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llEapps')

    HOME = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llHome')
    ACCOUNTS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llAccount')
    EXPLORE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llExplore')
    INBOX = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llBox')

    INBOX_BACK = (By.XPATH, '//android.widget.ImageButton[@content-desc="Navigate up"]')

    MB_BUTTON = (By.XPATH,
                 '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[3]')

    # Center Menu
    HAMBURGER_MENU = (By.XPATH,
                      '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView')

    E_APPS = (By.XPATH,
              '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]')
    LIMIT = (By.XPATH,
             '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]')
    PAY = (By.XPATH,
           '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[3]')

    def click_mobile_banking(self):
        self.click_on_element(self.MB_BUTTON)

        time.sleep(5)

    def click_center_menu(self):
        self.click_on_element(self.HAMBURGER_MENU)
        time.sleep(5)

    def click_eapps(self):
        self.click_on_element(self.E_APPS)
        time.sleep(5)

    def click_limit(self):
        self.click_on_element(self.LIMIT)
        time.sleep(5)

    def click_pay(self):
        self.click_on_element(self.PAY)
        time.sleep(5)

    def click_mobile_banking(self):
        self.click_on_element(self.MOBILE_BANKING)

    def click_explore(self):
        self.click_on_element(self.EXPLORE)

        time.sleep(5)
    def click_inbox(self):
        self.click_on_element(self.INBOX)
        self.click_on_element(self.INBOX_BACK)

        time.sleep(5)

    def click_account(self):
        self.click_on_element(self.ACCOUNTS)

        time.sleep(5)

    def click_home(self):
        self.click_on_element(self.HOME)
        time.sleep(15)