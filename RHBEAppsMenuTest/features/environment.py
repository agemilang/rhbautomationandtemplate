from appium import webdriver
from app.application import Application


def before_scenario(context, scenario):

    context.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub",
                                      desired_capabilities={"deviceName": "7TJFHUNVEAPBZLDE",
                                                            "platformName": "android",
                                                            "appPackage": "sg.com.rhbgroup.travelfx.android.sit",
                                                            "appActivity": "sg.com.rhbgroup.travelfx.android.transaction.ui.splash.SplashActivity",
                                                            "noReset": "true",
                                                            "dontStopAppOnReset": "true",
                                                            # 'autoGrantPermissions': 'true'
                                                            })


    # context.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub",
    #                                   desired_capabilities={"deviceName": "e02cafe9",
    #                                                         "platformName": "android",
    #                                                         "appPackage": "sg.com.rhbgroup.travelfx.android.sit",
    #                                                         "appActivity": "sg.com.rhbgroup.travelfx.android.transaction.ui.splash.SplashActivity",
    #                                                         "noReset": "true",
    #                                                         "dontStopAppOnReset": "true"
    #                                                         })

    # context.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub",
    #                                     desired_capabilities={"platformName": "iOS",
    #                                                           "deviceName": "arka's iPhone",
    #                                                           "bundleId": "com.Teravin.RHBTravelCard",
    #                                                           # "app": "/Users/admin/Downloads/RHBTravelCard.app",
    #                                                           "udid": "477bb7fed7ab855012e97ca72a0c7fee2ec41c0d",
    #                                                           "platformVersion": "12.4.9",
    #                                                           "automationName": "XCUITest",
    #                                                           "noReset": "true",
    #                                                           "locationServicesEnabled": "true",
    #                                                           "locationServicesAuthorized": "true",
    #                                                           "autoAcceptAlerts": "true",
    #                                                           "newCommandTimeout": "100",
    #                                                           "gpsEnabled": "true"
    #                                                           })



    context.driver.implicitly_wait(35)

    context.app = Application(context.driver)





def after_scenario(context, scenario):
    context.driver.close_app()
    context.driver.quit()
