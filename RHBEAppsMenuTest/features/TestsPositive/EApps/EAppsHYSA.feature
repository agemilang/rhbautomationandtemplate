Feature: Successful Fill EApps HYSA

#Scenario: Successful Fill EApps HYSA
#   Given I Fill PIN
##   When I Choose Maybe Later Biometric
#    Then I Click Mobile Banking
#    Then I Click Center Menu Choose EApps
##    Then I Fill Form Placement Detail
#    Then I Click Product HYSA
#    Then I Fill Form Account Detail HYSA
#    Then I Fill Form Contact Detail HYSA & TRIO
#    Then I Fill Employment Status HYSA & TRIO
#    Then I Fill Declaration HYSA & TRIO
#    Then I Confirm Application Detail
#    Then I Agree Term & Conditions

Scenario: Stage Customer Onboarding Name Screening Maker
  Given I POST admin login Customer Info HYSA
  Then I Get List Customer EApps Get Identity No And ID Name Customer Management HYSA
  Then I POST admin login Name Screening Maker HYSA
  Then I Get List Task Approval EApps Get Task ID Name Screening Maker HYSA
  Then I Get List Task Detail Approval EApps Get Onboard ID Name Screening Maker HYSA
  Then I Get List Task Approval EApps Get Reason ID Name Screening Maker HYSA
  Then I Post Action Task Approval EApps Name Screening Maker HYSA


Scenario: Stage Customer Onboarding Name Screening Checker
  Given I POST admin login Name Screening Checker HYSA
  Then I Get List Customer EApps Get Identity No And ID Name Screening Checker HYSA
  Then I Get List Task Approval EApps Get Task ID Name Screening Checker HYSA
  Then I Get List Task Detail Approval EApps Get Onboard ID Name Screening Checker HYSA
  Then I Post Action Task Approval EApps Name Screening Checker HYSA