# from features.pages.launch_page import LaunchPage
# from features.pages.Register.RegisterMyKad import RegisterMyKad
# from features.pages.Register.RegisterSingapore import RegisterSingapore
# from features.pages.Register.RegisterForeigner import RegisterForeigner
# from features.pages.LoginAfterApprovalAdmin.LoginMyKad import LoginMyKad

from features.pages.pin import Pin
from features.pages.pinIOS import PinIOS

from features.pages.HomeIOS import Home
from features.pages.HamburgerMenuIOS.c_Apply_For_New_Account_IOS import c_Apply_For_New_Account_IOS


# from features.pages.EAppsIOS.EAppsMyKadFDPIOS import EAppsMyKadFDPIOS


from features.pages.EApps.EAppsMyKadTRIO import EAppsMyKadTRIO
from features.pages.EApps.EAppsMyKadHYSA import EAppsMyKadHYSA
from features.pages.EApps.EAppsMyKadFD import EAppsMyKadFD
from features.pages.EApps.EAppsMyKadPP import EAppsMyKadPP
from features.pages.EApps.EAppsMyKadFDP import EAppsMyKadFDP

from features.pages.EAppsIOS.EAppsMyKadFDPIOS import EAppsMyKadFDPIOS



from features.pages.HamburgerMenu.a_MyAccount import a_MyAccount
from features.pages.HamburgerMenu.b_Pay_Transfer import b_Pay_Transfer
from features.pages.HamburgerMenu.c_Apply_For_New_Account import c_Apply_For_New_Account
from features.pages.HamburgerMenu.d_Fixed_Deposit_Placement import d_Fixed_Deposit_Placement
from features.pages.HamburgerMenu.e_Authentication import e_Authentication
from features.pages.HamburgerMenu.f_Notification import f_Notification
from features.pages.HamburgerMenu.g_Daily_Limit_Setting import g_Daily_Limit_Setting
from features.pages.HamburgerMenu.h_Frequently_Asked_Question import h_Frequently_Asked_Question
from features.pages.HamburgerMenu.i_RHB_Mobile_SG_App import i_RHB_Mobile_SG_App
from features.pages.HamburgerMenu.j_Personal_Information import j_Personal_Information
from features.pages.HamburgerMenu.k_Sign_Out import k_Sign_Out

from features.pages.HamburgerMenuIOS.a_MyAccount_IOS import a_MyAccount_IOS
from features.pages.HamburgerMenuIOS.b_Pay_Transfer_IOS import b_Pay_Transfer_IOS
from features.pages.HamburgerMenuIOS.c_Apply_For_New_Account_IOS import c_Apply_For_New_Account_IOS
from features.pages.HamburgerMenuIOS.d_Fixed_Deposit_Placement_IOS import d_Fixed_Deposit_Placement_IOS
# from features.pages.HamburgerMenuIOS.e_Authentication_IOS import e_Authentication_IOS
# from features.pages.HamburgerMenuIOS.f_Notification_IOS import f_Notification_IOS
# from features.pages.HamburgerMenuIOS.g_Daily_Limit_Setting_IOS import g_Daily_Limit_Setting_IOS
# from features.pages.HamburgerMenuIOS.h_Frequently_Asked_Question_IOS import h_Frequently_Asked_Question_IOS
# from features.pages.HamburgerMenuIOS.i_RHB_Mobile_SG_App_IOS import i_RHB_Mobile_SG_App_IOS
# from features.pages.HamburgerMenuIOS.j_Personal_Information_IOS import j_Personal_Information_IOS
# from features.pages.HamburgerMenuIOS.k_Sign_Out_IOS import k_Sign_Out_IOS

from features.pages.BottomMenu.Bottom_Menu import Bottom_Menu


from features.pages.HamburgerMenuIOS.a_MyAccount_IOS import a_MyAccount_IOS
from features.pages.HamburgerMenuIOS.b_Pay_Transfer_IOS import b_Pay_Transfer_IOS
from features.pages.HamburgerMenuIOS.k_Sign_Out_IOS import k_Sign_Out_IOS


from features.pages.SliderIOS.SliderFD import SliderFD
# from features.pages.main_page import MainPage


class Application:
    def __init__(self, driver):

        self.pin = Pin(driver)
        self.pinIOS = PinIOS(driver)

        self.Home = Home(driver)

        self.Bottom_Menu = Bottom_Menu(driver)

        self.c_Apply_For_New_Account = c_Apply_For_New_Account(driver)
        self.c_Apply_For_New_Account = c_Apply_For_New_Account(driver)


        self.c_Apply_For_New_Account_IOS = c_Apply_For_New_Account_IOS(driver)
        self.d_Fixed_Deposit_Placement_IOS = d_Fixed_Deposit_Placement_IOS(driver)



        self.EAppsMyKadHYSA = EAppsMyKadHYSA(driver)
        self.EAppsMyKadTRIO = EAppsMyKadTRIO(driver)
        self.EAppsMyKadPP = EAppsMyKadPP(driver)
        self.EAppsMyKadFD = EAppsMyKadFD(driver)
        self.EAppsMyKadFDP = EAppsMyKadFDP(driver)

        self.EAppsMyKadFDPIOS = EAppsMyKadFDPIOS(driver)



        self.a_MyAccount = a_MyAccount(driver)
        self.b_Pay_Transfer = b_Pay_Transfer(driver)
        self.c_Apply_For_New_Account = c_Apply_For_New_Account(driver)
        self.d_Fixed_Deposit_Placement = d_Fixed_Deposit_Placement(driver)
        self.e_Authentication = e_Authentication(driver)
        self.f_Notification = f_Notification(driver)
        self.g_Daily_Limit_Setting = g_Daily_Limit_Setting(driver)
        self.h_Frequently_Asked_Question = h_Frequently_Asked_Question(driver)
        self.i_RHB_Mobile_SG_App = i_RHB_Mobile_SG_App(driver)
        self.j_Personal_Information = j_Personal_Information(driver)
        self.k_Sign_Out = k_Sign_Out(driver)


        self.a_MyAccount_IOS = a_MyAccount_IOS(driver)
        self.b_Pay_Transfer_IOS = b_Pay_Transfer_IOS(driver)
        self.k_Sign_Out_IOS = k_Sign_Out_IOS(driver)




        self.SliderFD = SliderFD(driver)
        # self.main_page = MainPage(driver)
