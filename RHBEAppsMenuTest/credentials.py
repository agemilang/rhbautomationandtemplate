import random
import numpy as np
import string
from faker import Faker

fake = Faker([ 'en_US'])

phone = fake.msisdn()
phone1 = fake.msisdn()
phone2 = fake.msisdn()
phone3 = fake.msisdn()
phone4 = fake.msisdn()
phone5 = fake.msisdn()
phone6 = fake.msisdn()
phone7 = fake.msisdn()


email = 'arka'+ phone +'@yopmail.com'
email1 = 'arka'+ phone1 +'@yopmail.com'
email2 = 'arka'+ phone2 +'@yopmail.com'
email3 = 'arka'+ phone3 +'@yopmail.com'
email4 = 'arka'+ phone4 +'@yopmail.com'
email5 = 'arka'+ phone5 +'@yopmail.com'
email6 = 'arka'+ phone6 +'@yopmail.com'
email7 = 'arka'+ phone7 +'@yopmail.com'

url = 'https://13.251.82.102'

def number4_generator(size=4, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def char3_generator(size=3, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))

def char1_generator(size=1, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))


fin = fake.msisdn()
female = fake.name_female()
male = fake.name_male()

city = fake.city()
address = fake.address()
postalcode = fake.postalcode()
passport = random.randint(300000000, 900000000)
password = "Password2"
OTP1 = "123456"

# Telco
Singtel = np.array(["78697545",	"74866268",	"47302184",	"74759017",	"56408466",	"31317963",	"44215106",	"54939458"])
position_to_select = [random.randint(0, 7)]
Singtel_valid = Singtel[position_to_select]

M1 = np.array(["422071134",	"602950682",	"529867116",	"601241728",	"601585250",	"601948728",	"527249640",	"510398234"])
position_to_select = [random.randint(0, 7)]
M1_valid = M1[position_to_select]

Starhub = np.array(["1.15363327S",	"1.15380884S",	"1.11632214L",	"1.15146425A",	"1.13042071H",	"1.11387412U",	"1.15335113R",	"1.13882103I"])
position_to_select = [random.randint(0, 7)]
Starhub_valid = Starhub[position_to_select]

# Pacnet = np.array(["R00047-001",	"A00092-001",	"A00187-001",	"J00031-002",	"I00057-002",	"E00049-002",	"I00061-002",	"G00056-002"])
# position_to_select = [random.randint(0, 7)]
# Pacnet_valid = Pacnet[position_to_select]

Sunpage = np.array(["07488919",	"08361230",	"08502692",	"08400749",	"08000333",		"07848641",	"07806797",	"07615396"])
position_to_select = [random.randint(0, 7)]
Sunpage_valid = Sunpage[position_to_select]

Zone1511 = np.array(["12340000D",	"10109373D",	"10100196G",	"10050050E",	"10061060B",	"12340000D",	"10109373D",	"10100196G"])
position_to_select = [random.randint(0, 7)]
Zone1511_valid = Zone1511[position_to_select]

# Govt
IRAS = np.array(["15598289682029",	"15412611032007",	"15362752577041",	"15633491558052",	"15642242967030",	"15502442208074",	"15583818606063",	"15642601326018"])
position_to_select = [random.randint(0, 7)]
IRAS_valid = IRAS[position_to_select]

TPF = np.array(["0020560189080026",	"0020550218017038",	"0021660000757117",	"0020517000009215",	"0020560189080026",	"0021861326039117",	"0020560021785121",	"0021630000801113"])
position_to_select = [random.randint(0, 7)]
TPF_valid = TPF[position_to_select]


# Electricity
SPG = np.array(["9409911998",	"5666200510",	"8922067635",	"8900000004",	"5666200510",	"9300065233",	"9300020501",	"9300138881"])
position_to_select = [random.randint(0, 7)]
SPG_valid = SPG[position_to_select]

Geneco = np.array(["2884290960",	"9240990000",	"2221345678",	"8230124782",	"1234567890",	"9300134555"])
position_to_select = [random.randint(0, 5)]
Geneco_valid = Geneco[position_to_select]



No689 = np.array(["6",	"8", "9"])
position_to_select = [random.randint(0, 1)]
No689_valid = No689[position_to_select]




# Healthcare
# Tidak tau invoice
StemcordInvoice = np.array(["RC",	"IN"])
position_to_select = [random.randint(0, 1)]
StemcordInvoice_valid = StemcordInvoice[position_to_select]

Stemcord = np.array(["13516479",	"45688990",	"00008389",	"00027149",	"00003334",	"00019789"])
position_to_select = [random.randint(0, 5)]
Stemcord_valid = Stemcord[position_to_select]

# Insurance
# Error
# AvivaIndividual = np.array(["M8509691",	"U1469027",	"Q7583794",	"S9574856"])
# position_to_select = [random.randint(0, 3)]
# AvivaIndividual_valid = AvivaIndividual[position_to_select]
#
# AvivaMindef = np.array(['S1234567D'])
# position_to_select = [random.randint(0,0)]
# AvivaMindef_valid = AvivaMindef[position_to_select]

# Loans
HitaciDept1 = np.array(["72003",	"101578",	"93025",	"109041",	"104783",	"70920",	"82138",	"101696", "S1234567D", "T0311111Z"])
position_to_select = [random.randint(0, 9)]
HitaciDept1_valid = HitaciDept1[position_to_select]













