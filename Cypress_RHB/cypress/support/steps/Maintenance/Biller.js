const random = require('../../../credential');

var name = random.random_char(8)
  var name2 = random.random_char(10)
  var billercode = random.random_char(5)
  
  Then('I Click Biller', (content) => {
    cy.get(".navbar-item:nth-child(3) .navbar-item:nth-child(2)").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(2)').click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Fill Form Biller', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("tr:nth-child(1) .field:nth-child(1) .input").type(billercode)
    cy.get("tr:nth-child(2) > td > .field:nth-child(1) .input").type(name)
    cy.get("select[name='billerCategoryCode']").select('I - Insurance')
    cy.get("input[name='hostBillerCode']").type(random.random_char(5))

    cy.xpath("(//input[@id='0'])[2]").type(random.random_char(8))
    cy.xpath("(//input[@id='0'])[3]").type("1")

    
    cy.xpath("//select[@id='0'][1]").first().select('Alphanumeric')
    cy.xpath("(//input[@id='0'])[6]").type("3")
    cy.xpath("(//input[@id='0'])[7]").type("8")



    cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[3]/div/div/input").type("2")
    cy.get("div:nth-child(1) > div > div > .field .input").type("3")
    cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[5]/div/div[2]/div[2]/div[3]/div/div/div/input").type("8")


    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });

  Then('I Edit Biller', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))

    // cy.get("tr:nth-child(1) .field:nth-child(1) .input").clear()
    // cy.get("tr:nth-child(1) .field:nth-child(1) .input").first().type(random.random_char(5))
    cy.get("tr:nth-child(2) > td > .field:nth-child(1) .input").clear()
    cy.get("tr:nth-child(2) > td > .field:nth-child(1) .input").type(name2)
    cy.get("select[name='billerCategoryCode']").select('E - Electricity')
    cy.get("input[name='hostBillerCode").clear()
    cy.get("input[name='hostBillerCode']").type(random.random_char(5))

    cy.xpath("(//input[@id='0'])[2]").clear()
    cy.xpath("(//input[@id='0'])[2]").type(random.random_char(8))
    // cy.xpath("(//input[@id='0'])[3]").type("1")

    
    // cy.xpath("//select[@id='0'][1]").first().select('Alphanumeric')
    cy.xpath("(//input[@id='0'])[6]").clear()
    cy.xpath("(//input[@id='0'])[6]").type("4")
    cy.xpath("(//input[@id='0'])[7]").clear()
    cy.xpath("(//input[@id='0'])[7]").type("9")

    // cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[3]/div/div/input").clear()
    // cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[3]/div/div/input").type("2")
    cy.get("div:nth-child(1) > div > div > .field .input").clear()
    cy.get("div:nth-child(1) > div > div > .field .input").type("5")
    cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[5]/div/div[2]/div[2]/div[3]/div/div/div/input").clear()
    cy.xpath("//div[@id='app']/div/div/form/div/div/div[2]/table/tbody/tr[2]/td[5]/div/div[2]/div[2]/div[3]/div/div/div/input").type("9")


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });


  Then('I Delete Biller', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.wait(5000)
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });


  Then('I Delete Biller on DataBase', (content) => {

    cy.task('queryDb','DELETE FROM mbk_biller_field_mapping WHERE biller_code = "'+ billercode +'";');
    cy.task('queryDb','DELETE FROM mbk_biller WHERE name = "'+ name2 +'";');
    });