const random = require('../../../credential');


  var name = random.random_char(8)
  var name2 = random.random_char(10)
  
  Then('I Click Country', (content) => {
    cy.xpath("//a[contains(text(),'Country')]").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(1)').click({force: true})
    // cy.wait(5000)
  
  });


  Then('I Fill Form Country', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    
    cy.get("input[name='phoneCode']").type(random.random_numeric(3))
    cy.get("input[name='nationality']").type('Singapore')
    cy.get("textarea[name='description']").type(random.random_char(100))

    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });

  Then('I Edit Country', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)
    cy.get("input[name='phoneCode']").clear()
    cy.get("input[name='phoneCode']").type(random.random_numeric(3))
    cy.get("input[name='nationality']").clear()
    cy.get("input[name='nationality']").type('Malaysia')
    cy.get("textarea[name='description']").clear()
    cy.get("textarea[name='description']").type(random.random_char(100))


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });


  Then('I Delete Country', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });

  Then('I Delete Country on DataBase', (content) => {

    
    cy.task('queryDb','DELETE FROM mnt_country WHERE name = "'+ name2 +'";');
    });