const random = require('../../../credential');

  
  var number = random.random_numeric(5)
  var number2 = random.random_numeric(8)
  var number3 = random.random_numeric(6)
  var number4 = random.random_numeric(9)
  
  Then('I Click Salary Range', (content) => {
    cy.xpath("//a[contains(text(),'Salary Range')]").click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Fill Form Salary Range', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='salaryRange']").type("&" + number + " to &" + number2 )
    cy.get("textarea[name='description']").type(random.userData.randomdescription)

    

    cy.get('.footer-form > .button:nth-child(1)').click()
    // cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });


  Then('I Edit Salary Range', (content) => {

    
    //Search
    cy.get("input[name='salaryRange']").type("&" + number + " to &" + number2 )
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()


    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='salaryRange']").clear()
    cy.get("input[name='salaryRange']").type("&" + number3 + " to &" + number4 )
    cy.get("textarea[name='description']").clear()
    cy.get("textarea[name='description']").type(random.userData.randomdescription)


    cy.get('.footer-form > .button:nth-child(1)').click({force: true})
    // cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });


  Then('I Delete Salary Range', (content) => {

    
    //Search
    cy.get("input[name='salaryRange']").type("&" + number3 + " to &" + number4 )
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });


  Then('I Delete Salary Range on DataBase', (content) => {

    cy.task('queryDb','DELETE FROM mnt_salary_range WHERE name = "&'+ number3 + " to &" + number4+'";');
    });