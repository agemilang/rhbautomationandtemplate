const random = require('../../../credential');


  var name = random.random_char(8)
  var name2 = random.random_char(10)
  
  Then('I Click Currency', (content) => {
    cy.xpath("//a[contains(text(),'Currency')]").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(1)').click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Click Add Maintenance', (content) => {
    cy.get('.column:nth-child(2) > .field .button').click()
    // cy.wait(5000)
  
  });

  Then('I Fill Empty Form', (content) => {
    cy.get('.footer-form > .button:nth-child(1)').click()
    // cy.wait(5000)
  
  });

  Then('I Fill Form Currency', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    cy.get("input[name='isoCode']").type(random.random_char(3))
    cy.get("input[name='unit']").type("1")
    cy.get("input[name='isTransactional']").click()

    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });

  Then('I Edit Currency', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)

    // cy.get("input[name='isoCode']").clear()
    // cy.get("input[name='isoCode']").type(random.random_char(3))
    cy.get("input[name='unit']").clear()
    cy.get("input[name='unit']").type("2")

    cy.get("input[name='isTransactional']").click()


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });


  Then('I Delete Currency', (content) => {
    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });


  Then('I Delete Currency on DataBase', (content) => {

    
    cy.task('queryDb','DELETE FROM mnt_currency WHERE name = "'+ name2 +'";');
    });