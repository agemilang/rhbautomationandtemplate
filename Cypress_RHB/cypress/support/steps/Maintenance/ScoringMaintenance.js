const random = require('../../../credential');

  
  Then('I Click Scoring Maintenance', (content) => {
    cy.xpath("//a[contains(text(),'Scoring Maintenance')]").click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Fill Form Scoring Maintenance', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='salaryRange']").type("&" + random.random_numeric(8) + "to &" + random.random_numeric(9) )
    cy.get("textarea[name='description']").type(random.userData.randomdescription)

    

    cy.get('.footer-form > .button:nth-child(1)').click()
    // cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });