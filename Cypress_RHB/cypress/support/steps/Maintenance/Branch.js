const random = require('../../../credential');


  var name = random.random_char(8)
  var name2 = random.random_char(10)
  
  Then('I Click Branch', (content) => {
    cy.xpath("//a[contains(text(),'Branch')]").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(1)').click({force: true})
    // cy.wait(5000)
  
  });



  Then('I Fill Form Branch', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    cy.get("textarea[name='address1']").type(random.userData.randomaddress)
    cy.get("textarea[name='address2']").type(random.userData.randomaddress)
    cy.get("input[name='phoneNo']").type(random.random_numeric(13))
    cy.get("select[name='countryId']").select('Singapore')
    cy.get("select[name='provinceId']").select('Singapore')
    cy.get("select[name='cityId']").select('Singapore')
    cy.get("input[name='lat']").type(random.userData.randomlat)
    cy.get("input[name='longt']").type(random.userData.randomlong)
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });

  Then('I Edit Branch', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)

    cy.get("textarea[name='address1']").clear()
    cy.get("textarea[name='address1']").type(random.userData.randomaddress)
    cy.get("textarea[name='address2']").clear()
    cy.get("textarea[name='address2']").type(random.userData.randomaddress)
    cy.get("input[name='phoneNo']").clear()
    cy.get("input[name='phoneNo']").type(random.random_numeric(13))
    cy.get("select[name='countryId']").select('Singapore')
    cy.get("select[name='provinceId']").select('Singapore')
    cy.get("select[name='cityId']").select('Singapore')
    cy.get("input[name='lat']").clear()
    cy.get("input[name='lat']").type(random.userData.randomlat)
    cy.get("input[name='longt']").clear()
    cy.get("input[name='longt']").type(random.userData.randomlong)


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });


  Then('I Delete Branch', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });


  Then('I Delete Branch on DataBase', (content) => {

    // cy.exec('npm run delete')
    // cy.task('SELECT * FROM mnt_city' )
    cy.task('queryDb','DELETE FROM mnt_branch WHERE name = "'+ name2 +'";');
    });