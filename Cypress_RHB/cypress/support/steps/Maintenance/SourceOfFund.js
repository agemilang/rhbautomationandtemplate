const random = require('../../../credential');

var name = random.random_char(8)
var name2 = random.random_char(10)

  Then('I Click Source Of Fund', (content) => {
    cy.xpath("//a[contains(text(),'Source Of Fund')]").click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Fill Source Of Fund', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    cy.get("textarea[name='description']").type(random.userData.randomdescription)

    

    cy.get('.footer-form > .button:nth-child(1)').click()
    // cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });

  Then('I Edit Source Of Fund', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()


    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)
    cy.get("textarea[name='description']").clear()
    cy.get("textarea[name='description']").type(random.userData.randomdescription)


    cy.get('.footer-form > .button:nth-child(1)').click({force: true})
    // cy.get('[type="submit"]').click()
    cy.wait(15000)
  
  });


  Then('I Delete Source Of Fund', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });