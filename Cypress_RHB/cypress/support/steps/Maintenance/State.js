const random = require('../../../credential');


  var name = random.random_char(8)
  var name2 = random.random_char(10)
  
  Then('I Click State', (content) => {
    cy.xpath("//a[contains(text(),'State')]").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(1)').click({force: true})
    // cy.wait(5000)
  
  });

  Then('I Click Add Maintenance', (content) => {
    cy.get('.column:nth-child(2) > .field .button').click()
    // cy.wait(5000)
  
  });

  Then('I Fill Empty Form', (content) => {
    cy.get('.footer-form > .button:nth-child(1)').click()
    // cy.wait(5000)
  
  });

  Then('I Fill Form State', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    
    cy.get("select[name='countryId']").select('Singapore')
    cy.get("textarea[name='description']").type(random.random_char(100))

    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });

  Then('I Edit State', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)

    cy.get("select[name='countryId']").select('Malaysia')
    cy.get("textarea[name='description']").clear()
    cy.get("textarea[name='description']").type(random.random_char(100))


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });


  Then('I Delete State', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    // cy.get("input[name='code']").type(random.random_char(3))
    
    cy.wait(15000)
  
  });

  Then('I Delete State on DataBase', (content) => {

    // cy.exec('npm run delete')
    // cy.task('SELECT * FROM mnt_city' )
    cy.task('queryDb','DELETE FROM mnt_province WHERE name = "'+ name2 +'";');
    });