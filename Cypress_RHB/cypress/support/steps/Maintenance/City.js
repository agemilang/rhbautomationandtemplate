const random = require('../../../credential');


  var name = random.random_char(8)
  var name2 = random.random_char(10)
  
  Then('I Click City', (content) => {
    cy.xpath("//a[contains(text(),'City')]").click({force: true})

    // cy.get('.navbar-item:nth-child(3) .navbar-item:nth-child(1)').click({force: true})
    // cy.wait(5000)
  
  });

  

  Then('I Fill Form City', (content) => {
    // cy.get('.column:nth-child(2) > .field .button').click()
    cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").type(name)
    
    cy.get("select[name='provinceId']").select('Tokyo')
    cy.get("textarea[name='description']").type(random.random_char(100))

    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });

  Then('I Edit City', (content) => {

    
    //Search
    cy.get("input[name='name']").type(name)
    cy.get("button[type='submit']").click()
    cy.wait(5000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()

  //Edit
    cy.get(".footer-form > .button:nth-child(1)").click()
    // cy.get("input[name='code']").type(random.random_char(3))
    cy.get("input[name='name']").clear()
    cy.get("input[name='name']").type(name2)

    cy.get("select[name='provinceId']").select('Singapore')
    cy.get("textarea[name='description']").clear()
    cy.get("textarea[name='description']").type(random.random_char(100))


    
    cy.get('.footer-form > .button:nth-child(1)').click()
    cy.wait(15000)
  
  });


  Then('I Delete City', (content) => {

    // cy.exec('npm run delete')
    // cy.task('SELECT * FROM mnt_city' )
    
    // Search
    cy.get("input[name='name']").type(name2)
    cy.get("button[type='submit']").click()
    cy.wait(10000)
    cy.get('.table > tbody > tr > :nth-child(1)').first().click()
    cy.wait(10000)

  //Delete
    cy.get(".is-danger:nth-child(2)").click()
    cy.get(".is-danger:nth-child(1)").click()
    
    
    cy.wait(15000)
  
  });


Then('I Delete City on DataBase', (content) => {

  // cy.exec('npm run delete')
  // cy.task('SELECT * FROM mnt_city' )
  cy.task('queryDb','DELETE FROM mnt_city WHERE name = "'+ name2 +'";');
  });