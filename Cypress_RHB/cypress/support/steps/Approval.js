const random = require('../../credential');
  
  

  Then('I Approve Task Maintenance Add', () => {
    cy.get('.navbar-item:nth-child(2) .navbar-item:nth-child(2)').click({force: true})
    cy.get('tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1)').click()
    cy.wait(5000)
    cy.get('.footer-form > .button:nth-child(1)').click()

    // Reject
    // cy.get('.is-danger').click()
    // cy.get('.is-primary:nth-child(2)').click()

    // Approve
    cy.get('.modal-card-foot .button:nth-child(1)').click()
    cy.wait(5000)
    // Yes
    // cy.get('p > .button:nth-child(1)').click()
    // cy.wait(5000)
    // cy.contains("Success create new data")
    // cy.wait(5000)

  });

  Then('I approve Task Maintenance Edit', () => {
    cy.get('.navbar-item:nth-child(1) > .navbar-dropdown span').click({force: true})
    cy.wait(5000)
    cy.get(':nth-child(12) > .pagination-link').click()
    cy.wait(5000)
    cy.get('tbody > :nth-child(2) > :nth-child(2)').click()

    // Reject
    // cy.get('.is-danger').click()
    // cy.get('.is-primary:nth-child(2)').click()

    // Approve
    cy.get('.is-primary:nth-child(1)').click()
    // Yes
    cy.get('p > .button:nth-child(1)').click()
    cy.wait(5000)
    cy.contains("Success update data")
    cy.wait(5000)

  });

  Then('I approve Task Maintenance Delete', () => {
    cy.get('.navbar-item:nth-child(1) > .navbar-dropdown span').click({force: true})
    cy.wait(5000)
    cy.get(':nth-child(12) > .pagination-link').click()
    cy.wait(5000)
    cy.get('tbody > :nth-child(2) > :nth-child(2)').click()

    // Reject
    // cy.get('.is-danger').click()
    // cy.get('.is-primary:nth-child(2)').click()

    // Approve
    cy.get('.is-primary:nth-child(1)').click()
    // Yes
    cy.get('p > .button:nth-child(1)').click()
    cy.wait(5000)
    cy.contains("Success delete data")
    cy.wait(5000)

  });