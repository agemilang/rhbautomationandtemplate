// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const cucumber = require('cypress-cucumber-preprocessor').default
const sqlServer = require('cypress-sql-server');
const mysqlssh = require('mysql-ssh');
const fs = require('fs');

  

async function queryTestDb(query, config) {

  const client = await mysqlssh.connect
(
    {
        host: '13.228.200.152',
        user: 'arka',
        port: 22,
        privateKey: fs.readFileSync('/Users/admin/Documents/Work/Cypress_RHB/cypress/support/key/arka-rhb.pem')
    },
    {
        host: 'travelcard.cem67lu0xxit.ap-southeast-1.rds.amazonaws.com',
        user: 'travelcard',
        password: 'password',
        database: 'travelcard',
        port: 3306
    }
)


    return new Promise(resolve => {
    client.query(query, (error, results) => { 
        if (error) throw error;

        console.log(results); 
        mysqlssh.close();
        resolve(results);
    });
});

}



module.exports = (on, config) => {
  on('file:preprocessor', cucumber())
  on("task", {
    queryDb: async query => { 
        return await queryTestDb(query, config); 

    }
  });
}


