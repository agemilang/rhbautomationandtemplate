const faker = require('faker');

let userData = {
    randomfirstName: faker.name.firstName(),
    randomlastName: faker.name.lastName(),
    randomfirstName1: faker.name.firstName(),
    randomlastName1: faker.name.lastName(),
    randomfirstName2: faker.name.firstName(),
    randomlastName2: faker.name.lastName(),
    randomaddress: faker.address.streetAddress(),
    randomEmail: faker.internet.email(),
    randomPassword: faker.random.number(),
    randomphoneNumber1: faker.phone.phoneNumber(),
    randomphoneNumber2: faker.phone.phoneNumber(),

    randomdescription: faker.commerce.productDescription(),
    randomimage: faker.image.imageUrl(),

    randomlat: faker.address.latitude(),
    randomlong: faker.address.longitude()
}

function random_char(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function random_email(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function random_numeric(length) {
  var result           = '';
  var characters       = '1234567890';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}



  export {random_char, random_email, random_numeric, userData};