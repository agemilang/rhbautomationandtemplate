Feature: Nature Of Business

  I want to login RHB d0150s95
  


Scenario: Add And Approve Nature Of Business
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Nature Of Business
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Form Nature Of Business
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout


Scenario: Edit And Approve Nature Of Business 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Nature Of Business
    Then I Edit Nature Of Business
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete And Approve Nature Of Business 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Nature Of Business
    Then I Delete Nature Of Business
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout


Scenario: Delete Nature Of Business on Database
    Then I Delete Nature Of Business on DataBase

Scenario: Add And Reject Nature Of Business
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Nature Of Business
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Form Nature Of Business
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout
    