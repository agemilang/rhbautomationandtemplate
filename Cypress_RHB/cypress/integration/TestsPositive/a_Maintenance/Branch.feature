Feature: Branch

  I want to login RHB d0150s95
  


Scenario: Add And Approve Branch
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Branch
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Branch
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout


Scenario: Edit And Approve Branch 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Branch
    Then I Edit Branch
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete And Approve Branch 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Branch
    Then I Delete Branch
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete Branch on Database
    Then I Delete Branch on DataBase

Scenario: Add And Reject Branch
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Branch
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Branch
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout
    