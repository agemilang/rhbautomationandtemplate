Feature: Source Of Fund

I want to login RHB d0150s91
  
  Scenario: Add Source Of Fund
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Source Of Fund
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Source Of Fund
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

  Scenario: Edit And Approve Source Of Fund
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Source Of Fund
    Then I Edit Source Of Fund
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete And Approve Source Of Fund
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Source Of Fund
    Then I Delete Source Of Fund
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

 Scenario: Add And Reject Source Of Fund
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Source Of Fund
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Source Of Fund
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout