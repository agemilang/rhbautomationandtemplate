Feature: Scoring Maintenance

I want to login RHB d0150c100
  
  Scenario: Add Scoring Maintenance
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150c100 | password  |
    And I click on Sign in button
    Then I Click Scoring Maintenance
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Scoring Maintenance
    Then I Click Logout