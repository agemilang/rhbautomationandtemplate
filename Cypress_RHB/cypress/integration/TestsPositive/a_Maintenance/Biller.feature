Feature: Biller

  I want to login RHB d0150s91
  
  Scenario: Add And Approve Biller
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Biller
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Biller
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Edit And Approve Biller
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Biller
    Then I Edit Biller
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete And Approve Biller
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Biller
    Then I Delete Biller
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete Biller on Database
    Then I Delete Biller on DataBase

Scenario: Add And Reject Biller
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Biller
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Biller
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout