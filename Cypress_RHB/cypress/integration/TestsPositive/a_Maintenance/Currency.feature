Feature: Currency

  I want to login RHB d0150s95
  


Scenario: Add And Approve Currency
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Currency
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Form Currency
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout


Scenario: Edit And Approve Currency 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Currency
    Then I Edit Currency
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete And Approve Currency 
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Currency
    Then I Delete Currency
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete Currency on Database
    Then I Delete Currency on DataBase

Scenario: Add And Reject Currency
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Currency
    Then I Click Add Maintenance
    # Then I Fill Empty Form
    Then I Fill Form Currency
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout
    