Feature: Financial Institution

  I want to login RHB d0150s91
  
  Scenario: Add And Approval Financial Institution
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Financial Institution
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Financial Institution
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Edit And Approve Financial Institution
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Financial Institution
    Then I Edit Financial Institution
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout



Scenario: Delete And Approve Financial Institution
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Financial Institution
    Then I Delete Financial Institution
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Approve Task Maintenance Add
    Then I Click Logout

Scenario: Delete Financial Institution on Database
    Then I Delete Financial Institution on DataBase


Scenario: Add And Reject Financial Institution
    Given I open RHB login page
    When I type in RHB
        |  userid  | password  |
        |  d0150s91 | password  |
    And I click on Sign in button
    Then I Click Financial Institution
    Then I Click Add Maintenance
    Then I Fill Empty Form
    Then I Fill Form Financial Institution
    Then I Click Logout
    Then I type in RHB
        |  userid  | password  |
        |  d0150s95 | password  |
    And I click on Sign in button
    Then I Reject Task Maintenance Add
    Then I Click Logout