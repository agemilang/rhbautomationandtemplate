const faker = require('faker');
// const random = require('../credential');

var i;
var text = "";

let userData = {
    randomfirstName: faker.name.firstName(),
    randomlastName: faker.name.lastName(),
    randomfirstName1: faker.name.firstName(),
    randomlastName1: faker.name.lastName(),
    randomfirstName2: faker.name.firstName(),
    randomlastName2: faker.name.lastName(),
    randomaddress: faker.address.streetAddress(),
    randomEmail: faker.internet.email(),
    randomPassword: faker.random.number(),
    randomphoneNumber1: faker.phone.phoneNumber(),
    randomphoneNumber2: faker.phone.phoneNumber(),

    randomdescription: faker.commerce.productDescription(),
    randomimage: faker.image.imageUrl()
}

function random_char(length) {
    var result           = '';
    var characters       = 'ABCDEFG HIJK LMNOPQ RSTUV WXYZ abcde fghij klmn opqrs tuvwxyz 0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  
  function random_email(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  
  function random_numeric(length) {
    var result           = '';
    var characters       = '1234567890';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  const randomphoneno1 = random_numeric(12);
  const randomphoneno2 = random_numeric(12);
  const randomphoneno3 = random_numeric(12);

  const randomaccount = random_numeric(8);


const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
  path: './cypress/fixtures/corporateadd.csv',
  header: [
    {id: 'firstname', title: 'firstname'},
    {id: 'lastname', title: 'lastname'},
    {id: 'address', title: 'address'},
    {id: 'country', title: 'country'},
    {id: 'province', title: 'Province'},
    {id: 'city', title: 'City'},
    {id: 'registereddate', title: 'registereddate'},
    {id: 'phoneno', title: 'phone_no'},
    {id: 'Email', title: 'Email'},
    {id: 'serviceplan', title: 'servicePlan'},
    {id: 'firstnameadmin1', title: 'First Name_admin1'},
    {id: 'lastnameadmin1', title: 'lastname_admin1'},
    {id: 'useridadmin1', title: 'userid_admin1'},
    {id: 'useraliasadmin1', title: 'useralias_admin1'},
    {id: 'emailadmin1', title: 'email'},
    {id: 'mobilenoadmin1', title: 'mobileno'},
    {id: 'value', title: 'value'},
    {id: 'desc', title: 'desc'},
    {id: 'firstnameadmin2', title: 'First Name_admin2'},
    {id: 'lastnameadmin2', title: 'lastname_admin2'},
    {id: 'useridadmin2', title: 'userid_admin1'},
    {id: 'useraliasadmin2', title: 'useralias_admin2'},
    {id: 'emailadmin2', title: 'email'},
    {id: 'mobilenoadmin2', title: 'mobileno'},
    {id: 'accountno', title: 'account_no'},
    {id: 'accountname', title: 'account_name'},
    {id: 'accounttype', title: 'account_type'},
    {id: 'currency', title: 'currency'},
  ]
});

const data1 = [
  {
    firstname: userData.randomfirstName,
    lastname: userData.randomlastName,
    address: userData.randomaddress,
    country: 'IDN',
    province: 'ST',
    city: 'CT1',
    registereddate: '04/04/2021',
    phoneno: randomphoneno1,
    Email: userData.randomfirstName + '@yopmail.com',
    serviceplan: 'BASIC',
    firstnameadmin1: userData.randomfirstName1,
    lastnameadmin1: userData.randomlastName1,
    useridadmin1: userData.randomfirstName1,
    useraliasadmin1: userData.randomfirstName1 + userData.randomlastName1,
    emailadmin1: userData.randomfirstName1 + '@yopmail.com',
    mobilenoadmin1: randomphoneno2,
    value: ';',
    desc: '',
    firstnameadmin2: userData.randomfirstName2,
    lastnameadmin2: userData.randomlastName2,
    useridadmin2: userData.randomfirstName2,
    useraliasadmin2: userData.randomfirstName2 + userData.randomlastName2,
    emailadmin2: userData.randomfirstName2 + '@yopmail.com',
    mobilenoadmin2: randomphoneno3,
    accountno: randomaccount,
    accountname: userData.randomfirstName2 + userData.randomlastName2,
    accounttype: 'SA',
    currency: 'IDR',
  }
];

for (var i = 0, l = data.data1.length; i < l; i++) {
    var obj = data.data1[i];
    // ...
}

// for (i = 0; i < 5; i++) {
// csvWriter
//   .writeRecords(data)
//   .then(()=> console.log('The CSV file was written successfully'));
//   }
csvWriter
  .writeRecords(obj)
  .then(()=> console.log('The CSV file was written successfully'));