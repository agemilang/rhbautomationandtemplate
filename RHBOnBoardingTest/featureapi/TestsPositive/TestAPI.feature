Feature: Test CRUD methods in Sample REST API testing framework

#Background:
#	Given I set sample REST API url

#Scenario: POST post example
#  Given I Set POST posts api endpoint
# When I Set HEADER param request content type as "application/json."
#    And Set request Body
# And Send a POST HTTP request
# Then I receive valid HTTP response code 201
#    And Response BODY "POST" is non-empty.


Scenario: GET Customer Onboarding Manual KYC
  Given I POST admin login Manual KYC
   Then I Get List Customer Onboarding Get Identity No And ID Manual KYC
   Then I Get List Detail Customer Onboarding Manual KYC
   Then I Get List History Customer Onboarding Manual KYC
#   Then I Get List Document FATCA Customer Onboarding

Scenario: GET Id Task Customer Onboarding Manual KYC
  Given I Get List Task Approval Onboarding Get Task ID Manual KYC
   When I Get List Task Detail Approval Onboarding Get Onboard ID Manual KYC
   Then I Get List Task Document FATCA Approval Onboarding Manual KYC
   Then I Get List Task Document Approval by Task Stage Approval Onboarding Manual KYC
   Then I Get List Task Document PDF by Task ID Approval Onboarding Manual KYC
   Then I Post Upload Document Approval Onboarding Manual KYC
   Then I Post Action Task Approval Onboarding Manual KYC

Scenario: Stage Customer Onboarding Doc Check Maker
  Given I POST admin login Doc Check Maker
  Then I Get List Customer Onboarding Get Identity No And ID Doc Check Maker
  Then I Get List Task Approval Onboarding Get Task ID Doc Check Maker
  Then I Get List Task Detail Approval Onboarding Get Onboard ID Doc Check Maker
  Then I Post Upload Document Approval Onboarding Doc Check Maker
  Then I Post Action Task Approval Onboarding Doc Check Maker
#
Scenario: Stage Customer Onboarding Name Screening Maker
  Given I POST admin login Name Screening Maker
  Then I Get List Customer Onboarding Get Identity No And ID Name Screening Maker
  Then I Get List Task Approval Onboarding Get Task ID Name Screening Maker
  Then I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Maker
  Then I Get List Task Approval Onboarding Get Reason ID Name Screening Maker
  Then I Post Action Task Approval Onboarding Name Screening Maker
#
Scenario: Stage Customer Onboarding Name Screening Checker
  Given I POST admin login Name Screening Checker
  Then I Get List Customer Onboarding Get Identity No And ID Name Screening Checker
  Then I Get List Task Approval Onboarding Get Task ID Name Screening Checker
  Then I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Checker
  Then I Post Action Task Approval Onboarding Name Screening Checker
#
Scenario: Stage Customer Onboarding EDD
  Given I POST admin login EDD
  Then I Get List Customer Onboarding Get Identity No And ID EDD
  Then I Get List Task Approval Onboarding Get Task ID EDD
  Then I Get List Task Detail Approval Onboarding Get Onboard ID EDD
  Then I Post Upload Document Approval Onboarding EDD
  Then I Post Action Task Approval Onboarding EDD

#   Then I Get List Task Detail Customer Onboarding
#   Then I Get List Task Document FATCA Customer Onboarding
#   Then I Get List Detail Customer Onboarding
#   Then I Get List History Customer Onboarding




#  Then I Get List Task
#  Then I Get List Task Detail
#  Then I DELETE

#  When I Set HEADER param request content type as "application/json."
#  When Send GET HTTP request
#  Then I receive valid HTTP response code 200 for "GET."
#	And Response BODY "GET" is non-empty

#
#Scenario: UPDATE posts example
#  Given I Set PUT posts api endpoint for "1"
#  When I Set Update request Body
#	And Send PUT HTTP request
#  Then I receive valid HTTP response code 200 for "PUT."
#	And Response BODY "PUT" is non-empty
#
#
#Scenario: DELETE posts example
#  Given I Set DELETE posts api endpoint for "1"
#  When I Send DELETE HTTP request
#  Then I receive valid HTTP response code 200 for "DELETE."