from behave import given, when, then, step
import requests
import json

url = 'https://13.251.82.102'



#
@given(u'I POST admin login Manual KYC')
def login_admin(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s90",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    json_data = json.loads(response.text)
    json_formatted_str = json.dumps(json_data, indent=2)
    print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('Not Found.')


    # Menu Onboarding
    @then(u'I Get List Customer Onboarding Get Identity No And ID Manual KYC')
    def get_list_customer_onboarding(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=arka0658405968743@yopmail.com', headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")

        @then(u'I Get List Detail Customer Onboarding Manual KYC')
        def get_list_detail_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v1/customer/search/onboarding/'+ id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            json_data = json.loads(response.text)
            json_formatted_str = json.dumps(json_data, indent=2)
            print(json_formatted_str)
            print("\n")

        @then(u'I Get List History Customer Onboarding Manual KYC')
        def get_list_history_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v1/onboarding_history/onboarding/' + id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            json_data = json.loads(response.text)
            json_formatted_str = json.dumps(json_data, indent=2)
            print(json_formatted_str)
            print("\n")

        @then(u'I Get List Document FATCA Customer Onboarding Manual KYC')
        def get_list_document_fatca_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/onboarding_document/' + id + '/FATCA',
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            json_data = json.loads(response.text)
            json_formatted_str = json.dumps(json_data, indent=2)
            print(json_formatted_str)
            print("\n")



        # Menu Task Approval Onboarding
        @given(u'I Get List Task Approval Onboarding Get Task ID Manual KYC')
        def get_list_task_approval_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")

            @when(u'I Get List Task Detail Approval Onboarding Get Onboard ID Manual KYC')
            def get_list_task_detail_approval_onboarding(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")



                @then(u'I Get List Task Document FATCA Approval Onboarding Manual KYC')
                def get_list_task_document_fatca_approval_onboarding(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v2/onboarding_document/' + task_id + '/FATCA',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")

                @then(u'I Get List Task Document Approval by Task Stage Approval Onboarding Manual KYC')
                def get_list_task_document_approval_approval_onboarding(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v2/onboarding_document/' + task_id + '/APPROVAL_DOCUMENT/Manual%20KYC%20Override',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")

                @then(u'I Get List Task Document PDF by Task ID Approval Onboarding Manual KYC')
                def get_list_task_document_pdf_approval_onboarding(context):


                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(
                        url + '/api/v2/onboarding_document/' + task_id + '/pdf',
                        headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")



                @then(u'I Post Upload Document Approval Onboarding Manual KYC')
                def post_upload_document_approval_onboarding(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Manual KYC Override",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")


                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')


                @then(u'I Post Action Task Approval Onboarding Manual KYC')
                def post_action_task_approval_onboarding(context):

                    # print(admintoken)
                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer',  data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print(response.headers)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')





@given(u'I POST admin login Doc Check Maker')
def login_admin_doc_check_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s91",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('Not Found.')

    @then(u'I Get List Customer Onboarding Get Identity No And ID Doc Check Maker')
    def get_list_customer_onboarding_doc_check_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=arka0658405968743@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")


        @then(u'I Get List Task Approval Onboarding Get Task ID Doc Check Maker')
        def get_list_task_approval_onboarding_doc_check_maker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Doc Check Maker')
            def get_list_task_detail_approval_onboarding_doc_check_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")

                @then(u'I Post Upload Document Approval Onboarding Doc Check Maker')
                def post_upload_document_approval_onboarding_doc_check_maker(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Doc Check (Maker)",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')

                @then(u'I Post Action Task Approval Onboarding Doc Check Maker')
                def post_action_task_approval_onboarding_doc_check_maker(context):

                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print(response.headers)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')





@given(u'I POST admin login Name Screening Maker')
def login_admin_name_screening_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150c101",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('Not Found.')

    @then(u'I Get List Customer Onboarding Get Identity No And ID Name Screening Maker')
    def get_list_customer_onboarding_name_screening_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=arka0658405968743@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")



        @then(u'I Get List Task Approval Onboarding Get Task ID Name Screening Maker')
        def get_list_task_approval_onboarding_name_screening_maker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Maker')
            def get_list_task_detail_approval_onboarding_name_screening_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")

                @then(u'I Get List Task Approval Onboarding Get Reason ID Name Screening Maker')
                def get_list_task_approval_onboarding_name_screening_maker(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v1/reason/droplist/l1',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    print(response.json()[9]['id'])
                    reason_id = response.json()[9]['id']
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")


                    @then(u'I Post Action Task Approval Onboarding Name Screening Maker')
                    def post_action_task_approval_onboarding_name_screening_maker(context):

                        payload = {

                            "action": "recommend",
                            "comment": "hahahahha",
                            "id": task_id,
                            "reasonId": reason_id
                        }

                        headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                        response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                                 headers=headers, verify=False)
                        print(response)
                        print(response.headers)
                        print("\n")

                        if response.status_code == 200:
                            print('Success!')
                        elif response.status_code == 404:
                            print('Not Found.')
                        elif response.status_code == 500:
                            print('Internal Server Error.')




@given(u'I POST admin login Name Screening Checker')
def login_admin_name_screening_checker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s91",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('Not Found.')

    @then(u'I Get List Customer Onboarding Get Identity No And ID Name Screening Checker')
    def get_list_customer_onboarding_name_screening_checker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=arka0658405968743@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")



        @then(u'I Get List Task Approval Onboarding Get Task ID Name Screening Checker')
        def get_list_task_approval_onboarding_name_screening_checker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Checker')
            def get_list_task_detail_approval_onboarding_name_screening_checker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")




                @then(u'I Post Action Task Approval Onboarding Name Screening Checker')
                def post_action_task_approval_onboarding_name_screening_checker(context):

                    payload = {

                        "action": "recommend",
                        "comment": "hahahahha",
                        "id": task_id,
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print(response.headers)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')




@given(u'I POST admin login EDD')
def login_admin_doc_check_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s105",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    if response.status_code == 200:
        print('Success!')
    elif response.status_code == 404:
        print('Not Found.')

    @then(u'I Get List Customer Onboarding Get Identity No And ID EDD')
    def get_list_customer_onboarding_doc_check_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=arka0658405968743@yopmail.com',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        print("\n")


        @then(u'I Get List Task Approval Onboarding Get Task ID EDD')
        def get_list_task_approval_onboarding_doc_check_maker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID EDD')
            def get_list_task_detail_approval_onboarding_doc_check_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")

                @then(u'I Post Upload Document Approval Onboarding EDD')
                def post_upload_document_approval_onboarding_doc_check_maker(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Enhanced Due Dilligence",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')

                @then(u'I Post Action Task Approval Onboarding EDD')
                def post_action_task_approval_onboarding_doc_check_maker(context):

                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print(response.headers)
                    print("\n")

                    if response.status_code == 200:
                        print('Success!')
                    elif response.status_code == 404:
                        print('Not Found.')
                    elif response.status_code == 500:
                        print('Internal Server Error.')
#
#
# @then(u'I PUT')
# def step_impl(context):
#     payload = {
#     "name": "morpheus",
#     "job": "zion resident"
#     }
#
#     response = requests.post(url + '/api/users/2', data=payload)
#
#     print(response)
#     print(response.content)
#     print("\n")
#     # print(response.headers)
#
#     if response.status_code == 201:
#         print('Success!')
#     elif response.status_code == 404:
#         print('Not Found.')
#
# @then(u'I DELETE')
# def step_impl(context):
#     # payload = {
#     # "name": "morpheus",
#     # "job": "leader"
#     # }
#
#     response = requests.delete(url + '/api/users/2')
#
#     print(response)
#     print(response.content)
#     print("\n")
#     # print(response.headers)
#
#     if response.status_code == 204:
#         print('Success!')
#     elif response.status_code == 404:
#         print('Not Found.')




# api_endpoints = {}
# request_headers = {}
# response_codes ={}
# response_texts={}
# request_bodies = {}
# api_url='https://reqres.in/'
#
# @given(u'I set sample REST API url')
# def step_impl(context):
#     global api_url
#     api_url = 'https://reqres.in/'

# START POST Scenario
# @given(u'I Set POST posts api endpoint')
# def step_impl(context):
#     api_endpoints['POST_URL'] = api_url+'/posts'
#     print('url :'+api_endpoints['POST_URL'])
#
# @when(u'I Set HEADER param request content type as "{header_content_type}"')
# def step_impl(context, header_conent_type):
#     request_headers['Content-Type'] = header_conent_type
#
# #You may also include "And" or "But" as a step - these are renamed by behave to take the name of their preceding step, so:
# @when(u'Set request Body')
# def step_impl(context):
#     request_bodies['POST']={"title": "foo","body": "bar","userId": "1"}
#
# #You may also include "And" or "But" as a step - these are renamed by behave to take the name of their preceding step, so:
# @when(u'Send POST HTTP request')
# def step_impl(context):
#     # sending get request and saving response as response object
#     response = requests.post(url=api_endpoints['POST_URL'], json=request_bodies['POST'], headers=request_headers)
#     #response = requests.post(url=api_endpoints['POST_URL'], headers=request_headers) #https://jsonplaceholder.typicode.com/posts
#     # extracting response text
#     response_texts['POST']=response.text
#     print("post response :"+response.text)
#     # extracting response status_code
#     statuscode = response.status_code
#     response_codes['POST'] = statuscode
#
# @then(u'I receive valid HTTP response code 201')
# def step_impl(context):
#     print('Post rep code ;'+str(response_codes['POST']))
#     assert response_codes['POST'] == 201
# END POST Scenario






# START GET Scenario
# @given(u'I Set GET api endpoint')
# def step_impl(context):
#     api_endpoints['GET_URL'] = api_url+'/api/users?page=2'
#     print('url :'+api_endpoints['GET_URL'])
# #
# #You may also include "And" or "But" as a step - these are renamed by behave to take the name of their preceding step, so:
# @when(u'Send GET HTTP request')
# def step_impl(context):
#     # sending get request and saving response as response object
#     response = requests.get(url=api_endpoints['GET_URL'], headers=request_headers)
#     print(response)
#     # # extracting response text
#     response_texts['GET']=response.text
    # # extracting response status_code
    # statuscode = response.status_code
    # response_codes['GET'] = statuscode
#
# @then(u'I receive valid HTTP response code 200 for "{request_name}"')
# def step_impl(context,request_name):
#     print('Get rep code for '+request_name+':'+ str(response_codes[request_name]))
#     assert response_codes[request_name] == 200

# @then(u'Response BODY "{request_name}" is non-empty')
# def step_impl(context,request_name):
#     print('request_name: '+request_name)
#     print(response_texts)
#     assert response_texts[request_name] == not None
# END GET Scenario






#
# #START PUT/UPDATE
# @given(u'I Set PUT posts api endpoint for "{id}"')
# def step_impl(context,id):
#     api_endpoints['PUT_URL'] = api_url + '/posts/'+id
#     print('url :' + api_endpoints['PUT_URL'])
#
# @when(u'I Set Update request Body')
# def step_impl(context):
#     request_bodies['PUT']={"title": "foo","body": "bar","userId": "1","id": "1"}
#
# @when(u'Send PUT HTTP request')
# def step_impl(context):
#     # sending get request and saving response as response object  # response = requests.post(url=api_endpoints['POST_URL'], headers=request_headers) #https://jsonplaceholder.typicode.com/posts
#     response = requests.put(url=api_endpoints['PUT_URL'], json=request_bodies['PUT'], headers=request_headers)
#     # extracting response text
#     response_texts['PUT'] = response.text
#     print("update response :" + response.text)
#     # extracting response status_code
#     statuscode = response.status_code
#     response_codes['PUT'] = statuscode
# #END PUT/UPDATE
#
# #START DELETE
# @given(u'I Set DELETE posts api endpoint for "{id}"')
# def step_impl(context,id):
#     api_endpoints['DELETE_URL'] = api_url + '/posts/'+id
#     print('url :' + api_endpoints['DELETE_URL'])
#
# @when(u'I Send DELETE HTTP request')
# def step_impl(context):
#     # sending get request and saving response as response object
#     response = requests.delete(url=api_endpoints['DELETE_URL'])
#     # response = requests.post(url=api_endpoints['POST_URL'], headers=request_headers) #https://jsonplaceholder.typicode.com/posts
#     # extracting response text
#     response_texts['DELETE'] = response.text
#     print("DELETE response :" + response.text)
#     # extracting response status_code
#     statuscode = response.status_code
#     response_codes['DELETE'] = statuscode
# #END DELETE