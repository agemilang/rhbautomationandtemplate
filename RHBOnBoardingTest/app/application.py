from features.pages.launch_page import LaunchPageAndroid
from features.pages.launch_page_ios import LaunchPageIOS

from features.pages.RegisterIOS.RegisterMyKad_IOS import RegisterMyKad_IOS
from features.pages.RegisterIOS.RegisterMyInfo_IOS import RegisterMyInfo_IOS
from features.pages.RegisterIOS.ETBSMS_IOS import ETBSMS_IOS
from features.pages.RegisterIOS.RegisterForeigner_IOS import RegisterForeigner_IOS
from features.pages.LoginAfterAprovalAdminIOS.LoginMyKad_IOS import LoginMyKad_IOS


from features.pages.Sign_Out.Sign_Out import Sign_Out
from features.pages.Sign_Out_IOS.Sign_Out_IOS import Sign_Out_IOS

from features.pages.Register.RegisterMyKad import RegisterMyKad
from features.pages.Register.RegisterSingapore import RegisterSingapore
from features.pages.Register.RegisterForeigner import RegisterForeigner
from features.pages.LoginAfterApprovalAdmin.LoginMyKad import LoginMyKad
from features.pages.Register.ETBSMS import ETBSMS
# from features.pages.EApps.EAppsMyKad import EAppsMyKad

# from features.pages.main_page import MainPage


class Application:
    def __init__(self, driver):
        self.launch_page = LaunchPageAndroid(driver)
        self.launch_page_ios = LaunchPageIOS(driver)

        self.RegisterMyKad = RegisterMyKad(driver)
        self.RegisterSingapore = RegisterSingapore(driver)
        self.RegisterForeigner = RegisterForeigner(driver)
        self.ETBSMS = ETBSMS(driver)
        self.Sign_Out = Sign_Out(driver)

        self.RegisterMyKad_IOS = RegisterMyKad_IOS(driver)
        self.RegisterMyInfo_IOS = RegisterMyInfo_IOS(driver)
        self.RegisterForeigner_IOS = RegisterForeigner_IOS(driver)
        self.ETBSMS_IOS = ETBSMS_IOS(driver)
        self.Sign_Out_IOS = Sign_Out_IOS(driver)
        self.LoginMyKad_IOS = LoginMyKad_IOS(driver)


        self.LoginMyKad = LoginMyKad(driver)
        # self.EAppsMyKad = EAppsMyKad(driver)
        # self.main_page = MainPage(driver)
