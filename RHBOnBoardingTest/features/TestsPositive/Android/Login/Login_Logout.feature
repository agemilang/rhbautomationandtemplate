Feature: Tests for RHB application

Scenario: Successful Positif Test Login Logout
    Given I allow phone call permission
    When I open login page
    Then I Fill Login Existing
    Then I Fill Create PIN After Approval
    Then I Click Skip
    Then I Click Hamburger Menu
    Then I Click Sign Out
    Then I Fill PIN After Sign Out
    Then I Click Maybe Later
    Then I Click Skip
    Then I Click Hamburger Menu
    Then I Click Sign Out