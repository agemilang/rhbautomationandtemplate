Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration Foreigner WorkingSingapore No Mailing No
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register 3
    Then I Click Next Button Email
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose Foreigner
    Then I Click Next Button Take Picture Passport
    Then I Fill Personal Information Working at Singapore No Mailing No
    Then I Fill Mailing Address
    Then I Take Photo Proof of Residence
    Then I Choose 3 Product Application
    Then I Fill Additional Information
    Then I Fill Account Information 1
    Then I Agree with Term & Condition
    Then I Fill Email & Password
    Then I Fill Create PIN
    Then I Click MB Banking

