Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration Foreigner WorkingSingapore No Mailing Yes
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register 2
    Then I Click Next Button Email
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose Foreigner
    Then I Click Next Button Take Picture Passport
    Then I Fill Personal Information Working at Singapore No Mailing Yes
    Then I Take Photo Proof of Residence
    Then I Choose 3 Product Application
    Then I Fill Additional Information
    Then I Fill Account Information 3
    Then I Agree with Term & Condition
    Then I Fill Email & Password 5
    Then I Fill Create PIN
    Then I Click MB Banking

