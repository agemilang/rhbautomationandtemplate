Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration MyKad Working Singapore Yes Residential No Mailing No 3 Product Employed
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register 3
    Then I Click Next Button Email
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose MyKad
    Then I Click Next Button Take Picture
    Then I Fill Personal Information Working at Singapore Yes Residential No Mailing No
    Then I Fill Mailing Address
    Then I Take Photo Signature
    Then I Take Photo Employment Pass
    Then I Choose Product HYSP & TRIO
    Then I Fill Additional Information
    Then I Fill Account Information
    Then I Agree with Term & Condition
    Then I Fill Email & Password 3
    Then I Fill Create PIN
    Then I Click MB Banking

