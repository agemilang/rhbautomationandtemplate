Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration MyKad Working Singapore No Residential No Mailing Yes 3 Product Employed
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register 6
    Then I Click Next Button Email
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose MyKad
    Then I Click Next Button Take Picture
    Then I Fill Personal Information Working at Singapore No Residential No Mailing Yes
    Then I Take Photo Proof of Residence
    Then I Choose 3 Product Application
    Then I Fill Additional Information
    Then I Fill Account Information
    Then I Agree with Term & Condition
    Then I Fill Email & Password 6
    Then I Fill Create PIN
    Then I Click MB Banking

