Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration MyKad Working Singapore No Residential Yes Mailing No 3 Product Employed
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register 5
    Then I Click Next Button Email
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose MyKad
    Then I Click Next Button Take Picture
    Then I Fill Personal Information Working at Singapore No Residential Yes Mailing No
    Then I Fill Mailing Address
    Then I Choose Product HYSP & PREMIER PLUS
    Then I Fill Additional Information
    Then I Fill Account Information
    Then I Agree with Term & Condition
    Then I Fill Email & Password 5
    Then I Fill Create PIN
    Then I Click MB Banking

