Feature: Tests for RHB application

Scenario: Successful Positif Test Login Logout
    Given I open login page IOS
    Then I Fill Login Existing IOS
    Then I Fill PIN IOS
    Then I Click Skip IOS
    Then I Click Hamburger Menu IOS
    Then I Click Sign Out IOS
    Then I Fill PIN After Sign Out IOS
    Then I Click Maybe Later IOS
    Then I Click Skip IOS
    Then I Click Hamburger Menu IOS
    Then I Click Sign Out IOS