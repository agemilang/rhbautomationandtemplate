Feature: Tests for RHB application IOS

Scenario: Successful IOS
#    Given I allow phone call permission
    Given I open register page IOS
#    When I Allow RHB Mobile to access this device's location? IOS
    Then I Fill Registration IOS 1
    Then I Fill OTP IOS
    Then I Fill Password IOS
    Then I Choose MyKad IOS
    Then I Take Photo MyKad IOS
    Then I Fill Personal Information Working at Singapore Yes Residential Yes Mailing Yes IOS
    Then I Take Picture Employment Pass IOS
    Then I Select Product TRIO IOS
    Then I Fill Additional Information IOS
    Then I Fill Account Information IOS
    Then I Check Term & Condition IOS
    Then I Fill Login After Register IOS 1
    Then I Fill PIN After Register IOS
    Then I Click Mobile Banking IOS




#
#    API
Scenario: GET Customer Onboarding Manual KYC
  Given I POST admin login Manual KYC
   Then I Get List Customer Onboarding Get Identity No And ID Manual KYC
   Then I Get List Detail Customer Onboarding Manual KYC
   Then I Get List History Customer Onboarding Manual KYC
#   Then I Get List Document FATCA Customer Onboarding

   Then I Get List Task Approval Onboarding Get Task ID Manual KYC
   Then I Get List Task Detail Approval Onboarding Get Onboard ID Manual KYC
   Then I Get List Task Document FATCA Approval Onboarding Manual KYC
   Then I Get List Task Document Approval by Task Stage Approval Onboarding Manual KYC
   Then I Get List Task Document PDF by Task ID Approval Onboarding Manual KYC
   Then I Post Upload Document Approval Onboarding Manual KYC
   Then I Post Action Task Approval Onboarding Manual KYC Approve

Scenario: Stage Customer Onboarding Doc Check Maker
  Given I POST admin login Doc Check Maker
  Then I Get List Customer Onboarding Get Identity No And ID Doc Check Maker
  Then I Get List Task Approval Onboarding Get Task ID Doc Check Maker
  Then I Get List Task Detail Approval Onboarding Get Onboard ID Doc Check Maker
  Then I Post Upload Document Approval Onboarding Doc Check Maker
  Then I Post Action Task Approval Onboarding Doc Check Maker Approve
#
#Scenario: Stage Customer Onboarding Name Screening Maker
#  Given I POST admin login Name Screening Maker
#  Then I Get List Customer Onboarding Get Identity No And ID Name Screening Maker
#  Then I Get List Task Approval Onboarding Get Task ID Name Screening Maker
#  Then I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Maker
#  Then I Get List Task Approval Onboarding Get Reason ID Name Screening Maker
#  Then I Post Action Task Approval Onboarding Name Screening Maker Approve
##
#Scenario: Stage Customer Onboarding Name Screening Checker
#  Given I POST admin login Name Screening Checker
#  Then I Get List Customer Onboarding Get Identity No And ID Name Screening Checker
#  Then I Get List Task Approval Onboarding Get Task ID Name Screening Checker
#  Then I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Checker
#  Then I Post Action Task Approval Onboarding Name Screening Checker Approve
##
Scenario: Stage Customer Onboarding EDD
  Given I POST admin login EDD
  Then I Get List Customer Onboarding Get Identity No And ID EDD
  Then I Get List Task Approval Onboarding Get Task ID EDD
  Then I Get List Task Detail Approval Onboarding Get Onboard ID EDD
  Then I Post Upload Document Approval Onboarding EDD
  Then I Post Action Task Approval Onboarding EDD Approve

#
#Scenario: Successful Positif Test Login After Approval MyKad Working Singapore Yes Residential Yes Mailing Yes 3 Product Employed
#  Given I open login page IOS
#    Then I Fill Login After Register IOS 1