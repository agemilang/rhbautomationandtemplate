Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration MyKad Working Singapore Yes Residential No Mailing Yes 3 Product Employed
    Given I open register page IOS
#    When I Allow RHB Mobile to access this device's location? IOS
    Then I Fill Registration IOS 3
    Then I Fill OTP IOS
    Then I Fill Password IOS
    Then I Choose MyKad IOS
    Then I Take Photo MyKad IOS
    Then I Fill Personal Information Working at Singapore Yes Residential No Mailing Yes IOS
    Then I Take Picture Proof of Residential IOS
    Then I Take Picture Employment Pass IOS
    Then I Select Product HYSA IOS
    Then I Fill Additional Information IOS
    Then I Fill Account Information IOS
    Then I Check Term & Condition IOS
    Then I Fill Login After Register IOS 3
    Then I Fill PIN After Register IOS
    Then I Click Mobile Banking IOS
