Feature: Tests for RHB application

Scenario: Successful Positif Test Register RHBMobileRegistration Foreigner WorkingSingapore Yes Mailing No
    Given I open register page IOS
#    When I Allow RHB Mobile to access this device's location? IOS
    Then I Fill Registration IOS 2
    Then I Fill OTP IOS
    Then I Fill Password IOS
    Then I Choose Foreigner IOS
    Then I Click Next Button Take Picture Passport IOS
    Then I Fill Personal Information Working at Singapore Yes Mailing No IOS
    Then I Fill Mailing Address IOS
    Then I Take Photo Proof of Residence IOS
    Then I Take Photo Employment Pass IOS
    Then I Select Product HYSA IOS
    Then I Fill Additional Information IOS
    Then I Fill Account Information IOS
    Then I Check Term & Condition IOS
    Then I Fill Login After Register IOS 2
    Then I Fill PIN After Register IOS
    Then I Click Mobile Banking IOS
