from behave import given, when, then
import requests
import json
from credentials import email, email1, email2, email3, email4, email5, email6, email7, url
import datetime



@given(u'I POST admin login Manual KYC')
def login_admin(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s90",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=4)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    # Menu Onboarding
    @then(u'I Get List Customer Onboarding Get Identity No And ID Manual KYC')
    def get_list_customer_onboarding(context):

        print(email)
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=' + email, headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")

        assert response.status_code == 200


        @then(u'I Get List Detail Customer Onboarding Manual KYC')
        def get_list_detail_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v1/customer/search/onboarding/'+ id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=4)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200

        @then(u'I Get List History Customer Onboarding Manual KYC')
        def get_list_history_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v1/onboarding_history/onboarding/' + id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=4)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200

        @then(u'I Get List Document FATCA Customer Onboarding Manual KYC')
        def get_list_document_fatca_customer_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/onboarding_document/' + id + '/FATCA',
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=4)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200

        # Menu Task Approval Onboarding
        @then(u'I Get List Task Approval Onboarding Get Task ID Manual KYC')
        def get_list_task_approval_onboarding(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200

            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Manual KYC')
            def get_list_task_detail_approval_onboarding(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")

                assert response.status_code == 200



                @then(u'I Get List Task Document FATCA Approval Onboarding Manual KYC')
                def get_list_task_document_fatca_approval_onboarding(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v2/onboarding_document/' + task_id + '/FATCA',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Get List Task Document Approval by Task Stage Approval Onboarding Manual KYC')
                def get_list_task_document_approval_approval_onboarding(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v2/onboarding_document/' + task_id + '/APPROVAL_DOCUMENT/Manual%20KYC%20Override',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Get List Task Document PDF by Task ID Approval Onboarding Manual KYC')
                def get_list_task_document_pdf_approval_onboarding(context):


                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(
                        url + '/api/v2/onboarding_document/' + task_id + '/pdf',
                        headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")

                    assert response.status_code == 200



                @then(u'I Post Upload Document Approval Onboarding Manual KYC')
                def post_upload_document_approval_onboarding(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Manual KYC Override",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")


                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding Manual KYC Approve')
                def post_action_task_approval_onboarding(context):

                    # print(admintoken)
                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer',  data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding Manual KYC Reject')
                def post_action_task_approval_onboarding(context):

                    # print(admintoken)
                    payload = {
                        "action": "reject",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer',  data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200




@given(u'I POST admin login Doc Check Maker')
def login_admin_doc_check_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s91",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer Onboarding Get Identity No And ID Doc Check Maker')
    def get_list_customer_onboarding_doc_check_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username='+ email,
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")

        assert response.status_code == 200


        @then(u'I Get List Task Approval Onboarding Get Task ID Doc Check Maker')
        def get_list_task_approval_onboarding_doc_check_maker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200



            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Doc Check Maker')
            def get_list_task_detail_approval_onboarding_doc_check_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                # print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                print(response.json()['object']['onboardId'])


                alias1 = response.json()['object']['customerInformationDto']['alias1']
                print(alias1)

                alias2 = response.json()['object']['customerInformationDto']['alias2']
                print(alias2)

                countryOfBirthId = response.json()['object']['customerInformationDto']['countryOfBirthId']
                print(countryOfBirthId)

                dateOfBirthold = response.json()['object']['customerInformationDto']['dateOfBirth']
                print(dateOfBirthold)
                print(datetime.datetime.strptime(dateOfBirthold, '%d/%m/%Y').strftime('%Y-%m-%d'))
                dateOfBirth = datetime.datetime.strptime(dateOfBirthold, '%d/%m/%Y').strftime('%Y-%m-%d')



                genderCode = response.json()['object']['customerInformationDto']['genderCode']
                print(genderCode)

                identityNo = response.json()['object']['customerInformationDto']['identityNo']
                print(identityNo)



                # print(response.json()['object']['customerInformationDto'])
                addressLine1 = response.json()['object']['mailingAddressDto']['addressLine1']
                print(addressLine1)

                addressLine2 = response.json()['object']['mailingAddressDto']['addressLine2']
                print(addressLine2)

                cityName = response.json()['object']['mailingAddressDto']['cityName']
                print(cityName)

                countryId = response.json()['object']['mailingAddressDto']['countryId']
                print(countryId)

                floorNo = response.json()['object']['mailingAddressDto']['floorNo']
                print(floorNo)

                postalCode = response.json()['object']['mailingAddressDto']['postalCode']
                print(postalCode)

                stateId = response.json()['object']['mailingAddressDto']['stateId']
                print(stateId)




                mailingAddressSame = response.json()['object']['mailSame']
                print(mailingAddressSame)

                maritalStatusCode = response.json()['object']['customerInformationDto']['maritalStatusCode']
                print(maritalStatusCode)

                name = response.json()['object']['customerInformationDto']['fullName']
                print(name)

                nationalityCode = response.json()['object']['customerInformationDto']['nationalityCode']
                print(nationalityCode)


                residentialaddressLine1 = response.json()['object']['residentialAddressDto']['addressLine1']
                print(residentialaddressLine1)
                residentialaddressLine2 = response.json()['object']['residentialAddressDto']['addressLine2']
                print(residentialaddressLine2)
                residentialcityName = response.json()['object']['residentialAddressDto']['cityName']
                print(residentialcityName)
                residentialcountryId = response.json()['object']['residentialAddressDto']['countryId']
                print(residentialcountryId)
                residentialfloorNo = response.json()['object']['residentialAddressDto']['floorNo']
                print(residentialfloorNo)
                residentialpostalCode = response.json()['object']['residentialAddressDto']['postalCode']
                print(residentialpostalCode)
                residentialstateId = response.json()['object']['residentialAddressDto']['stateId']
                print(residentialstateId)

                # print(response.json()['object']['residentialAddressDto'])

                # print(customerInformationDto,mailingAddressDto,residentialAddressDto)
                # Body json
                json_data = json.loads(response.text)
                json_formatted_str = json.dumps(json_data, indent=2)
                print(json_formatted_str)

                print("\n")

                assert response.status_code == 200

                @then(u'I Post Upload Document Approval Onboarding Doc Check Maker')
                def post_upload_document_approval_onboarding_doc_check_maker(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Doc Check (Maker)",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding Doc Check Maker Approve')
                def post_action_task_approval_onboarding_doc_check_maker_approve(context):

                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200


                @then(u'I Post Action Task Approval Onboarding Doc Check Maker Reject')
                def post_action_task_approval_onboarding_doc_check_maker_reject(context):

                    payload = {
                        "action": "reject",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200


                @then(u'I Post Action Task Approval Onboarding Doc Check Maker Amend')
                def post_action_task_approval_onboarding_doc_check_maker_amend(context):

                    payload = {
                        "action": "amend",

                        "amendmentDto": {
                        "customerInformationDto":{"alias1":alias1,"alias2":alias2,"countryOfBirthId":countryOfBirthId,"dateOfBirth":dateOfBirth,"genderCode":genderCode,"identityNo":identityNo},
                        # "mailingAddressDto":{"addressLine1":addressLine1,"addressLine2":addressLine2,"cityName":cityName,"countryId":countryId,"floorNo":floorNo,"postalCode":postalCode, "stateId":stateId},
                        "mailingAddressSame":mailingAddressSame, "maritalStatusCode":maritalStatusCode, "name":name, "nationalityCode":nationalityCode,
                        "residentialAddressDto":{"addressLine1":residentialaddressLine1,"addressLine2":residentialaddressLine2,"cityName":residentialcityName,"countryId":residentialcountryId,"floorNo":residentialfloorNo,"postalCode":residentialpostalCode, "stateId":residentialstateId},
                        "comment": "dwadwadawdawdaw",
                        "id": "539f473e-e6d4-4dc5-9d66-de4c3e28832d",
                        }
                    }

                    print(payload)

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)

                    json_data = json.loads(response.text)
                    json_formatted_str = json.dumps(json_data, indent=2)
                    print(json_formatted_str)
                    print("\n")

                    assert response.status_code == 200






@given(u'I POST admin login Name Screening Maker')
def login_admin_name_screening_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150c101",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer Onboarding Get Identity No And ID Name Screening Maker')
    def get_list_customer_onboarding_name_screening_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=' + email,
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")
        assert response.status_code == 200



        @then(u'I Get List Task Approval Onboarding Get Task ID Name Screening Maker')
        def get_list_task_approval_onboarding_name_screening_maker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)

            # Body json
            # print(response.json()['content'][0]['id'])
            print(response.json())
            task_id = response.json()['content'][0]['id']
            json_data = json.loads(response.text)
            json_formatted_str = json.dumps(json_data)
            print(json_formatted_str)
            print(json_data)
            print("\n")
            assert response.status_code == 200


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Maker')
            def get_list_task_detail_approval_onboarding_name_screening_maker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                # print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)
                assert response.status_code == 200
                print("\n")


                @then(u'I Get List Task Approval Onboarding Get Reason ID Name Screening Maker')
                def get_list_task_approval_onboarding_name_screening_maker(context):
                    headers = {"Authorization": "Bearer" + admintoken}
                    response = requests.get(url + '/api/v1/reason/droplist/l1',
                                            headers=headers, verify=False)
                    # Code response
                    print(response)
                    # Body json
                    print(response.json()[9]['id'])
                    reason_id = response.json()[9]['id']
                    # json_data = json.loads(response.text)
                    # json_formatted_str = json.dumps(json_data, indent=2)
                    # print(json_formatted_str)
                    print("\n")
                    assert response.status_code == 200


                    @then(u'I Post Action Task Approval Onboarding Name Screening Maker Approve')
                    def post_action_task_approval_onboarding_name_screening_maker(context):

                        payload = {

                            "action": "recommend",
                            "comment": "hahahahha",
                            "id": task_id,
                            "reasonId": reason_id
                        }

                        headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                        response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                                 headers=headers, verify=False)
                        print(response)
                        print("\n")

                        assert response.status_code == 200


                    @then(u'I Post Action Task Approval Onboarding Name Screening Maker Reject')
                    def post_action_task_approval_onboarding_name_screening_maker(context):

                        payload = {

                            "action": "reject",
                            "comment": "hahahahha",
                            "id": task_id,
                            "reasonId": reason_id
                        }

                        headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                        response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                                 headers=headers, verify=False)
                        print(response)
                        print("\n")

                        assert response.status_code == 200

@given(u'I POST admin login Name Screening Checker')
def login_admin_name_screening_checker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s91",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer Onboarding Get Identity No And ID Name Screening Checker')
    def get_list_customer_onboarding_name_screening_checker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=' + email,
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")

        assert response.status_code == 200


        @then(u'I Get List Task Approval Onboarding Get Task ID Name Screening Checker')
        def get_list_task_approval_onboarding_name_screening_checker(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            json_data = json.loads(response.text)
            json_formatted_str = json.dumps(json_data, indent=2)
            print(json_formatted_str)
            print("\n")
            assert response.status_code == 200


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID Name Screening Checker')
            def get_list_task_detail_approval_onboarding_name_screening_checker(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                # print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")
                assert response.status_code == 200



                @then(u'I Post Action Task Approval Onboarding Name Screening Checker Approve')
                def post_action_task_approval_onboarding_name_screening_checker(context):

                    payload = {

                        "action": "recommend",
                        "comment": "hahahahha",
                        "id": task_id,
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding Name Screening Checker Reject')
                def post_action_task_approval_onboarding_name_screening_checker(context):

                    payload = {

                        "action": "reject",
                        "comment": "hahahahha",
                        "id": task_id,
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200


@given(u'I POST admin login EDD')
def login_admin_edd(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s105",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Customer Onboarding Get Identity No And ID EDD')
    def get_list_customer_onboarding_edd(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/onboarding?username=' + email,
                                headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        # print(response.text)
        print(response.json()['content'][0]['identityNo'])
        identityno = response.json()['content'][0]['identityNo']
        print("\n")
        assert response.status_code == 200


        @then(u'I Get List Task Approval Onboarding Get Task ID EDD')
        def get_list_task_approval_onboarding_edd(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.get(url + '/api/v2/apprv/onboarding/customer?objectId=' + identityno,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.json()['content'][0]['id'])
            task_id = response.json()['content'][0]['id']
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=2)
            # print(json_formatted_str)
            print("\n")
            assert response.status_code == 200


            @then(u'I Get List Task Detail Approval Onboarding Get Onboard ID EDD')
            def get_list_task_detail_approval_onboarding_edd(context):
                headers = {"Authorization": "Bearer" + admintoken}
                response = requests.get(url + '/api/v2/apprv/onboarding/customer/' + task_id,
                                        headers=headers, verify=False)
                # Code response
                print(response)
                print(response.json()['object']['onboardId'])
                id_onboard = response.json()['object']['onboardId']
                # Body json
                # json_data = json.loads(response.text)
                # json_formatted_str = json.dumps(json_data, indent=2)
                # print(json_formatted_str)

                print("\n")
                assert response.status_code == 200

                @then(u'I Post Upload Document Approval Onboarding EDD')
                def post_upload_document_approval_onboarding_edd(context):
                    files = {'file': open('DeskripsiMaskot.pdf', 'rb')}

                    payload = {

                        "onboardId": id_onboard,
                        "taskName": "Enhanced Due Dilligence",
                        "documentType": "APPROVAL_DOCUMENT"
                    }

                    headers = {"Authorization": "Bearer " + admintoken}
                    response = requests.post(url + '/api/v2/onboarding_document', files=files, data=payload,
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding EDD Approve')
                def post_action_task_approval_onboarding_edd(context):

                    payload = {
                        "action": "approve",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200

                @then(u'I Post Action Task Approval Onboarding EDD Reject')
                def post_action_task_approval_onboarding_edd(context):
                    payload = {
                        "action": "reject",
                        "comment": "",
                        "id": task_id
                    }

                    headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
                    response = requests.post(url + '/api/v2/apprv/onboarding/customer', data=json.dumps(payload),
                                             headers=headers, verify=False)
                    print(response)
                    print("\n")

                    assert response.status_code == 200