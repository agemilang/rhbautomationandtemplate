from behave import given, when, then


@then("I Fill Login After Register IOS 1")
def login_after_register(context):
    context.app.LoginMyKad_IOS.login_after_register_ios1()

@then("I Fill Login After Register IOS 2")
def login_after_register(context):
    context.app.LoginMyKad_IOS.login_after_register_ios2()

@then("I Fill Login After Register IOS 3")
def login_after_register(context):
    context.app.LoginMyKad_IOS.login_after_register_ios3()

@then("I Fill Login After Register IOS 4")
def login_after_register(context):
    context.app.LoginMyKad_IOS.login_after_register_ios4()

@then("I Fill PIN After Register IOS")
def i_fill_create_pin_ios(context):
    context.app.LoginMyKad_IOS.i_fill_create_pin_ios()

@then("I Click Mobile Banking IOS")
def i_click_mobile_banking_ios(context):
    context.app.LoginMyKad_IOS.i_click_mobile_banking_ios()

@then("I Fill Login Existing IOS")
def login_after_register(context):
    context.app.LoginMyKad_IOS.login_existing_ios()

@then("I Fill PIN IOS")
def i_fill_create_pin_ios(context):
    context.app.LoginMyKad_IOS.i_fill_create_pin_ios()

# @then("I Click Maybe Later IOS")
# def click_maybe_later(context):
#     context.app.Sign_Out_IOS.click_maybe_later()