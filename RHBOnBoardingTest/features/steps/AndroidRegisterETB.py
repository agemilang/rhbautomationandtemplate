from behave import given, when, then
import requests
import json
from credentials import  email, email1, email2, email3, email4, email5, email6, email7, url


@when("I open register page existing")
def create_new_account_etb(context):
    context.app.launch_page.create_new_account_etb()

@then("I select register sms otp")
def select_register_smsotp(context):
    context.app.ETBSMS.select_register_smsotp()

@then("I fill identity")
def select_register_smsotp(context):
    context.app.ETBSMS.fill_identity()

@then("I fill create credential")
def fill_create_credential(context):
    context.app.ETBSMS.fill_create_credential()