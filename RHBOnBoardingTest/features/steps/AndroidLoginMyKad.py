from behave import given, when, then

@given("I allow phone call permission login")
def allow_permission_phonecalls(context):
    context.app.launch_page.allow_permission_phonecalls()


@when("I Fill Email & Password After Approval")
def fill_login(context):
    context.app.LoginMyKad.fill_login()

@when("I Fill Email & Password After Reject")
def fill_login(context):
    context.app.LoginMyKad.fill_login()




@then("I Fill Create PIN After Approval")
def button_login(context):
    context.app.RegisterMyKad.create_pin()

@then("I Fill Create PIN After Reject")
def button_login(context):
    context.app.RegisterMyKad.create_pin()

@then("I Click Skip Activation Soft Token After Approval")
def button_login(context):
    context.app.LoginMyKad.click_skip_activation()

@then("I Click Mobile Banking After Approval")
def button_login(context):
    context.app.LoginMyKad.click_mobile_banking()

@then("I Click Mobile Banking After Reject")
def button_login(context):
    context.app.LoginMyKad.click_mobile_banking()

@then("I Check Message Error Account Rejected")
def check_message_error_rejected(context):
    context.app.LoginMyKad.check_message_error_rejected()




@then("I Click Hamburger Menu and E apps")
def button_login(context):
    context.app.EAppsMyKad.click_hamburger_menu()

@then("I Choose Product")
def button_login(context):
    context.app.EAppsMyKad.choose_product()

@then("I Fill Form Account Detail")
def button_login(context):
    context.app.EAppsMyKad.fill_form_account_detail()

@then("I Fill Form Placement Detail")
def button_login(context):
    context.app.EAppsMyKad.fill_form_placement_detail()

@then("I Fill Login Existing")
def fill_login_existing(context):
    context.app.RegisterMyKad.fill_login_existing()






