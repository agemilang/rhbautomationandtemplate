from behave import given, when, then


@then("I Choose MyInfo Singaporean")
def choose_singaporean(context):
    context.app.RegisterSingapore.choose_singaporean()

@then("I Choose Id MyInfo Singaporean")
def choose_id_singaporean(context):
    context.app.RegisterSingapore.choose_id_singaporean()