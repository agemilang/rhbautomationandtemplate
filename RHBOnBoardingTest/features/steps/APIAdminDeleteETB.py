from behave import given, when, then
import requests
import json
from credentials import email, email1, email2, email3, email4, email5, email6, email7, url
import datetime



@given(u'I POST admin login Customer Management')
def login_admin(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s105",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=4)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200


    @then(u'I Get List Customer Management S9912375C')
    def get_list_customer_management(context):

        print(email)
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v1/customer/search/management?size=10&page=1&username=&identityNo=S9912375C', headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        print(response.text)
        # print(response.json()['content'][0]['identityNo'])
        # identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")
        print(id)
        assert response.status_code == 200

    @then(u'I Get List Customer Management S9812390C')
    def get_list_customer_management(context):
        print(email)
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(
            url + '/api/v1/customer/search/management?size=10&page=1&username=&identityNo=S9812390C',
            headers=headers, verify=False)
        # Code response
        print(response)
        # Body json
        print(response.text)
        # print(response.json()['content'][0]['identityNo'])
        # identityno = response.json()['content'][0]['identityNo']
        id = response.json()['content'][0]['id']
        print("\n")
        print(id)
        assert response.status_code == 200


        @then(u'Delete Customer Management')
        def delete_customer_management(context):
            headers = {"Authorization": "Bearer" + admintoken}
            response = requests.delete(url + '/api/v1/customer/search/management/'+ id,
                                    headers=headers, verify=False)
            # Code response
            print(response)
            # Body json
            # print(response.text)
            # json_data = json.loads(response.text)
            # json_formatted_str = json.dumps(json_data, indent=4)
            # print(json_formatted_str)
            print("\n")

            assert response.status_code == 200





@then(u'I POST admin login Approve Delete Customer ETB')
def login_admin_doc_check_maker(context):
    payload = {
        "grant_type": "password",
        "username": "d0150s90",
        "password": "mm",
        "scope": "read",
        "client_id": "backoffice"
    }

    headers = {"Authorization" : "Basic YmFja29mZmljZTptVm03P1ZhQy1MJTReY1Nt"}
    response = requests.post(url + '/oauth/token', data=payload, headers=headers, verify=False)
    print(response)
    print("\n")
    # json_data = json.loads(response.text)
    # json_formatted_str = json.dumps(json_data, indent=2)
    # print(json_formatted_str)
    # print(response.json()['access_token'])
    admintoken = response.json()['access_token']

    assert response.status_code == 200

    @then(u'I Get List Task Detail Approval Customer Get ID customer')
    def get_list_task_detail_approval_onboarding_doc_check_maker(context):
        headers = {"Authorization": "Bearer" + admintoken}
        response = requests.get(url + '/api/v2/apprv/customer/?size=10&page=1&objectName=&updatedDate=&objectId=',
                                headers=headers, verify=False)
        # Code response
        print(response)
        # print(response.json()['object']['onboardId'])
        id_customer_approval = response.json()['content'][2]['id']
        print(id_customer_approval)
        print(response.text)

        # Body json
        # json_data = json.loads(response.text)
        # json_formatted_str = json.dumps(json_data, indent=2)
        # print(json_formatted_str)

        print("\n")

        assert response.status_code == 200

        @then(u'I Post Action Task Approval Delete Customer')
        def post_action_task_approval_onboarding_doc_check_maker_approve(context):

            payload = {

                "id": id_customer_approval,
                "remark": "",
                "taskAction": "approve"
            }

            headers = {"Authorization": "Bearer " + admintoken, "Content-Type": "application/json"}
            response = requests.post(url + '/api/v2/apprv/customer', data=json.dumps(payload),
                                     headers=headers, verify=False)
            print(response)
            print("\n")

            assert response.status_code == 200
