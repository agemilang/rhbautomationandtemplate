from behave import given, when, then


@given("I open login page IOS")
def login_with_existing_account_ios(context):
    context.app.launch_page_ios.login_with_existing_account_ios()

@when("I open login page")
def login_with_existing_account(context):
    context.app.launch_page.login_with_existing_account()




@then("I Click Skip IOS")
def click_skip(context):
    context.app.Sign_Out_IOS.click_skip()

@then("I Click Skip")
def click_skip(context):
    context.app.Sign_Out.click_skip()

@then("I Click Hamburger Menu IOS")
def click_hamburger_menu(context):
    context.app.Sign_Out_IOS.click_hamburger_menu()

@then("I Click Hamburger Menu")
def click_hamburger_menu(context):
    context.app.Sign_Out.click_hamburger_menu()

# Sign Out
@then("I Click Sign Out IOS")
def click_sign_out(context):
    context.app.Sign_Out_IOS.click_sign_out()

@then("I Click Sign Out")
def click_sign_out(context):
    context.app.Sign_Out.click_sign_out()


# PIN
@then("I Fill PIN After Sign Out IOS")
def fill_pin(context):
    context.app.Sign_Out_IOS.fill_pin()

@then("I Fill PIN After Sign Out")
def pin(context):
    context.app.Sign_Out.pin()

# MAYBE LATER
@then("I Click Maybe Later IOS")
def click_maybe_later(context):
    context.app.Sign_Out_IOS.click_maybe_later()

@then("I Click Maybe Later")
def click_maybe_later(context):
    context.app.Sign_Out.click_maybe_later()