from behave import given, when, then
import requests
import json
from credentials import  email, email1, email2, email3, email4, email5, email6, email7, url


@given("I open exist register page IOS")
def create_exist_account_ios(context):
    context.app.launch_page_ios.create_exist_account_ios()


@then("I select register sms otp IOS")
def select_register_smsotp_ios(context):
    context.app.ETBSMS_IOS.select_register_smsotp_ios()

@then("I fill identity IOS")
def fill_identity_ios(context):
    context.app.ETBSMS_IOS.fill_identity_ios()

@then("I fill create credential IOS")
def fill_create_credential_ios(context):
    context.app.ETBSMS_IOS.fill_create_credential_ios()

@then("I Check Term & Condition OTP IOS")
def check_term_condition_ios_otp(context):
    context.app.ETBSMS_IOS.check_term_condition_ios_otp()

@then("I skip soft token IOS")
def skip_soft_token_ios(context):
    context.app.ETBSMS_IOS.skip_soft_token_ios()