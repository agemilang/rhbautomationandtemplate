from behave import given, when, then
import os
from appium import webdriver
import time
from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode



@given("I open register page IOS")
def create_new_account_ios(context):
    context.app.launch_page_ios.create_new_account_ios()

@when("I Allow RHB Mobile to access this device's location? IOS")
def button_allow_ios(context):
    context.app.RegisterMyKad_IOS.button_allow_ios()

@then("I Fill Registration IOS 1")
def form_registration(context):
    context.app.RegisterMyKad_IOS.fill_registration_ios1()

@then("I Fill Registration IOS 2")
def form_registration(context):
    context.app.RegisterMyKad_IOS.fill_registration_ios2()

@then("I Fill Registration IOS 3")
def form_registration(context):
    context.app.RegisterMyKad_IOS.fill_registration_ios3()

@then("I Fill Registration IOS 4")
def form_registration(context):
    context.app.RegisterMyKad_IOS.fill_registration_ios4()






@then("I Fill OTP IOS")
def fill_otp(context):
    context.app.RegisterMyKad_IOS.fill_otp_ios()

@then("I Fill Password IOS")
def fill_password(context):
    context.app.RegisterMyKad_IOS.fill_password_ios()

@then("I Choose MyKad IOS")
def choose_mykad(context):
    context.app.RegisterMyKad_IOS.choose_mykad_ios()

@then("I Take Photo MyKad IOS")
def take_photo_mykad(context):
    context.app.RegisterMyKad_IOS.take_photo_mykad_ios()



# Kemungkinan Pengisian Personal Information
#1
@then("I Fill Personal Information Working at Singapore Yes Residential Yes Mailing Yes IOS")
def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes_ios()
# 2
@then("I Fill Personal Information Working at Singapore Yes Residential Yes Mailing No IOS")
def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no_ios()
# 3
@then("I Fill Personal Information Working at Singapore Yes Residential No Mailing Yes IOS")
def fill_personal_information_working_in_singapore_yes_residential_no_mailing_yes_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_yes_residential_no_mailing_yes_ios()
# 4
@then("I Fill Personal Information Working at Singapore Yes Residential No Mailing No IOS")
def fill_personal_information_working_in_singapore_yes_residential_no_mailing_no_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_yes_residential_no_mailing_no_ios()
# 5
@then("I Fill Personal Information Working at Singapore No Residential Yes Mailing Yes IOS")
def fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes_ios()
# 6
@then("I Fill Personal Information Working at Singapore No Residential Yes Mailing No IOS")
def fill_personal_information_working_in_singapore_no_residential_yes_mailing_no_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_no_residential_yes_mailing_no_ios()
# 7
@then("I Fill Personal Information Working at Singapore No Residential No Mailing Yes IOS")
def fill_personal_information_working_in_singapore_no_residential_no_mailing_yes_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_no_residential_no_mailing_yes_ios()
# 8
@then("I Fill Personal Information Working at Singapore No Residential No Mailing No IOS")
def fill_personal_information_working_in_singapore_no_residential_no_mailing_no_ios(context):
    context.app.RegisterMyKad_IOS.fill_personal_information_working_in_singapore_no_residential_no_mailing_no_ios()

#
# @then("I Fill Personal Information Form IOS")
# def fill_personal_information(context):
#     context.app.RegisterMyKad_IOS.fill_personal_information_ios()



@then("I Fill Mailing Address IOS")
def fill_personal_information(context):
    context.app.RegisterMyKad_IOS.fill_mailing_address_ios()

@then("I Take Picture Proof of Residential IOS")
def take_picture_your_proof_residence_ios(context):
    context.app.RegisterMyKad_IOS.take_picture_your_proof_residence_ios()

@then("I Take Picture Employment Pass IOS")
def take_picture_your_employment_pass(context):
    context.app.RegisterMyKad_IOS.take_picture_your_employment_pass_ios()

@then("I Select Product HYSA IOS")
def select_product_application_hysa(context):
    context.app.RegisterMyKad_IOS.select_product_application_hysa_ios()

@then("I Select Product TRIO IOS")
def select_product_application_trio_ios(context):
    context.app.RegisterMyKad_IOS.select_product_application_trio_ios()

@then("I Select Product PP IOS")
def select_product_application_hysa(context):
    context.app.RegisterMyKad_IOS.select_product_application_hysa_ios()




@then("I Fill Additional Information IOS")
def fill_additional_information_ios(context):
    context.app.RegisterMyKad_IOS.fill_additional_information_ios()

@then("I Fill Account Information IOS")
def fill_account_information(context):
    context.app.RegisterMyKad_IOS.fill_account_information_ios()

@then("I Check Term & Condition IOS")
def check_term_condition(context):
    context.app.RegisterMyKad_IOS.check_term_condition_ios()

