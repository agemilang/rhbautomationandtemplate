from behave import given, when, then


@then("I Choose Foreigner IOS")
def choose_foreigner_ios(context):
    context.app.RegisterForeigner_IOS.choose_foreigner_ios()

@then("I Click Next Button Take Picture Passport IOS")
def allow_camera_take_picture_passport_ios(context):
    context.app.RegisterForeigner_IOS.allow_camera_take_picture_passport_ios()

@then("I Fill Personal Information Working at Singapore Yes Mailing Yes IOS")
def fill_personal_information_working_in_singapore_yes_mailing_yes_ios(context):
    context.app.RegisterForeigner_IOS.fill_personal_information_working_in_singapore_yes_mailing_yes_ios()

@then("I Fill Personal Information Working at Singapore Yes Mailing No IOS")
def fill_personal_information_working_in_singapore_yes_mailing_no_ios(context):
    context.app.RegisterForeigner_IOS.fill_personal_information_working_in_singapore_yes_mailing_no_ios()

@then("I Fill Personal Information Working at Singapore No Mailing Yes IOS")
def fill_personal_information_working_in_singapore_no_mailing_yes_ios(context):
    context.app.RegisterForeigner_IOS.fill_personal_information_working_in_singapore_no_mailing_yes_ios()

@then("I Fill Personal Information Working at Singapore No Mailing No IOS")
def fill_personal_information_working_in_singapore_no_mailing_no_ios(context):
    context.app.RegisterForeigner_IOS.fill_personal_information_working_in_singapore_no_mailing_no_ios()


@then("I Take Photo Proof of Residence IOS")
def take_picture_proof_of_residential_address_ios(context):
    context.app.RegisterForeigner_IOS.take_picture_proof_of_residential_address_ios()

@then("I Take Photo Employment Pass IOS")
def take_picture_employment_pass_ios(context):
    context.app.RegisterForeigner_IOS.take_picture_employment_pass_ios()
