from behave import given, when, then


@then("I Choose MyInfo IOS")
def choose_myinfo_ios(context):
    context.app.RegisterMyInfo_IOS.choose_myinfo_ios()

@then("I Click Login MyInfo IOS")
def i_click_login_myinfo(context):
    context.app.RegisterMyInfo_IOS.i_click_login_myinfo()

