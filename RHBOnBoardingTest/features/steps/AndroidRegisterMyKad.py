from behave import given, when, then
import requests
import json
from credentials import  email, email1, email2, email3, email4, email5, email6, email7, url


@given("I allow phone call permission")
def allow_permission_phonecalls(context):
    context.app.launch_page.allow_permission_phonecalls()

@when("I open register page")
def create_new_account(context):
    context.app.launch_page.create_new_account()


@then("I Allow RHB Mobile to access this device's location?")
def button_allow(context):
    context.app.RegisterMyKad.button_allow()

@then("I Check Error Message")
def button_allow(context):
    context.app.RegisterMyKad.check_error_message()



@then("I register")
def fill_field(context):
    context.app.RegisterMyKad.fill_field()

@then("I register 1")
def fill_field1(context):
    context.app.RegisterMyKad.fill_field1()

@then("I register 2")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field2()

@then("I register 3")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field3()

@then("I register 4")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field4()

@then("I register 5")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field5()

@then("I register 6")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field6()

@then("I register 7")
def fill_field2(context):
    context.app.RegisterMyKad.fill_field7()






@then("I Click Next Button Email")
def button_next(context):
    context.app.RegisterMyKad.button_next()

@then("I Fill OTP Number")
def fill_otp1(context):
    context.app.RegisterMyKad.fill_otp1()

@then("I Click Next Button OTP 1")
def button_next(context):
    context.app.RegisterMyKad.button_next()

@then("I Fill Password")
def fill_password(context):
    context.app.RegisterMyKad.fill_password()

@then("I Click Next Button Password")
def button_next(context):
    context.app.RegisterMyKad.button_next()

@then("I Choose MyKad")
def choose_mykad(context):
    context.app.RegisterMyKad.choose_mykad()

@then("I Click Next Button Take Picture")
def allow_camera_take_picture(context):
    context.app.RegisterMyKad.allow_camera_take_picture()


# Kemungkinan Pengisian Personal Information
# 1
@then("I Fill Personal Information Working at Singapore Yes Residential Yes Mailing Yes")
def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes()
# 2
@then("I Fill Personal Information Working at Singapore Yes Residential Yes Mailing No")
def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no()
# 3
@then("I Fill Personal Information Working at Singapore Yes Residential No Mailing Yes")
def fill_personal_information_working_in_singapore_yes_residential_no_mailing_no(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_yes_residential_no_mailing_yes()
# 4
@then("I Fill Personal Information Working at Singapore Yes Residential No Mailing No")
def fill_personal_information_working_in_singapore_yes_residential_no_mailing_no(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_yes_residential_no_mailing_no()
# 5
@then("I Fill Personal Information Working at Singapore No Residential Yes Mailing Yes")
def fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes()
# 6
@then("I Fill Personal Information Working at Singapore No Residential Yes Mailing No")
def fill_personal_information_working_in_singapore_no_residential_yes_mailing_no(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_no_residential_yes_mailing_no()
# 7
@then("I Fill Personal Information Working at Singapore No Residential No Mailing Yes")
def fill_personal_information_working_in_singapore_no_residential_no_mailing_yes(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_no_residential_no_mailing_yes()
# 8
@then("I Fill Personal Information Working at Singapore No Residential No Mailing No")
def fill_personal_information_working_in_singapore_no_residential_no_mailing_no(context):
    context.app.RegisterMyKad.fill_personal_information_working_in_singapore_no_residential_no_mailing_no()



@then("I Fill Mailing Address")
def fill_mailing_address(context):
    context.app.RegisterMyKad.fill_mailing_address()

@then("I Take Photo Signature")
def take_signature(context):
    context.app.RegisterMyKad.take_signature()

@then("I Take Photo Proof of Residence")
def take_proof_of_residence(context):
    context.app.RegisterMyKad.take_proof_of_residence()


@then("I Take Photo Employment Pass")
def take_employment_pass(context):
    context.app.RegisterMyKad.take_employment_pass()



@then("I Choose Product HYSP")
def choose_HYSP(context):
    context.app.RegisterMyKad.choose_HYSP()

@then("I Choose Product TRIO")
def choose_TRIO(context):
    context.app.RegisterMyKad.choose_TRIO()

@then("I Choose Product PREMIER PLUS")
def choose_PREMIERPLUS(context):
    context.app.RegisterMyKad.choose_PREMIERPLUS()

@then("I Choose Product HYSP & TRIO")
def choose_HYSP_TRIO(context):
    context.app.RegisterMyKad.choose_HYSP_TRIO()

@then("I Choose Product TRIO & PREMIER PLUS")
def choose_TRIO_PREMIERPLUS(context):
    context.app.RegisterMyKad.choose_TRIO_PREMIERPLUS()

@then("I Choose Product HYSP & PREMIER PLUS")
def choose_HYSP_PREMIERPLUS(context):
    context.app.RegisterMyKad.choose_HYSP_PREMIERPLUS()

@then("I Choose 3 Product Application")
def choose_3_product_application(context):
    context.app.RegisterMyKad.choose_3_product_application()



@then("I Fill Additional Information")
def fill_additional_information_employed(context):
    context.app.RegisterMyKad.fill_additional_information_employed()

@then("I Fill Account Information 1")
def fill_account_information_1(context):
    context.app.RegisterMyKad.fill_account_information_1()

@then("I Fill Account Information 2")
def fill_account_information_2(context):
    context.app.RegisterMyKad.fill_account_information_2()

@then("I Fill Account Information 3")
def fill_account_information_3(context):
    context.app.RegisterMyKad.fill_account_information_3()





@then("I Agree with Term & Condition")
def agree_term_condition(context):
    context.app.RegisterMyKad.agree_term_condition()

@then("I Fill Email & Password")
def fill_login(context):
    context.app.RegisterMyKad.fill_login()

@then("I Fill Email & Password 1")
def fill_login1(context):
    context.app.RegisterMyKad.fill_login1()

@then("I Fill Email & Password 2")
def fill_login2(context):
    context.app.RegisterMyKad.fill_login2()

@then("I Fill Email & Password 3")
def fill_login3(context):
    context.app.RegisterMyKad.fill_login3()

@then("I Fill Email & Password 4")
def fill_login4(context):
    context.app.RegisterMyKad.fill_login4()

@then("I Fill Email & Password 5")
def fill_login5(context):
    context.app.RegisterMyKad.fill_login5()

@then("I Fill Email & Password 6")
def fill_login6(context):
    context.app.RegisterMyKad.fill_login6()

@then("I Fill Email & Password 7")
def fill_login7(context):
    context.app.RegisterMyKad.fill_login7()

@then("I Fill Email & Password 8")
def fill_login7(context):
    context.app.RegisterMyKad.fill_login8()

@then("I Fill Email & Password 9")
def fill_login7(context):
    context.app.RegisterMyKad.fill_login9()

@then("I Fill Create PIN")
def create_pin(context):
    context.app.RegisterMyKad.create_pin()

@then("I Click MB Banking")
def create_pin(context):
    context.app.RegisterMyKad.click_Mobile_Banking()

@then("I Check Message Error On Progress")
def create_pin(context):
    context.app.RegisterMyKad.check_message_error_on_progress()











