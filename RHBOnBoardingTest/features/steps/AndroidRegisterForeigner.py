from behave import given, when, then


@then("I Choose Foreigner")
def choose_foreigner(context):
    context.app.RegisterForeigner.choose_foreigner()

@then("I Click Next Button Take Picture Passport")
def allow_camera_take_picture_passport(context):
    context.app.RegisterForeigner.allow_camera_take_picture_passport()

@then("I Fill Personal Information Working at Singapore Yes Mailing Yes")
def fill_personal_information_working_in_singapore_yes_mailing_yes(context):
    context.app.RegisterForeigner.fill_personal_information_working_in_singapore_yes_mailing_yes()

@then("I Fill Personal Information Working at Singapore Yes Mailing No")
def fill_personal_information_working_in_singapore_yes_mailing_no(context):
    context.app.RegisterForeigner.fill_personal_information_working_in_singapore_yes_mailing_no()

@then("I Fill Personal Information Working at Singapore No Mailing Yes")
def fill_personal_information_working_in_singapore_no_mailing_yes(context):
    context.app.RegisterForeigner.fill_personal_information_working_in_singapore_no_mailing_yes()

@then("I Fill Personal Information Working at Singapore No Mailing No")
def fill_personal_information_working_in_singapore_no_mailing_no(context):
    context.app.RegisterForeigner.fill_personal_information_working_in_singapore_no_mailing_no()