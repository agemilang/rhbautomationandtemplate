from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class Sign_Out_IOS(Page):

    SKIP = (By.XPATH, '//XCUIElementTypeButton[@name="SKIP"]')


    SIGN_OUT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[16]/XCUIElementTypeOther/XCUIElementTypeButton')
    # SIGN_OUT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[9]/XCUIElementTypeOther')

    # SIGN_OUT = (By.ID, 'Sign Out')
    CANCEL = (By.ID, 'android:id/button2')
    CONFIRM_SIGN_OUT = (By.ID, 'android:id/button1')

    B1 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[1]')
    B2 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[2]')
    B3 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[3]')
    B4 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[4]')
    B5 = (By.XPATH, '(//XCUIElementTypeImage[@name="star-grey"])[5]')

    RT = (By.ID, 'Response Time')
    N = (By.ID, 'Navigation')
    AF = (By.ID, 'App Features')
    EU = (By.ID, 'Ease of Use')
    LF = (By.ID, 'Look & Feel')
    NS = (By.ID, 'No. of Steps')

    TB = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView')

    SUBMIT = (By.XPATH, '//XCUIElementTypeButton[@name="Submit"]')
    HAMBURGER_MENU = (By.ID, 'icMenu')

    FORGOTPIN = (By.XPATH, '//XCUIElementTypeButton[@name="Forgot PIN"]')

    # PIN 2
    NO_1 = (By.XPATH,
            '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton')
    NO_2 = (By.XPATH,
            '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton')
    NO_3 = (By.XPATH,
            '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton')
    NO_4 = (By.ID, 'four_icon')
    NO_5 = (By.ID, 'five_icon')
    NO_6 = (By.ID, 'six_icon')
    NO_7 = (By.ID, 'seven_icon')
    NO_8 = (By.ID, 'eight_icon')
    NO_9 = (By.ID, 'nine_icon')
    NO_0 = (By.ID, 'zero_icon')
    BACKSPACE = (By.ID, 'backspace_ic')

    ERROR_PIN_FALSE = (By.ID, "'Oops! Your PIN doesn't seem right. Try again.'")

    Forgot_PIN = (By.ID, "Forgot PIN")

    MAYBE_LATER = (By.XPATH, '//XCUIElementTypeButton[@name="MAYBE LATER"]')
    MAYBE_LATER2 = (By.ID, "MAYBE LATER")

    def click_skip(self):
        time.sleep(5)
        self.click_on_element(self.SKIP)

        # time.sleep(5)

    def click_hamburger_menu(self):
        time.sleep(5)
        self.click_on_element(self.HAMBURGER_MENU)

        time.sleep(5)

    def click_sign_out(self):
        time.sleep(5)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=45).release().perform()
        self.click_on_element(self.SIGN_OUT)
        # self.click_on_element(self.CANCEL)
        # self.click_on_element(self.SIGN_OUT)
        # self.click_on_element(self.CONFIRM_SIGN_OUT)
        time.sleep(5)
        self.click_on_element(self.B5)
        time.sleep(5)
        self.click_on_element(self.SUBMIT)

    def fill_pin(self):
        time.sleep(10)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)

        time.sleep(5)

    def click_maybe_later(self):
        time.sleep(5)
        self.click_on_element(self.MAYBE_LATER)