from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time

class Sign_Out(Page):


    SIGN_OUT = (By.XPATH,
                '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout')
    CANCEL = (By.ID, 'android:id/button2')
    CONFIRM_SIGN_OUT = (By.ID, 'android:id/button1')
    # PIN
    NO_1 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btOne')
    NO_2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btTwo')
    NO_3 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btThree')
    NO_4 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFour')
    NO_5 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btFive')
    NO_6 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSix')
    NO_7 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSeven')
    NO_8 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btEight')
    NO_9 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNine')
    NO_0 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btZero')
    BACKSPACE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btBackscape')

    ERROR_PIN_FALSE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvEnterPin')

    # Skip Activate Soft Token
    SKIP_ACTIVATION = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btSkipSoftTokenActivation")

    HAMBURGER_MENU = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btHamburgerMenu')

    B1 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibStar1')
    B2 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibStar2')
    B3 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibStar3')
    B4 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibStar4')
    B5 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/ibStar5')

    RT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btResponseTime')
    N = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNavigation')
    AF = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btMoreFeature')
    EU = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btEaseOfUse')
    LF = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btLookAndFeel')
    NS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNoOfSteps')

    TB = (By.ID,
          'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')

    SUBMIT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btSubmit')

    Forgot_PIN = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btForgotPin")

    MAYBE_LATER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btCancel')


    def pin(self):
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)
        self.click_on_element(self.NO_3)
        self.click_on_element(self.NO_2)
        self.click_on_element(self.NO_1)

        time.sleep(5)

    def click_skip(self):
        self.click_on_element(self.SKIP_ACTIVATION)

    def click_hamburger_menu(self):
        self.click_on_element(self.HAMBURGER_MENU)

    def click_sign_out(self):
        self.driver.swipe(15, 1844, 15, 560)
        self.click_on_element(self.SIGN_OUT)
        self.click_on_element(self.CANCEL)
        self.click_on_element(self.SIGN_OUT)
        self.click_on_element(self.CONFIRM_SIGN_OUT)
        self.click_on_element(self.B4)
        self.click_on_element(self.SUBMIT)

        time.sleep(15)

    def click_maybe_later(self):
        time.sleep(5)
        self.click_on_element(self.MAYBE_LATER)
