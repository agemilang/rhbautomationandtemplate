from credentials import TIN, fullnamec1, fullnamec2, fullnamec3, fullnamec4, phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class RegisterMyKad(Page):
    print('Email = ' + email)
    print('Email 1= ' + email1)
    print('Email 2= ' + email2)
    print('Email 3= ' + email3)
    print('Email 4= ' + email4)
    print('Email 5= ' + email5)
    print('Email 6= ' + email6)
    print('Email 7= ' + email7)

    BUTTON_ALLOW_ACCESS_LOCATION = (By.ID, "com.android.permissioncontroller:id/permission_allow_foreground_only_button")
    PHONE_NUMBER_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    EMAIL_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    CONFIRM_EMAIL_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    # ERROR_MESSAGE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/tvErrorEditTextBox")
    ERROR_MESSAGE = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView")



    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")

    OTP1 = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.EditText")

    PASSWORD = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    SEE_PASSWORD = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    RE_PASSWORD = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    SEE_RE_PASSWORD = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')

    MYKAD = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llMalaysian")

    BUTTON_ALLOW_CAMERA = (By.ID, "com.android.permissioncontroller:id/permission_allow_button")
    TAKE_MYCARD_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    ACCEPT_MYCARD_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    TAKE_MYCARD_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    ACCEPT_MYCARD_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    TAKE_SELFIE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btCamera")

    # Personal Information
    ID_NUMBER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    COMBOBOX_COUNTRYBIRTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    COMBOBOX_NATIONALITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    COMBOBOX_GENDER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    FULLNAME = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    BIRTHDATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    COMBOBOX_MARTIALSTATUS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    OPTION_BUTTON_YES = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbWorkingInSingaporeYes')
    FIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    RESIDENTIAL_COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout')
    RESIDENTIAL_STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout')
    RESIDENTIAL_CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_APARTMENT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    RESIDENTIAL_POSTALCODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_OPTION_BUTTON_YES = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # Is your residential address the same as the address shown on your MyKad?
    RESIDENTIAL_ADDRESS_SAME_ON_MYKAD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # My mailing address is the same as my residential address
    MY_MAILING_SAME = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbSameAsResidential')


    # Page Mailing address
    MAILING_COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    MAILING_ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    MAILING_APARTMENT = (By.XPATH, 'hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_POSTALCODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Proof of Residence
    SIGNATURE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/ivSignature")
    SIGNATURE_TAKE_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    SIGNATURE_ACCEPT_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")

    # Take a picture of your employment pass
    EMPLOYMENT_TAKE_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    EMPLOYMENT_ACCEPT_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    EMPLOYMENT_TAKE_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    EMPLOYMENT_ACCEPT_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")

    # Product Application
    PRODUCT_HIGH_YIELD = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbHysa")
    PRODUCT_TRIO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbTrio")
    PRODUCT_PREMIER_PLUS = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbPremierPlus")

    # Additional Information
    # Employed
    EMPLOYMENT_STATUS = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/tvValue")
    OCCUPATION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    EMPLOYER_NAME = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText")
    EMPLOYER_BUSINESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    INCOME_ANNUM = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    SOURCE_FUND = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout')
    CHECKLIST1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.CheckBox')
    CHECKLIST2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.CheckBox')
    SOURCE_WEALTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout')
    DONE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/action_next")

    # Account Information
    PURPOSE_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')

    # Are you a resident, green card holder, or citizen of the United States of America or any of its territories?
    CITIZEN_US = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/rbAmericaResidentYes")

    US_TIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Do you have a Power of Attorney granted to person with U.S. Address?
    POWER_US = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/rbPowerOfAttorneyYes")

    COUNTRY_JURISDICTION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    TIN_NUMBER = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText')

    # Terms & Conditionsq
    # pilihan1
    I_UNDERSTOOD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbTermCondition')

    # pilihan2
    I_AGREE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotional')
    VOICE_CALL = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalVoiceCall')
    SMS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalSms')



    # Sign In
    SignIn = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btApply')
    EMAIL_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PASSWORD_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')


    # CREATE PIN LOGIN
    CREATE_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CREATE_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    CONFIRM_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CONFIRM_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')


    # MB Button
    MB_BUTTON = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')
    MESSAGE_ERROR_ON_PROGRESS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvCode')





    # ERORR MESSAGE
    def check_error_message(self):
        error = self.find_elements(self.ERROR_MESSAGE)
        compare_data_with_expected(expected="Please fill up this field", real=error[0].text)


    def button_allow(self):
        self.click_on_element(self.BUTTON_ALLOW_ACCESS_LOCATION)

    def fill_field(self):
        self.input(phone, self.PHONE_NUMBER_TEXT)
        self.input(email, self.EMAIL_TEXT)
        self.input(email, self.CONFIRM_EMAIL_TEXT)

    def fill_field1(self):
        self.input(phone1, self.PHONE_NUMBER_TEXT)
        self.input(email1, self.EMAIL_TEXT)
        self.input(email1, self.CONFIRM_EMAIL_TEXT)

    def fill_field2(self):
        self.input(phone2, self.PHONE_NUMBER_TEXT)
        self.input(email2, self.EMAIL_TEXT)
        self.input(email2, self.CONFIRM_EMAIL_TEXT)

    def fill_field3(self):
        self.input(phone3, self.PHONE_NUMBER_TEXT)
        self.input(email3, self.EMAIL_TEXT)
        self.input(email3, self.CONFIRM_EMAIL_TEXT)

    def fill_field4(self):
        self.input(phone4, self.PHONE_NUMBER_TEXT)
        self.input(email4, self.EMAIL_TEXT)
        self.input(email4, self.CONFIRM_EMAIL_TEXT)

    def fill_field5(self):
        self.input(phone5, self.PHONE_NUMBER_TEXT)
        self.input(email5, self.EMAIL_TEXT)
        self.input(email5, self.CONFIRM_EMAIL_TEXT)

    def fill_field6(self):
        self.input(phone6, self.PHONE_NUMBER_TEXT)
        self.input(email6, self.EMAIL_TEXT)
        self.input(email6, self.CONFIRM_EMAIL_TEXT)

    def fill_field7(self):
        self.input(phone7, self.PHONE_NUMBER_TEXT)
        self.input(email7, self.EMAIL_TEXT)
        self.input(email7, self.CONFIRM_EMAIL_TEXT)


    def button_next(self):
        self.click_on_element(self.BUTTON_NEXT)
        # time.sleep(15)
    def fill_otp1(self):
        self.input(OTP1, self.OTP1)

    def fill_password(self):
        self.input("Password2", self.PASSWORD)
        self.click_on_element(self.SEE_PASSWORD)
        self.input("Password2", self.RE_PASSWORD)
        self.click_on_element(self.SEE_RE_PASSWORD)

    def choose_mykad(self):
        self.click_on_element(self.MYKAD)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    def allow_camera_take_picture(self):
        self.click_on_element(self.BUTTON_ALLOW_CAMERA)
        self.click_on_element(self.TAKE_MYCARD_FRONT)
        self.click_on_element(self.ACCEPT_MYCARD_FRONT)
        self.click_on_element(self.TAKE_MYCARD_BACK)
        self.click_on_element(self.ACCEPT_MYCARD_BACK)
        self.click_on_element(self.TAKE_SELFIE)
        time.sleep(5)

        # for i in range(2):
        #     touch = TouchAction(self.driver)
        #     touch.press(x=34, y=2059).move_to(x=42, y=420)   .release()   .perform()



    #  Kemungkinan Pengisian Personal Information sudah ada option isian yang berbeda
    # 1
    def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(fullnamec1, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        time.sleep(5)

        self.click_on_element(self.OPTION_BUTTON_YES)

        self.driver.swipe(582, 1385, 608, 544)
        time.sleep(5)

        self.input(fin, self.FIN)
        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        # self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.RESIDENTIAL_ADDRESS_SAME_ON_MYKAD)
        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.BUTTON_NEXT)

        time.sleep(5)
    # 2
    def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no(self):
        self.input(phone1, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)

        self.click_on_element(self.OPTION_BUTTON_YES)
        self.input(fin, self.FIN)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.RESIDENTIAL_ADDRESS_SAME_ON_MYKAD)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 3
    def fill_personal_information_working_in_singapore_yes_residential_no_mailing_yes(self):
        self.input(phone2, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)

        self.click_on_element(self.OPTION_BUTTON_YES)
        self.input(fin, self.FIN)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 4
    def fill_personal_information_working_in_singapore_yes_residential_no_mailing_no(self):
        self.input(phone3, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)

        self.click_on_element(self.OPTION_BUTTON_YES)
        self.input(fin, self.FIN)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 5
    def fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes(self):
        self.input(phone4, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)


        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.RESIDENTIAL_ADDRESS_SAME_ON_MYKAD)
        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 6
    def fill_personal_information_working_in_singapore_no_residential_yes_mailing_no(self):
        self.input(phone5, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.RESIDENTIAL_ADDRESS_SAME_ON_MYKAD)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 7
    def fill_personal_information_working_in_singapore_no_residential_no_mailing_yes(self):
        self.input(phone6, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)


        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)

    # 8
    def fill_personal_information_working_in_singapore_no_residential_no_mailing_no(self):
        self.input(phone7, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.COMBOBOX_GENDER)
        self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.FULLNAME)
        self.input("23/03/2000", self.BIRTHDATE)
        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)


        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.RESIDENTIAL_CITY)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.swipe(582, 1385, 608, 544)
        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(5)





    def fill_mailing_address(self):
        self.click_on_element(self.MAILING_COUNTRY)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.MAILING_STATE)
        self.input("Singapore", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(city, self.MAILING_CITY)
        self.input(address, self.MAILING_ADDRESS)
        # self.input(, self.MAILING__APARTMENT)
        self.input(postalcode, self.MAILING_POSTALCODE)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def take_signature(self):
        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_FRONT)
        self.click_on_element(self.SIGNATURE_ACCEPT_FRONT)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def take_proof_of_residence(self):
        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_FRONT)
        self.click_on_element(self.SIGNATURE_ACCEPT_FRONT)
        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(10)

    def take_employment_pass(self):
        self.click_on_element(self.BUTTON_NEXT)
        self.click_on_element(self.EMPLOYMENT_TAKE_FRONT)
        self.click_on_element(self.EMPLOYMENT_ACCEPT_FRONT)
        self.click_on_element(self.EMPLOYMENT_TAKE_BACK)
        self.click_on_element(self.EMPLOYMENT_ACCEPT_BACK)
        time.sleep(10)




    def choose_HYSP(self):
        self.click_on_element(self.PRODUCT_HIGH_YIELD)
        self.click_on_element(self.BUTTON_NEXT)

    def choose_TRIO(self):
        self.click_on_element(self.PRODUCT_TRIO)
        self.click_on_element(self.BUTTON_NEXT)

    def choose_PREMIERPLUS(self):
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.PRODUCT_PREMIER_PLUS)
        self.click_on_element(self.BUTTON_NEXT)

    def choose_HYSP_TRIO(self):
        self.click_on_element(self.PRODUCT_HIGH_YIELD)
        self.click_on_element(self.PRODUCT_TRIO)


    def choose_TRIO_PREMIERPLUS(self):
        self.click_on_element(self.PRODUCT_TRIO)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.PRODUCT_PREMIER_PLUS)
        self.click_on_element(self.BUTTON_NEXT)

    def choose_HYSP_PREMIERPLUS(self):
        self.click_on_element(self.PRODUCT_HIGH_YIELD)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.PRODUCT_PREMIER_PLUS)
        self.click_on_element(self.BUTTON_NEXT)

    def choose_3_product_application(self):
        self.click_on_element(self.PRODUCT_HIGH_YIELD)
        self.click_on_element(self.PRODUCT_TRIO)
        time.sleep(10)

        self.driver.swipe(38, 1086, 45, 366)
        self.click_on_element(self.PRODUCT_PREMIER_PLUS)
        self.click_on_element(self.BUTTON_NEXT)



    def fill_additional_information_employed(self):
        self.click_on_element(self.EMPLOYMENT_STATUS)
        self.input("Employed", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.OCCUPATION)
        self.input("Actor", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input(female, self.EMPLOYER_NAME)
        self.click_on_element(self.EMPLOYER_BUSINESS)
        self.input("Beer", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.INCOME_ANNUM)
        self.input("$90", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.SOURCE_FUND)
        self.input("Business", self.SEARCH)
        self.click_on_element(self.CHECKLIST1)
        self.click_on_element(self.CHECKLIST2)
        self.click_on_element(self.DONE)
        self.click_on_element(self.SOURCE_WEALTH)
        self.input("Business", self.SEARCH)
        self.click_on_element(self.CHECKLIST1)
        self.click_on_element(self.DONE)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_account_information_1(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Saving", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)

        self.click_on_element(self.CITIZEN_US)
        self.input(TIN, self.US_TIN)


        self.click_on_element(self.COUNTRY_JURISDICTION)
        time.sleep(5)

        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)
        time.sleep(30)

        self.input(phone, self.TIN_NUMBER)

        # ADD ANOTHER COUNTRY

        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_FRONT)
        self.click_on_element(self.SIGNATURE_ACCEPT_FRONT)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.BUTTON_NEXT)


    def fill_account_information_2(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Saving", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.click_on_element(self.POWER_US)
        self.click_on_element(self.COUNTRY_JURISDICTION)
        time.sleep(5)

        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)
        time.sleep(15)

        self.input(phone, self.TIN_NUMBER)

        # ADD ANOTHER COUNTRY



        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_FRONT)
        self.click_on_element(self.SIGNATURE_ACCEPT_FRONT)
        self.driver.swipe(582, 1385, 608, 544)
        self.click_on_element(self.BUTTON_NEXT)


    def fill_account_information_3(self):
        self.click_on_element(self.PURPOSE_ACCOUNT)
        self.input("Saving", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)

        self.click_on_element(self.COUNTRY_JURISDICTION)
        time.sleep(5)

        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.driver.swipe(582, 1385, 608, 544)
        time.sleep(15)

        self.input(phone, self.TIN_NUMBER)

        # ADD ANOTHER COUNTRY



        self.click_on_element(self.SIGNATURE)
        self.click_on_element(self.SIGNATURE_TAKE_FRONT)
        self.click_on_element(self.SIGNATURE_ACCEPT_FRONT)
        self.click_on_element(self.BUTTON_NEXT)











    def agree_term_condition(self):
        self.click_on_element(self.I_UNDERSTOOD)
        self.click_on_element(self.I_AGREE)
        self.click_on_element(self.VOICE_CALL)
        self.click_on_element(self.SMS)


        self.click_on_element(self.BUTTON_NEXT)
        time.sleep(35)







    # def allow_permission_phonecalls_login(self):
    #     self.click_on_element(self.BUTTON_ALLOW_ACCESS_LOCATION)


    def fill_login(self):
        self.click_on_element(self.SignIn)
        self.input(email, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login1(self):
        self.click_on_element(self.SignIn)
        self.input(email1, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login2(self):
        self.click_on_element(self.SignIn)
        self.input(email2, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login3(self):
        self.click_on_element(self.SignIn)
        self.input(email3, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login4(self):
        self.click_on_element(self.SignIn)
        self.input(email4, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login5(self):
        self.click_on_element(self.SignIn)
        self.input(email5, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login6(self):
        self.click_on_element(self.SignIn)
        self.input(email6, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login7(self):
        self.click_on_element(self.SignIn)
        self.input(email7, self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)
        
    def create_pin(self):
        self.input("321321", self.CREATE_PIN)
        self.click_on_element(self.SEE_CREATE_PIN)
        self.input("321321", self.CONFIRM_PIN)
        self.click_on_element(self.SEE_CONFIRM_PIN)
        self.click_on_element(self.BUTTON_NEXT)

    def fill_login_existing(self):
        # self.click_on_element(self.SignIn)
        self.input('isyak12@yopmail.com', self.EMAIL_LOGIN)
        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.BUTTON_NEXT)


    def click_Mobile_Banking(self):
        self.click_on_element(self.MB_BUTTON)

    def check_message_error_on_progress(self):
        error_message_account_rejected = self.find_elements(self.MESSAGE_ERROR_ON_PROGRESS)
        compare_data_with_expected(
            expected="Oops!\nYou have a pending application. Please try again once it has been completed.",
            real=error_message_account_rejected[0].text)


        # text = self.find_element(self.CHECK_TEXT)
        # print(len(text))
        #
        # expected_text = ["Oops! You have a pending application. Please try again once it has been completed."]
        # actual_text = []
        #
        # for value in text:
        #     ele_name = value.get_attribute("text")
        #     print(ele_name)
        #     actual_text.append(ele_name)
        #
        # print(actual_text)
        #
        # assert expected_text == actual_text


        time.sleep(5)



