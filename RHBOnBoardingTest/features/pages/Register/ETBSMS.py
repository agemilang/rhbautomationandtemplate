from credentials import TIN, fullnamec1, fullnamec2, fullnamec3, fullnamec4, phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode

from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class ETBSMS(Page):
    # Sign In
    REGISTER_SMS_OTP = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llRegisterViaSMS')
    IDENTITY_TYPE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llDropdown')
    SEARCH = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/etQuery')
    CHOOSE_ROW1 = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/tvName')

    IDENTITY_NUMBER = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    DOB = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    NEXT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btNext')


    EMAIL = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PASSWORD = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_PASSWORD = (By.XPATH,'(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    CONFIRM_PASSWORD = (By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CONFIRM_PASSWORD = (By.XPATH,'(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')

    def select_register_smsotp(self):
        self.click_on_element(self.REGISTER_SMS_OTP)

    def fill_identity(self):
        self.click_on_element(self.IDENTITY_TYPE)
        self.input("NRIC", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        self.input("S9912375C", self.IDENTITY_NUMBER)
        self.input("27051976", self.DOB)
        self.click_on_element(self.NEXT)


    def fill_create_credential(self):
        self.input(email, self.EMAIL)
        self.input("Password2", self.PASSWORD)
        self.click_on_element(self.SEE_PASSWORD)
        self.input("Password2", self.CONFIRM_PASSWORD)
        self.click_on_element(self.SEE_CONFIRM_PASSWORD)
        self.click_on_element(self.NEXT)
        time.sleep(10)
