from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


class RegisterSingapore(Page):
    print('Email = ' + email)


    BUTTON_ALLOW_ACCESS_LOCATION = (By.ID, "com.android.permissioncontroller:id/permission_allow_always_button")
    PHONE_NUMBER_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    EMAIL_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    CONFIRM_EMAIL_TEXT = (By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")

    OTP1 = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.EditText")

    PASSWORD = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    SEE_PASSWORD = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    RE_PASSWORD = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
    SEE_RE_PASSWORD = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')

    SINGAPOREAN = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llSingaporean")

    # WEB MYINFO
    COMBOBOX_ID = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.widget.Spinner")
    CHOOSE_ID_MYINFO = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[8]")
    LOGIN = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.widget.Button")

    I_AGREE_MYINFO = (By.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[5]/android.widget.Button[1]")
    I_AGREE_ID = (By.ID, "allow")










    BUTTON_ALLOW_CAMERA = (By.ID, "com.android.permissioncontroller:id/permission_allow_button")
    TAKE_MYCARD_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    ACCEPT_MYCARD_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    TAKE_MYCARD_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    ACCEPT_MYCARD_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    TAKE_SELFIE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btCamera")

    # Personal Information
    ID_NUMBER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    COMBOBOX_COUNTRYBIRTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    SEARCH = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/etQuery")
    CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    COMBOBOX_NATIONALITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    COMBOBOX_GENDER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    FULLNAME = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    BIRTHDATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    COMBOBOX_MARTIALSTATUS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    OPTION_BUTTON_YES = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbWorkingInSingaporeYes')
    FIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    RESIDENTIAL_COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout')
    RESIDENTIAL_STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout')
    RESIDENTIAL_CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_APARTMENT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    RESIDENTIAL_POSTALCODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    RESIDENTIAL_OPTION_BUTTON_YES = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # Is your residential address the same as the address shown on your MyKad?
    RESIDENTIAL_ADDRESS_SAME_ON_MYKAD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # My mailing address is the same as my residential address
    MY_MAILING_SAME = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbSameAsResidential')


    # Page Mailing address
    MAILING_COUNTRY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_STATE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_CITY = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    MAILING_ADDRESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    MAILING_APARTMENT = (By.XPATH, 'hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView')
    MAILING_POSTALCODE = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Proof of Residence
    SIGNATURE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/ivSignature")
    SIGNATURE_TAKE_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    SIGNATURE_ACCEPT_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")

    # Take a picture of your employment pass
    EMPLOYMENT_TAKE_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    EMPLOYMENT_ACCEPT_FRONT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")
    EMPLOYMENT_TAKE_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btTakePicture")
    EMPLOYMENT_ACCEPT_BACK = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btAccept")

    # Product Application
    PRODUCT_HIGH_YIELD = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbHysa")
    PRODUCT_TRIO = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbTrio")
    PRODUCT_PREMIER_PLUS = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/cbPremierPlus")

    # Additional Information
    # Employed
    EMPLOYMENT_STATUS = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/tvValue")
    OCCUPATION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    EMPLOYER_NAME = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/textInputEditText")
    EMPLOYER_BUSINESS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    INCOME_ANNUM = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    SOURCE_FUND = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.LinearLayout')
    CHECKLIST1 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.CheckBox')
    CHECKLIST2 = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.CheckBox')
    SOURCE_WEALTH = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[7]/android.widget.LinearLayout/android.widget.LinearLayout')
    DONE = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/action_next")

    # Account Information
    PURPOSE_ACCOUNT = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')

    # Are you a resident, green card holder, or citizen of the United States of America or any of its territories?
    CITIZEN_US = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/rbAmericaResidentYes")

    # Do you have a Power of Attorney granted to person with U.S. Address?
    POWER_US = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/rbPowerOfAttorneyYes")

    COUNTRY_JURISDICTION = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]')
    TIN_NUMBER = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # Terms & Conditions
    # pilihan1
    I_UNDERSTOOD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbTermCondition')

    # pilihan2
    I_AGREE = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotional')
    VOICE_CALL = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalVoiceCall')
    SMS = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/cbPromotionalSms')



    # Sign In
    SignIn = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/btApply')
    EMAIL_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PASSWORD_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')

    # CREATE PIN LOGIN
    CREATE_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CREATE_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    CONFIRM_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CONFIRM_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')



    # def button_allow(self):
    #     self.click_on_element(self.BUTTON_ALLOW_ACCESS_LOCATION)

    def fill_field(self):
        self.input(phone, self.PHONE_NUMBER_TEXT)
        self.input(email, self.EMAIL_TEXT)
        self.input(email, self.CONFIRM_EMAIL_TEXT)

    def button_next(self):
        self.click_on_element(self.BUTTON_NEXT)

    def fill_otp1(self):
        self.input(OTP1, self.OTP1)

    def fill_password(self):
        self.input("Password2", self.PASSWORD)
        self.click_on_element(self.SEE_PASSWORD)
        self.input("Password2", self.RE_PASSWORD)
        self.click_on_element(self.SEE_RE_PASSWORD)

    def choose_singaporean(self):
        self.click_on_element(self.SINGAPOREAN)
        time.sleep(5)

    def choose_id_singaporean(self):
        self.click_on_element(self.COMBOBOX_ID)
        self.click_on_element(self.CHOOSE_ID_MYINFO)
        self.click_on_element(self.LOGIN)
        time.sleep(5)
        touch = TouchAction(self.driver)
        # touch.press(x=34, y=2059).move_to(x=42, y=420).release().perform()
        touch.press(x=968, y=1854).move_to(x=988, y=326).release().perform()
        time.sleep(10)
        # self.click_on_element(self.I_AGREE_ID)
        self.click_on_element(self.I_AGREE_MYINFO)
        time.sleep(15)