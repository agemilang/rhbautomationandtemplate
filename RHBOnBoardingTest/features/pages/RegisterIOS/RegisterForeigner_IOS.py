from credentials import fullnamec1, fullnamec2, fullnamec3, fullnamec4, phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, \
    phone6, email6, phone7, email7, OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver


def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)


class RegisterForeigner_IOS(Page):

    # SINGAPOREAN = (By.XPATH,
    #                '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    # MYKAD = (By.XPATH, '//XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')
    FOREIGNER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeButton')

    ALLOW = (By.CLASS_NAME, "Allow")

    NEXT = (By.ID, "NEXT")
    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')
    TAKE_PHOTO = (By.ID, "cam")

    RETAKE_PHOTO = (By.XPATH, '//XCUIElementTypeButton[@name="RETAKE"]')
    ACCEPT_PHOTO = (By.XPATH, '//XCUIElementTypeButton[@name="ACCEPT"]')
    # TAKE_PHOTO_SELFIE = (By.XPATH, '//XCUIElementTypeButton[@name="cam"]')


    # Personal Information
    PASSPORT_NUMBER = (By.XPATH, '//XCUIElementTypeOther[@name="etIdNumber"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    EXPIRY_DATE = (By.XPATH, '(//XCUIElementTypeOther[@name="editTextStackView"])[1]/XCUIElementTypeTextField')
    COMBOBOX_GENDER = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[1]')
    SEARCH = (By.ID, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')

    CHOOSE_FEMALE = (By.ID, 'Female')
    CHOOSE_MALE = (By.ID, 'Male')




    # CHOOSE_ROW1 = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llWrapper")
    FULL_NAME_PASSPORT = (By.ID, 'etFullName')
    COUNTRY_OF_BIRTH = (By.ID, 'ddCountryBirth')
    COMBOBOX_NATIONALITY = (By.ID, 'ddNationality')
    CHOOSE_AUSTRALIA = (By.XPATH, '//XCUIElementTypeStaticText[@name="Australia"]')
    CHOOSE_AUSTRALIAN = (By.XPATH, '//XCUIElementTypeStaticText[@name="Australian"]')
    BIRTHDATE = (By.ID, 'etDob')

    COMBOBOX_MARTIALSTATUS = (By.ID, 'ddMaritalStatus')
    CHOOSE_SINGLE = (By.XPATH, '//XCUIElementTypeStaticText[@name="Single"]')

    OPTION_BUTTON_YES = (By.ID, 'buttonYes')
    FIN = (By.ID, 'etFinNumber')
    # FIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout')

    # FIN = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/etFinNumber')

    # SCROOL

    RESIDENTIAL_COUNTRY = (By.ID, 'ddResidentialCountry')
    RESIDENTIAL_STATE = (By.ID, 'ddResidentialState')
    CHOOSE_SINGAPORE = (By.XPATH, "//XCUIElementTypeStaticText[@name='Singapore']")

    RESIDENTIAL_CITY = (By.ID, 'etResidentialCity')
    RESIDENTIAL_ADDRESS = (By.ID, 'etAddressLine1')
    RESIDENTIAL_APARTMENT = (By.ID, 'etApartmentNumber')
    RESIDENTIAL_POSTALCODE = (By.ID, 'etPostalCode')
    # RESIDENTIAL_OPTION_BUTTON_YES = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # Is your residential address the same as the address shown on your MyKad?
    # RESIDENTIAL_ADDRESS_SAME_ON_MYKAD = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/rbSameAsMyKadYes')

    # My mailing address is the same as my residential address
    MY_MAILING_SAME = (By.ID, 'checkBoxButton')

    # def screen_shot(self):
    #     ts = time.strftime("%Y_%m_%d_%H:%M:%S")
    # 
    #     self.driver.save_screenshot("/Users/admin/Documents/RHBOnBoardingTest/ScreenShot/" + ts + ".png")

    # Proof of Residential Addrress
    TAKE_PICTURE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]')
    TAKE_PICTURE_CAM = (By.XPATH, '//XCUIElementTypeButton[@name="iconIcCam"]')
    # NEXT = (By.XPATH, '//XCUIElementTypeButton[@name="NEXT"]')





    def choose_foreigner_ios(self):
        # self.screen_shot()

        self.click_on_element(self.FOREIGNER)

        time.sleep(5)

    def allow_camera_take_picture_passport_ios(self):
        self.click_on_element(self.NEXT)
        self.click_on_element(self.ALLOW)
        self.click_on_element(self.TAKE_PHOTO)
        self.click_on_element(self.ACCEPT_PHOTO)
        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)

    def fill_personal_information_working_in_singapore_yes_mailing_yes_ios(self):
        self.input(phone, self.PASSPORT_NUMBER)
        self.input("29102023", self.EXPIRY_DATE)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(fullnamec1, self.FULL_NAME_PASSPORT)
        self.click_on_element(self.COUNTRY_OF_BIRTH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)
        time.sleep(5)


        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.OPTION_BUTTON_YES)
        # time.sleep(5)
        self.input(phone, self.FIN)
        self.click_on_element(self.DONE)

        # time.sleep(5)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        time.sleep(5)
        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()


        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.MY_MAILING_SAME)

        self.click_on_element(self.NEXT)

        time.sleep(15)

    def fill_personal_information_working_in_singapore_yes_mailing_no_ios(self):
        self.input(phone1, self.PASSPORT_NUMBER)
        self.input("29102023", self.EXPIRY_DATE)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(fullnamec2, self.FULL_NAME_PASSPORT)
        self.click_on_element(self.COUNTRY_OF_BIRTH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)
        time.sleep(5)


        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.OPTION_BUTTON_YES)
        # time.sleep(5)
        self.input(phone, self.FIN)
        self.click_on_element(self.DONE)

        # time.sleep(5)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        time.sleep(5)
        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()


        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        # self.click_on_element(self.MY_MAILING_SAME)

        self.click_on_element(self.NEXT)

        time.sleep(15)




    def fill_personal_information_working_in_singapore_no_mailing_yes_ios(self):
        self.input(phone2, self.PASSPORT_NUMBER)
        self.input("29102023", self.EXPIRY_DATE)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(fullnamec3, self.FULL_NAME_PASSPORT)
        self.click_on_element(self.COUNTRY_OF_BIRTH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)
        time.sleep(5)


        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        # self.click_on_element(self.OPTION_BUTTON_YES)
        # time.sleep(5)
        # self.input(phone, self.FIN)
        # self.click_on_element(self.DONE)

        # time.sleep(5)


        time.sleep(5)
        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()
        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()


        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.MY_MAILING_SAME)

        self.click_on_element(self.NEXT)

        time.sleep(15)

    def fill_personal_information_working_in_singapore_no_mailing_no_ios(self):
        self.input(phone3, self.PASSPORT_NUMBER)
        self.input("29102023", self.EXPIRY_DATE)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(fullnamec4, self.FULL_NAME_PASSPORT)
        self.click_on_element(self.COUNTRY_OF_BIRTH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)
        time.sleep(5)


        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        # self.click_on_element(self.OPTION_BUTTON_YES)
        # time.sleep(5)
        # self.input(phone, self.FIN)
        # self.click_on_element(self.DONE)

        # time.sleep(5)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        time.sleep(5)
        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()


        # self.input(, self.RESIDENTIAL_APARTMENT)
        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        # self.click_on_element(self.MY_MAILING_SAME)

        self.click_on_element(self.NEXT)

        time.sleep(15)





    def take_picture_proof_of_residential_address_ios(self):
        self.click_on_element(self.NEXT)
        self.click_on_element(self.TAKE_PICTURE)
        time.sleep(5)
        self.click_on_element(self.TAKE_PICTURE_CAM)
        self.click_on_element(self.ACCEPT_PHOTO)
        self.click_on_element(self.NEXT)
        time.sleep(5)



    def take_picture_employment_pass_ios(self):
        self.click_on_element(self.NEXT)
        time.sleep(5)
        self.click_on_element(self.TAKE_PICTURE_CAM)
        self.click_on_element(self.ACCEPT_PHOTO)
        time.sleep(5)
        self.click_on_element(self.TAKE_PICTURE_CAM)
        self.click_on_element(self.ACCEPT_PHOTO)
        # self.click_on_element(self.NEXT)
        time.sleep(5)

























