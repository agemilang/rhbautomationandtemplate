from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver


def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class RegisterMyKad_IOS(Page):
    ALLOW = (By.CLASS_NAME, "Allow")
    OK = (By.CLASS_NAME, "OK")
    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')
    LOGIN = (By.ID, "LOG IN")

    PHONE_NUMBER = (By.ID, "etPhoneNumber")
    EMAIL = (By.ID, "etEmailAddress")
    RE_EMAIL = (By.ID, "etEmailAddressConfirmation")
    PROMOTIONAL_CODE = (By.ID, "etPromotionalCode")
    # NEXT = (By.ID, "btNext")
    NEXT = (By.ID, "NEXT")

    OTP = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField')
    RESEND_SMS = (By.ID, "Resend SMS")

    PASSWORD = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_PASSWORD = (By.XPATH, '(//XCUIElementTypeButton[@name="iconIcPwHidden"])[1]')
    REPASSWORD = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_REPASSWORD = (By.XPATH, '(//XCUIElementTypeButton[@name="iconIcPwHidden"])[2]')

    SINGAPOREAN = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    MYKAD = (By.XPATH, '//XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')
    FOREIGNER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeButton')

    TAKE_PHOTO = (By.XPATH, '//XCUIElementTypeButton[@name="iconIcCam"]')
    RETAKE_PHOTO = (By.XPATH, '//XCUIElementTypeButton[@name="RETAKE"]')
    ACCEPT_PHOTO = (By.XPATH, '//XCUIElementTypeButton[@name="ACCEPT"]')
    TAKE_PHOTO_SELFIE = (By.XPATH, '//XCUIElementTypeButton[@name="cam"]')

    # PERSONAL INFORMATION
    ID_NUMBER = (By.ID, 'etIdNumber')
    COMBOBOX_COUNTRYBIRTH = (By.ID, 'ddCountryBirth')
    SEARCH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    CHOOSE_AUSTRALIA = (By.XPATH, '//XCUIElementTypeStaticText[@name="Australia"]')
    CHOOSE_AUSTRALIAN = (By.XPATH, '//XCUIElementTypeStaticText[@name="Australian"]')

    COMBOBOX_NATIONALITY = (By.ID, 'ddNationality')
    COMBOBOX_GENDER = (By.ID, 'ddGender')
    CHOOSE_FEMALE = (By.XPATH, '//XCUIElementTypeStaticText[@name="Female"]')
    CHOOSE_MALE = (By.XPATH, '//XCUIElementTypeStaticText[@name="Male"]')

    FULLNAME = (By.ID, 'etFullName')
    BIRTHDATE = (By.ID, 'etDob')

    COMBOBOX_MARTIALSTATUS = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]')
    # COMBOBOX_MARTIALSTATUS = (By.ID, "ddMaritalStatus")
    CHOOSE_SINGLE = (By.XPATH, '//XCUIElementTypeStaticText[@name="Single"]')


    WORKING_IN_SINGAPORE = (By.ID, 'yesWorkInSingapore')
    FIN = (By.ID, 'etFinNumber')



    RESIDENTIAL_COUNTRY = (By.ID, "ddResidentialCountry")
    # # RESIDENTIAL_COUNTRY = (By.XPATH, "//XCUIElementTypeApplication[@name='RHB Mobile']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[10]/XCUIElementTypeOther/XCUIElementTypeButton")
    # # RESIDENTIAL_COUNTRY = (By.XPATH, "//XCUIElementTypeApplication[@name='RHB Mobile']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[10]/XCUIElementTypeOther/XCUIElementTypeButton")
    #
    CHOOSE_SINGAPORE = (By.XPATH, "//XCUIElementTypeStaticText[@name='Singapore']")
    #
    #
    RESIDENTIAL_STATE = (By.ID, "ddResidentialState")

    RESIDENTIAL_CITY = (By.ID, "etResidentialCity")
    RESIDENTIAL_ADDRESS = (By.ID, "etAddressLine1")
    RESIDENTIAL_APPARTMENT = (By.ID, "etApartmentNumber")
    RESIDENTIAL_POSTALCODE = (By.ID, "etPostalCode")

    NO_RESIDENTIAL = (By.ID, 'noSameMyKad')

    MY_MAILING_SAME = (By.ID, "cbSameAsResidential")
    # MY_MAILING_SAME = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[17]')


    # Mailing not same
    MAILING_COUNTRY = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[1]')
    MAILING_STATE = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[2]')
    MAILING_CITY = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    MAILING_ADDRESS = (By.XPATH, '//XCUIElementTypeTextView[@name="textViewMultiLine"]')
    MAILING_APARTMENT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    MAILING_POSTAL_CODE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')

    # Proof of Residential
    TAKE_PICTURE_PROOF_RESIDENTIAL = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]')



    # Product
    HYSA = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther')
    TRIO = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther')
    PP = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther')

    # Additional Information
    EMPLOYMENT_STATUS = (By.XPATH, '//XCUIElementTypeOther[@name="viewDropDownView"]')
    EMPLOYED = (By.ID, 'Employed')

    OCCUPATION = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[2]')
    ACTOR = (By.ID, 'ACTOR')


    EMPLOYER_NAME = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')

    NATURE_BUSINESS = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[3]')
    ADVERTISING = (By.ID, 'ADVERTISING')
    # IMPORT = (By.ID, 'IMPORT')

    INCOME_ANNUM = (By.XPATH, '(//XCUIElementTypeOther[@name="viewDropDownView"])[4]')
    NINETY = (By.ID, '$90,001 to $120,000 p.a.')

    SOURCE_FUND = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeOther')
    BUSINESS_INCOME = (By.ID, 'Business Income')
    BUSINESS_INVESTMENT = (By.ID, 'Business investment')

    SOURCE_WEALTH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther')
    SALARY = (By.ID, 'Salary')


    # Account Information
    PURPOSE_ACCOUNT = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]')
    INVESTMENT = (By.ID, 'Investment')

    YES_CITIZEN_AMERICAN = (By.XPATH, '(//XCUIElementTypeButton[@name="buttonYes"])[1]')
    ALERT_CITIZEN_AMERICAN = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    YES_GRANTED_US_ADDRESS = (By.XPATH, '(//XCUIElementTypeButton[@name="buttonYes"])[2]')
    ALERT_GRANTED_US_ADDRESS = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeButton')


    COUNTRY_TAX_JURIDICTION = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]')
    NO_TIN = (By.XPATH, '(//XCUIElementTypeButton[@name="buttonNo"])[3]')

    TIN_NUMBER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]')



    REASON = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]')
    REASON1 = (By.ID, 'The country / jurisdiction where the Account Holder is resident does not issue TINs to its residents')
    REASON2 = (By.ID, 'The Account Holder is otherwise unable to obtain a TIN or equivalent number')
    REASON3 = (By.ID, 'No TIN is required')

    ADD_ANOTHER_COUNTRY = (By.ID, '+ Add another country / jurisdiction of tax residence')

    SIGNATURE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[9]/XCUIElementTypeOther/XCUIElementTypeOther')

    # Term & Condition
    I_UNDERSTOOD = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[1]')

    # pilihan2
    I_AGREE = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[2]')
    VOICE_CALL = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[3]')
    SMS = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[4]')


    def screen_shot(self):
        ts = time.strftime("%Y_%m_%d_%H:%M:%S")

        self.driver.save_screenshot("/Users/admin/Documents/RHBOnBoardingTest/ScreenShot/"+ts+".png")


    def button_allow_ios(self):


        # time.sleep(5)
        # self.click_on_element(self.ALLOW)
        # time.sleep(5)
        self.click_on_element(self.ALLOW)
        time.sleep(5)
        self.click_on_element(self.ALLOW)
        time.sleep(5)

    def fill_registration_ios1(self):
        self.input(phone, self.PHONE_NUMBER)
        self.input(email, self.EMAIL)
        self.input(email, self.RE_EMAIL)
        self.driver.hide_keyboard()

        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def fill_registration_ios2(self):
        self.input(phone1, self.PHONE_NUMBER)
        self.input(email1, self.EMAIL)
        self.input(email1, self.RE_EMAIL)
        self.driver.hide_keyboard()

        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def fill_registration_ios3(self):
        self.input(phone2, self.PHONE_NUMBER)
        self.input(email2, self.EMAIL)
        self.input(email2, self.RE_EMAIL)
        self.driver.hide_keyboard()

        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def fill_registration_ios4(self):
        self.input(phone3, self.PHONE_NUMBER)
        self.input(email3, self.EMAIL)
        self.input(email3, self.RE_EMAIL)
        self.driver.hide_keyboard()

        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)


    def fill_otp_ios(self):
        self.input("123456", self.OTP)
        # self.screen_shot()

        self.click_on_element(self.DONE)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def fill_password_ios(self):
        self.input("Password2", self.PASSWORD)
        icon_password = self.driver.find_elements_by_xpath("//XCUIElementTypeButton[@name='iconIcPwHidden']")[0]
        icon_password.click()
        self.input("Password2", self.REPASSWORD)
        icon_repassword = self.driver.find_elements_by_xpath("//XCUIElementTypeButton[@name='iconIcPwHidden']")[1]
        icon_repassword.click()
        self.driver.hide_keyboard()

        # self.screen_shot()
        self.click_on_element(self.NEXT)
        # self.screen_shot()

        time.sleep(5)


    def choose_mykad_ios(self):
        # self.screen_shot()

        self.click_on_element(self.MYKAD)

        time.sleep(5)

    def take_photo_mykad_ios(self):
        self.click_on_element(self.NEXT)
        # self.screen_shot()

        self.click_on_element(self.OK)
        # self.click_on_element(self.TAKE_PHOTO)
        # self.click_on_element(self.RETAKE_PHOTO)

        # self.screen_shot()

        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.ACCEPT_PHOTO)

        # self.click_on_element(self.TAKE_PHOTO)
        # self.click_on_element(self.RETAKE_PHOTO)
        # self.screen_shot()
        time.sleep(5)
        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.ACCEPT_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.TAKE_PHOTO_SELFIE)
        time.sleep(10)

    #  Kemungkinan Pengisian Personal Information sudah ada option isian yang berbeda
    # 1

    def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_yes_ios(self):
        # self.screen_shot()

        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        # self.screen_shot()

        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        # self.screen_shot()

        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345)   .release()   .perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)
        # self.screen_shot()

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        # self.screen_shot()

        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)


        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()
        # self.screen_shot()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        # self.screen_shot()

        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        self.click_on_element(self.WORKING_IN_SINGAPORE)

        #
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.input(fin, self.FIN)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()


        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        # self.screen_shot()

        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        # self.screen_shot()

        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)
        # self.screen_shot()

        time.sleep(5)

    # 2

    def fill_personal_information_working_in_singapore_yes_residential_yes_mailing_no_ios(self):
        # self.screen_shot()

        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)
        # self.screen_shot()

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()
        # self.screen_shot()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        self.click_on_element(self.WORKING_IN_SINGAPORE)

        #
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.input(fin, self.FIN)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)
        # self.screen_shot()

        # self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)

    # 3

    def fill_personal_information_working_in_singapore_yes_residential_no_mailing_yes_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        self.click_on_element(self.WORKING_IN_SINGAPORE)

        #
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)

        self.input(fin, self.FIN)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.NO_RESIDENTIAL)


        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)



    # 4

    def fill_personal_information_working_in_singapore_yes_residential_no_mailing_no_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        self.click_on_element(self.WORKING_IN_SINGAPORE)

        #
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        time.sleep(5)

        self.input(fin, self.FIN)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.NO_RESIDENTIAL)

        # self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)

    # 5

    def fill_personal_information_working_in_singapore_no_residential_yes_mailing_yes_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)


        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=365).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        # self.click_on_element(self.NO_RESIDENTIAL)

        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)

    # 6

    def fill_personal_information_working_in_singapore_no_residential_yes_mailing_no_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=365).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        # self.click_on_element(self.NO_RESIDENTIAL)

        # self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)

    # 7

    def fill_personal_information_working_in_singapore_no_residential_no_mailing_yes_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=365).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=365).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        # self.click_on_element(self.NO_RESIDENTIAL)

        self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)

    # 8

    def fill_personal_information_working_in_singapore_no_residential_no_mailing_no_ios(self):
        self.input(phone, self.ID_NUMBER)
        self.click_on_element(self.COMBOBOX_COUNTRYBIRTH)
        self.input("australia", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        self.click_on_element(self.COMBOBOX_NATIONALITY)
        self.input("australian", self.SEARCH)
        self.click_on_element(self.CHOOSE_AUSTRALIAN)

        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345).release().perform()
        # self.driver.(284, 565, 280, 445)
        time.sleep(5)

        self.click_on_element(self.COMBOBOX_GENDER)
        # self.input("Female", self.SEARCH)
        self.click_on_element(self.CHOOSE_FEMALE)
        self.input(female, self.FULLNAME)
        self.driver.hide_keyboard()

        self.input("06042000", self.BIRTHDATE)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=365).move_to(x=280, y=145).release().perform()

        self.click_on_element(self.COMBOBOX_MARTIALSTATUS)
        # self.input("Single", self.SEARCH)
        self.click_on_element(self.CHOOSE_SINGLE)
        time.sleep(5)
        # self.click_on_element(self.CHOOSE_SINGLE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=365).release().perform()
        time.sleep(5)

        self.click_on_element(self.RESIDENTIAL_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.RESIDENTIAL_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.RESIDENTIAL_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        self.input(address, self.RESIDENTIAL_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.RESIDENTIAL_POSTALCODE)
        self.click_on_element(self.DONE)

        self.click_on_element(self.NO_RESIDENTIAL)

        # self.click_on_element(self.MY_MAILING_SAME)
        self.click_on_element(self.NEXT)

        time.sleep(5)






    def fill_mailing_address_ios(self):
        # self.screen_shot()

        self.click_on_element(self.MAILING_COUNTRY)
        self.click_on_element(self.CHOOSE_SINGAPORE)
        self.click_on_element(self.MAILING_STATE)
        self.click_on_element(self.CHOOSE_SINGAPORE)

        self.input(city, self.MAILING_CITY)
        self.driver.hide_keyboard()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()


        self.input(address, self.MAILING_ADDRESS)
        self.driver.hide_keyboard()

        self.input(postalcode, self.MAILING_POSTAL_CODE)
        self.click_on_element(self.DONE)
        # self.screen_shot()

        self.click_on_element(self.NEXT)

    def take_picture_your_proof_residence_ios(self):
        # self.screen_shot()

        self.click_on_element(self.TAKE_PICTURE_PROOF_RESIDENTIAL)

        self.click_on_element(self.TAKE_PHOTO)
        self.click_on_element(self.ACCEPT_PHOTO)
        # self.screen_shot()

        self.click_on_element(self.NEXT)


    def take_picture_your_employment_pass_ios(self):
        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.ACCEPT_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.ACCEPT_PHOTO)
        time.sleep(5)


    def select_product_application_hysa_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_hysa_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_trio_ios(self):
        # self.screen_shot()

        self.click_on_element(self.TRIO)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_pp_ios(self):
        # self.screen_shot()

        self.click_on_element(self.PP)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_hysa_trio_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)
        self.click_on_element(self.TRIO)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_hysa_trio_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)
        self.click_on_element(self.TRIO)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_hysa_pp_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)
        self.click_on_element(self.PP)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def select_product_application_hysa_trio_pp_ios(self):
        # self.screen_shot()

        self.click_on_element(self.HYSA)
        self.click_on_element(self.TRIO)
        self.click_on_element(self.PP)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def fill_additional_information_ios(self):
        # self.screen_shot()

        self.click_on_element(self.EMPLOYMENT_STATUS)
        time.sleep(5)

        # self.screen_shot()

        self.click_on_element(self.EMPLOYED)
        self.click_on_element(self.OCCUPATION)
        time.sleep(5)
        # self.screen_shot()

        self.click_on_element(self.ACTOR)
        self.input(male+phone, self.EMPLOYER_NAME)
        self.driver.hide_keyboard()

        self.click_on_element(self.NATURE_BUSINESS)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.ADVERTISING)


        self.click_on_element(self.INCOME_ANNUM)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.NINETY)

        self.click_on_element(self.SOURCE_FUND)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.BUSINESS_INCOME)
        self.click_on_element(self.BUSINESS_INVESTMENT)
        self.click_on_element(self.DONE)


        self.click_on_element(self.SOURCE_WEALTH)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.SALARY)
        self.click_on_element(self.DONE)

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()


        self.click_on_element(self.NEXT)

    def fill_account_information_ios(self):
        # self.screen_shot()

        self.click_on_element(self.PURPOSE_ACCOUNT)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.INVESTMENT)
        self.click_on_element(self.COUNTRY_TAX_JURIDICTION)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.CHOOSE_AUSTRALIA)
        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()


        self.input(phone, self.TIN_NUMBER)
        # self.screen_shot()

        self.input(phone, self.DONE)


        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=265).release().perform()
        time.sleep(5)
        # self.screen_shot()

        self.click_on_element(self.SIGNATURE)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.TAKE_PHOTO)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.ACCEPT_PHOTO)
        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(5)

    def check_term_condition_ios(self):
        # self.screen_shot()

        self.click_on_element(self.I_UNDERSTOOD)
        self.click_on_element(self.I_AGREE)
        self.click_on_element(self.VOICE_CALL)
        self.click_on_element(self.SMS)
        # self.screen_shot()

        self.click_on_element(self.NEXT)
        time.sleep(75)
        # self.screen_shot()
        # time.sleep(65)
        # self.screen_shot()

        self.click_on_element(self.LOGIN)




