from credentials import TIN, fullnamec1, fullnamec2, fullnamec3, fullnamec4, phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode

from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time

def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class ETBSMS_IOS(Page):
    # Sign In

    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')

    REGISTER_SMS_OTP = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')
    IDENTITY_TYPE = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]')
    SEARCH = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    CHOOSE_ROW1 = (By.XPATH, '//XCUIElementTypeStaticText[@name="NRIC"]')

    IDENTITY_NUMBER = (By.XPATH,'//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    DOB = (By.XPATH,'//XCUIElementTypeOther[@name="editTextStackView"]/XCUIElementTypeTextField')

    NEXT = (By.XPATH, '//XCUIElementTypeButton[@name="NEXT"]')


    EMAIL = (By.XPATH,'//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField')
    PASSWORD = (By.XPATH,'//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_PASSWORD = (By.XPATH,'(//XCUIElementTypeButton[@name="iconIcPwHidden"])[1]')
    CONFIRM_PASSWORD = (By.XPATH,'//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_CONFIRM_PASSWORD = (By.XPATH,'(//XCUIElementTypeButton[@name="iconIcPwHidden"])[2]')

    # Term & Condition
    I_UNDERSTOOD = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[1]')

    # pilihan2
    I_AGREE = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[2]')
    VOICE_CALL = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[3]')
    SMS = (By.XPATH, '(//XCUIElementTypeButton[@name="checkMarkUnchecked 1"])[4]')



    SKIP = (By.XPATH,'//XCUIElementTypeButton[@name="SKIP"]')



    def select_register_smsotp_ios(self):
        time.sleep(5)
        self.click_on_element(self.REGISTER_SMS_OTP)
        time.sleep(5)

    def fill_identity_ios(self):
        self.click_on_element(self.IDENTITY_TYPE)
        self.input("NRIC", self.SEARCH)
        self.click_on_element(self.CHOOSE_ROW1)
        # self.input("S9912375C", self.IDENTITY_NUMBER)
        # self.input("27051976", self.DOB)
        self.input("S9812390C", self.IDENTITY_NUMBER)
        self.input("25051992", self.DOB)
        self.click_on_element(self.NEXT)


    def fill_create_credential_ios(self):
        self.input(email, self.EMAIL)
        self.input("Password2", self.PASSWORD)
        self.click_on_element(self.SEE_PASSWORD)
        self.input("Password2", self.CONFIRM_PASSWORD)
        self.click_on_element(self.SEE_CONFIRM_PASSWORD)
        self.click_on_element(self.DONE)
        self.click_on_element(self.NEXT)
        time.sleep(5)

    def skip_soft_token_ios(self):
        self.click_on_element(self.SKIP)
        time.sleep(5)

    def check_term_condition_ios_otp(self):
        # self.screen_shot()

        self.click_on_element(self.I_UNDERSTOOD)
        self.click_on_element(self.I_AGREE)
        self.click_on_element(self.VOICE_CALL)
        self.click_on_element(self.SMS)
        # self.screen_shot()

        self.click_on_element(self.NEXT)
