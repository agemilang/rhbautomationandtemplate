from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, male, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver


def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class RegisterMyInfo_IOS(Page):
    ALLOW = (By.CLASS_NAME, "Allow")
    OK = (By.CLASS_NAME, "OK")
    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')
    LOGIN = (By.ID, "LOG IN")

    PHONE_NUMBER = (By.ID, "etPhoneNumber")
    EMAIL = (By.ID, "etEmailAddress")
    RE_EMAIL = (By.ID, "etEmailAddressConfirmation")
    PROMOTIONAL_CODE = (By.ID, "etPromotionalCode")
    # NEXT = (By.ID, "btNext")
    NEXT = (By.ID, "NEXT")

    OTP = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField')
    RESEND_SMS = (By.ID, "Resend SMS")

    PASSWORD = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_PASSWORD = (By.XPATH, '(//XCUIElementTypeButton[@name="iconIcPwHidden"])[1]')
    REPASSWORD = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_REPASSWORD = (By.XPATH, '(//XCUIElementTypeButton[@name="iconIcPwHidden"])[2]')

    SINGAPOREAN = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    # MYKAD = (By.XPATH, '//XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')
    # FOREIGNER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeButton')


    COMBO_BOX_USER = (By.XPATH, '(//XCUIElementTypeOther[@name="Mock pass login"])[1]/XCUIElementTypeOther[3]')
    LOGIN = (By.ID, "Login")
    I_AGREE = (By.ID, "I Agree")
    NO_THANKS = (By.ID, "No Thanks")

    # def screen_shot(self):
    #     ts = time.strftime("%Y_%m_%d_%H:%M:%S")
    #
    #     self.driver.save_screenshot("/Users/admin/Documents/RHBOnBoardingTest/ScreenShot/"+ts+".png")
    #

    def screen_shot(self):
        ts = time.strftime("%Y_%m_%d_%H:%M:%S")

        self.driver.save_screenshot("/Users/admin/Documents/RHBOnBoardingTest/ScreenShot/"+ts+".png")




    def choose_myinfo_ios(self):
        self.screen_shot()

        self.click_on_element(self.SINGAPOREAN)

        time.sleep(10)

    def i_click_login_myinfo(self):
        self.screen_shot()


        # self.input("S9812380F", self.COMBO_BOX_USER)
        self.input("S6005048A", self.COMBO_BOX_USER)

        time.sleep(5)

        self.screen_shot()

        # self.click_on_element(self.DONE)
        #
        # self.screen_shot()

        self.click_on_element(self.LOGIN)
        time.sleep(5)
        self.screen_shot()

        touch = TouchAction(self.driver)
        touch.long_press(x=284, y=565).move_to(x=280, y=345)   .release()   .perform()
        time.sleep(5)
        self.screen_shot()

        self.click_on_element(self.I_AGREE)
        time.sleep(15)

        self.screen_shot()

