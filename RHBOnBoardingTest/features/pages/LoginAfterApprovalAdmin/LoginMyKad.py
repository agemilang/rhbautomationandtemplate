from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class LoginMyKad(Page):


    BUTTON_ALLOW_ACCESS_LOCATION = (By.ID, "com.android.permissioncontroller:id/permission_allow_always_button")

    BUTTON_SIGN_IN = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btSignIn")

    EMAIL_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    PASSWORD_LOGIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_PASSWORD = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/ivPeek")


    BUTTON_NEXT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btNext")

    ERROR_ACCOUNT_REJECTED = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/tvMessage")




    CREATE_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CREATE_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[1]')
    CONFIRM_PIN = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText')
    SEE_CONFIRM_PIN = (By.XPATH, '(//android.widget.ImageButton[@content-desc="RHB Mobile"])[2]')


    # Skip Activate Soft Token
    SKIP_ACTIVATION = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btSkipSoftTokenActivation")

    # Mobile Banking Button
    MB_BUTTON = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llMobileBanking')

    # Hamburger Menu
    HAMBURGER_MENU = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView')
    E_APPS = (By.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]')

    # Choose Product
    FIX_DEPOSIT = (By.ID, 'sg.com.rhbgroup.travelfx.android.sit:id/llFda')

    # Allow Location
    ALLOW_ALL_TIME = (By.ID, 'com.android.permissioncontroller:id/permission_allow_always_button')
    ALLOW_ONLY_USE_APP = (By.ID, 'com.android.permissioncontroller:id/permission_allow_foreground_only_button')
    DENY = (By.ID, 'com.android.permissioncontroller:id/permission_deny_button')




    def fill_login(self):
        self.click_on_element(self.BUTTON_SIGN_IN)
        self.input(email, self.EMAIL_LOGIN)
        # self.find_element(self.EMAIL_LOGIN).clear()

        self.input("Password2", self.PASSWORD_LOGIN)
        self.click_on_element(self.SEE_PASSWORD)
        self.click_on_element(self.BUTTON_NEXT)

        time.sleep(10)

    def check_message_error_rejected(self):
        error_message_account_rejected = self.find_elements(self.ERROR_ACCOUNT_REJECTED)
        compare_data_with_expected(
            expected="Access denied.\n\nFor assistance, please call Customer Support.",
            real=error_message_account_rejected[0].text)



    # def create_pin(self):
    #     self.input("995995", self.CREATE_PIN)
    #     self.click_on_element(self.SEE_CREATE_PIN)
    #     self.input("995995", self.CONFIRM_PIN)
    #     self.click_on_element(self.SEE_CONFIRM_PIN)
    #     self.click_on_element(self.BUTTON_NEXT)
    #     # time.sleep(10)


    def click_skip_activation(self):
        self.click_on_element(self.SKIP_ACTIVATION)

    def click_mobile_banking(self):
        self.click_on_element(self.MB_BUTTON)


