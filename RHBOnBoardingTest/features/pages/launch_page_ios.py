from selenium.webdriver.common.by import By
from features.pages.base_page import Page
from credentials import phone, email
import time

class LaunchPageIOS(Page):
    BUTTON_LOGIN = (By.XPATH, '//XCUIElementTypeButton[@name="SIGN IN"]')
    BACK = (By.XPATH, '//XCUIElementTypeButton[@name="back"]')

    BUTTON_CREATE_ACCOUNT = (By.XPATH, '//XCUIElementTypeButton[@name="REGISTER NOW"]')
    NEW_CUSTOMER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')
    NEW_CUSTOMER2 = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton')

    EXIST_CUSTOMER = (By.XPATH, '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton')

    # BUTTON_ALLOW_PHONECALL = (By.NAME, "com.android.permissioncontroller:id/permission_allow_button")

    # def allow_permission_phonecalls(self):
    #     self.click_on_element(self.BUTTON_ALLOW_PHONECALL)






    def login_with_existing_account_ios(self):
        self.click_on_element(self.BUTTON_LOGIN)

    def create_new_account_ios(self):
        # time.sleep(5)
        # self.click_on_element(self.BACK)

        time.sleep(5)
        self.click_on_element(self.BUTTON_CREATE_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.NEW_CUSTOMER)

    def create_exist_account_ios(self):
        time.sleep(5)
        self.click_on_element(self.BUTTON_CREATE_ACCOUNT)
        time.sleep(5)
        self.click_on_element(self.EXIST_CUSTOMER)