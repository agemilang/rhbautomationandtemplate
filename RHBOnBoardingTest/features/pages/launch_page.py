import time

from selenium.webdriver.common.by import By
from features.pages.base_page import Page
from credentials import phone, email

class LaunchPageAndroid(Page):
    BUTTON_LOGIN = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btSignIn")
    BUTTON_CREATE_ACCOUNT = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/btApplyNow")
    BUTTON_ALLOW_PHONECALL = (By.ID, "com.android.permissioncontroller:id/permission_allow_button")
    NEW_BANK_CUSTOMER = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llNewToBank")
    EXIST_CUSTOMER = (By.ID, "sg.com.rhbgroup.travelfx.android.sit:id/llExistingToBank")

    def allow_permission_phonecalls(self):
        self.click_on_element(self.BUTTON_ALLOW_PHONECALL)

    def login_with_existing_account(self):
        self.click_on_element(self.BUTTON_LOGIN)
        time.sleep(5)







    def create_new_account(self):
        self.click_on_element(self.BUTTON_CREATE_ACCOUNT)
        self.click_on_element(self.NEW_BANK_CUSTOMER)

    def create_new_account_etb(self):
        self.click_on_element(self.BUTTON_CREATE_ACCOUNT)
        self.click_on_element(self.EXIST_CUSTOMER)

    def fill_field(self):
        self.input(phone, self.PHONE_NUMBER_TEXT)
        self.input(email, self.EMAIL_TEXT)
        self.input(email, self.CONFIRM_EMAIL_TEXT)

    def button_next(self):
        self.click_on_element(self.BUTTON_NEXT)