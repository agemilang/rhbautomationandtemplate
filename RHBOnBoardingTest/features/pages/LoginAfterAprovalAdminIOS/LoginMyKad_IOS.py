from credentials import phone, email, phone1, email1, phone2, email2, phone3, email3, phone4, email4, phone5, email5, phone6, email6, phone7, email7,  OTP1, female, fin, city, address, postalcode
from selenium.webdriver.common.by import By
from features.pages.base_page import Page
import time
from appium.webdriver.common.touch_action import TouchAction


def compare_data_with_expected(expected, real):
    assert expected == real, "Expected '{}', but got '{}'".format(expected, real)

class LoginMyKad_IOS(Page):

    DONE = (By.XPATH, '//XCUIElementTypeButton[@name="Done"]')
    NEXT = (By.ID, "NEXT")

    # LOGIN AFTER REGISTER
    LOGIN_EMAIL = (By.XPATH,
                   '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeTextField')
    LOGIN_PASSWORD = (By.XPATH,
                      '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeSecureTextField')
    LOGIN_SEE_PASSWORD = (By.XPATH, '//XCUIElementTypeButton[@name="iconIcPwHidden"]')
    LOGIN_BUTTON = (By.XPATH,
                    '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton')

    PIN = (By.XPATH,
           '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField[1]')
    SEE_PIN = (By.ID, 'iconIcPwHidden')
    REPIN = (By.XPATH,
             '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField')
    SEE_REPIN = (By.ID, 'Button')

    MB_BUTTON = (By.XPATH,
                 '//XCUIElementTypeApplication[@name="RHB Mobile"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[3]')

    ALERT_NOT_APPROVAL = (By.ID, 'Oops We are processing your application(s) and will notify you in 1 working day')


    def login_after_register_ios1(self):
        # self.screen_shot()

        self.input(email, self.LOGIN_EMAIL)
        self.input("Password2", self.LOGIN_PASSWORD)
        self.click_on_element(self.LOGIN_SEE_PASSWORD)
        self.driver.hide_keyboard()
        # self.screen_shot()

        self.click_on_element(self.LOGIN_BUTTON)
        time.sleep(5)

    def login_after_register_ios2(self):
        # self.screen_shot()

        self.input(email1, self.LOGIN_EMAIL)
        self.input("Password2", self.LOGIN_PASSWORD)
        self.click_on_element(self.LOGIN_SEE_PASSWORD)
        self.driver.hide_keyboard()
        # self.screen_shot()

        self.click_on_element(self.LOGIN_BUTTON)
        time.sleep(5)

    def login_after_register_ios3(self):
        # self.screen_shot()

        self.input(email2, self.LOGIN_EMAIL)
        self.input("Password2", self.LOGIN_PASSWORD)
        self.click_on_element(self.LOGIN_SEE_PASSWORD)
        self.driver.hide_keyboard()
        # self.screen_shot()

        self.click_on_element(self.LOGIN_BUTTON)
        time.sleep(5)

    def login_after_register_ios4(self):
        # self.screen_shot()

        self.input(email3, self.LOGIN_EMAIL)
        self.input("Password2", self.LOGIN_PASSWORD)
        self.click_on_element(self.LOGIN_SEE_PASSWORD)
        self.driver.hide_keyboard()
        # self.screen_shot()

        self.click_on_element(self.LOGIN_BUTTON)
        time.sleep(5)

    def login_existing_ios(self):
        # self.screen_shot()

        self.input("isyak12@yopmail.com", self.LOGIN_EMAIL)
        self.input("Password2", self.LOGIN_PASSWORD)
        self.click_on_element(self.LOGIN_SEE_PASSWORD)
        self.driver.hide_keyboard()
        # self.screen_shot()

        self.click_on_element(self.LOGIN_BUTTON)
        time.sleep(5)

    def i_fill_create_pin_ios(self):
        self.input("321321", self.PIN)
        self.click_on_element(self.SEE_PIN)

        self.input("321321", self.REPIN)
        self.click_on_element(self.SEE_REPIN)

        self.click_on_element(self.DONE)
        time.sleep(5)
        # self.screen_shot()
        self.click_on_element(self.NEXT)
        time.sleep(5)
        # self.screen_shot()

    def i_click_mobile_banking_ios(self):
        self.click_on_element(self.MB_BUTTON)
        time.sleep(5)
        # self.screen_shot()
        time.sleep(5)






