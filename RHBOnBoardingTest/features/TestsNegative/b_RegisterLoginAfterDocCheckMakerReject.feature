Feature: Tests User Register and Login after Reject Doc Check Maker

Scenario: Successful Negative Test Register RHBMobileRegistration MyKad Working Singapore Yes Residential Yes Mailing Yes 3 Product Employed
    Given I allow phone call permission
    When I open register page
    Then I Allow RHB Mobile to access this device's location?
    Then I register
    Then I Click Next Button Email
#    Then I Check Error Message
    Then I Fill OTP Number
    Then I Click Next Button OTP 1
    Then I Fill Password
    Then I Click Next Button Password
    Then I Choose MyKad
    Then I Click Next Button Take Picture
    Then I Fill Personal Information Working at Singapore Yes Residential Yes Mailing Yes
    Then I Take Photo Employment Pass
    Then I Choose 3 Product Application
    Then I Fill Additional Information
    Then I Fill Account Information
    Then I Agree with Term & Condition
    Then I Fill Email & Password
    Then I Fill Create PIN
    Then I Click MB Banking
    Then I Check Message Error On Progress

#    API
Scenario: GET Customer Onboarding Manual KYC
  Given I POST admin login Manual KYC
   Then I Get List Customer Onboarding Get Identity No And ID Manual KYC
   Then I Get List Detail Customer Onboarding Manual KYC
   Then I Get List History Customer Onboarding Manual KYC
#   Then I Get List Document FATCA Customer Onboarding

   Then I Get List Task Approval Onboarding Get Task ID Manual KYC
   Then I Get List Task Detail Approval Onboarding Get Onboard ID Manual KYC
   Then I Get List Task Document FATCA Approval Onboarding Manual KYC
   Then I Get List Task Document Approval by Task Stage Approval Onboarding Manual KYC
   Then I Get List Task Document PDF by Task ID Approval Onboarding Manual KYC
   Then I Post Upload Document Approval Onboarding Manual KYC
   Then I Post Action Task Approval Onboarding Manual KYC Approve


Scenario: Successful Negative Test Login After Reject MyKad Working Singapore Yes Residential Yes Mailing Yes 3 Product Employed
  Given I allow phone call permission
  When I Fill Email & Password After Approval
  Then I Fill Create PIN After Approval
  Then I Click Mobile Banking After Approval
  Then I Check Message Error On Progress


Scenario: Stage Customer Onboarding Doc Check Maker Reject
  Given I POST admin login Doc Check Maker
  Then I Get List Customer Onboarding Get Identity No And ID Doc Check Maker
  Then I Get List Task Approval Onboarding Get Task ID Doc Check Maker
  Then I Get List Task Detail Approval Onboarding Get Onboard ID Doc Check Maker
  Then I Post Upload Document Approval Onboarding Doc Check Maker
  Then I Post Action Task Approval Onboarding Doc Check Maker Reject

Scenario: Successful Negative Test Login After Reject MyKad Working Singapore Yes Residential Yes Mailing Yes 3 Product Employed
  Given I allow phone call permission
  When I Fill Email & Password After Approval
  Then I Check Message Error Account Rejected







